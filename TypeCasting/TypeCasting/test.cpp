#include <iostream>
using namespace std;

//int main()
//{
//	// 隐式类型转换
//	int i = 1;
//	double d = i;
//	printf("%d, %.2f\n", i, d);
//	// 显式类型转换
//	int* pi = &i;
//	int address = (int)pi;
//	printf("%d, %p\n", address, pi);
//	return 0;
//}


//class A
//{
//public:
//	A(const int& a1 = 1, const int& a2 = 1)
//		:_a1(a1)
//		,_a2(a2)
//	{}
//
//	operator int() const
//	{
//		return _a1 + _a2;
//	}
//
//	int get() const
//	{
//		return _a1 + _a2;
//	}
//
//	virtual void f() {}
//private:
//	int _a1;
//	int _a2;
//};
//
//class B : public A
//{
//public:
//	int _b = 1;
//};
// 
//class A
//{
//public:
//	A(const int& a = 1)
//		:_a(a)
//	{}
//
//	/*operator int()
//	{
//		return _a;
//	}*/
//
//	int getA() const
//	{
//		return _a;
//	}
//private:
//	int _a;
//};
//class B
//{
//public:
//	B(const A& a) // 使用 A 构造 B
//		:_b(a.getA())
//	{}
//private:
//	int _b;
//};
//int main()
//{
//	// 内置类型 -> 自定义类型
//
//	// 自定义类型->内置类型
//	//int a = a1;
//
//	// 自定义类型->自定义类型
//	A a1 = 10;
//	B b = a1;
//	return 0;
//}


//void func(A* pa)
//{
//	B* pb1 = static_cast<B*>(pa);
//	B* pb2 = dynamic_cast<B*>(pa);
//
//	pb1->_b++;
//	cout << "pb1:" << pb1 << endl;
//	cout << "pb2:" << pb2 << endl;
//}

//void func(A& ra)
//{
//	B& pb1 = static_cast<B&>(ra);
//	B& pb2 = dynamic_cast<B&>(ra);
//
//	pb1._b++;
//	//cout << "pb1:" << pb1 << endl;
//	//cout << "pb2:" << pb2 << endl;
//}
//int main()
//{
//	// 四个类型转换操作符，为了使类型转换可视性更强，便于追踪调试
//
//	// 关联性较强的转换
//	int i = static_cast<int>(1.1);
//	A a1 = static_cast<A>(1);
//
//	//关联性不强的转换
//	int address1 = reinterpret_cast<int>(&i);
//	int* pi = reinterpret_cast<int*>(i);
//
//	// 失去常性的转换
//	volatile const int ca = 1;
//	//int* pa = (int*)&a;
//	int* pa = const_cast<int*>(&ca);
//	(*pa)++;
//	cout << ca << " " << *pa << endl;
//
//	// 父类转换到子类
//	A a;
//	B b;
//	func(&a);
//	func(&b);
//	return 0;
//}

//class A
//{
//public:
//	A(const int& a = 1)
//		:_a(a)
//	{}
//private:
//	int _a;
//};

//int main()
//{
//	double d = 11.45;
//	int i = static_cast<int>(d);
//	cout << i << endl;
//
//	A a = static_cast<A>(1);
//	return 0;
//}

//int main()
//{
//	int i = 10;
//	int* pi = reinterpret_cast<int*>(&i);
//	cout << pi << ":" << *pi << endl;
//	return 0;
//}

//int main()
//{
//	volatile const int ca = 1;
//	//int* pa = (int*)&ca;
//	int* pa = const_cast<int*>(&ca);
//	(*pa)++;
//
//	cout << ca << endl;
//	cout << *pa << endl;
//	return 0;
//}

class A
{
public:
	virtual void f() {}
private:
	int _a = 10;
};

class B : public A
{
public:
	int _b;
};

void func(A* pa) // A 类指针
{
	// 强制转换为 B 类指针
	//B* pb = static_cast<B*>(pa);

	// 动态转换
	B* pb = dynamic_cast<B*>(pa);
	if (pb)
	{
		// 转换成功
		// 修改 B 类的成员，测试越界
		cout << "转换成功" << endl;
		pb->_b++;
	}
	else
	{
		// 转换失败
		cout << "转换失败" << endl;
	}
}

int main()
{
	A a;
	B b;

	func(&b); // pa 指向 B 类对象
	func(&a); // pa 指向 A 类对象

	decltype(a) a1;
	return 0;
}