#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"

void StackInit(Stack* ps, int capacity)
{
	ps->data = (int*)malloc(sizeof(int) * capacity);
	ps->capacity = capacity;
}

void func(int i)
{
	cout << i << endl;
}