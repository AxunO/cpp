#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string.h>
#include <assert.h>
using namespace std;

namespace ns1
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;

		// ����
		string(const char* str = "")
		{
			_size = _capacity = strlen(str);
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		// ����
		~string()
		{
			delete[] _str;
			_size = _capacity = 0;
		}

		//��������
		string(const string& s)
		{
			// ���
			_str = new char[s._capacity + 1];
			strcpy(_str, s._str);
			_size = s._size;
			_capacity = s._capacity;
		}

		// ��ֵ����
		string& operator=(const string& s)
		{
			// ��գ������ǿ��ռ�
			clear();
			if (s._capacity > _capacity)
				reserve(s._capacity);

			strcpy(_str, s._str);
			_size = s._size;

			return *this;
		}

		// ����
		// 1.[]
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}
		
		const char& operator[](size_t pos) const
		{
			assert(pos < _size);
			return _str[pos];
		}

		// 2.������
		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return (_str + _size);
		}
		
		const_iterator begin() const
		{
			return _str;
		}

		const_iterator end() const
		{
			return _str + _size;
		}

		// �޸�
		void push_back(char c)
		{
			//if (_size == _capacity)
			//	reserve(_capacity == 0 ? 4 : 2 * _capacity); // ��������

			//_str[_size++] = c;
			//_str[_size] = '\0';

			// ���� insert
			insert(_size, c);
		}

		void append(const char* str)
		{
			//size_t len = strlen(str);
			//// �������
			//if (_size + len > _capacity)
			//	reserve(_size + len);

			//strcpy(_str + _size, str);
			//_size += len;

			// ���� insert
			insert(_size, str);
		}

		string& operator+=(char c)
		{
			push_back(c);
			return *this;
		}
		
		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		void insert(size_t pos, char c)
		{
			assert(pos <= _size);

			// �������
			if (_size == _capacity)
				reserve(_capacity == 0 ? 4 : 2 * _capacity); // ��������

			int i = _size + 1;
			while (i > pos)
			{
				_str[i] = _str[i - 1];
				i--;
			}

			_str[pos] = c;
			++_size;
		}

		void insert(size_t pos, const char* str)
		{
			assert(pos <= _size);

			size_t len = strlen(str);
			// �������
			if (_size + len > _capacity)
				reserve(_size + len);

			int i = _size + len;
			while (i >= pos + len)
			{
				_str[i] = _str[i - len];
				i--;
			}
			strncpy(_str + pos, str, len);
			_size += len;
		}

		void erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);

			if (len == npos || _size - pos <= len)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
		}

		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		size_t find(char ch, size_t pos = 0) const
		{
			assert(pos < _size);

			for (int i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
					return i;
			}
			return npos;
		}

		size_t find(const char* str, size_t pos = 0) const
		{
			assert(pos < _size);
			
			char* r = strstr(_str + pos, str);
			if (r)
				return r - _str;
			return npos;
		}

		string substr(size_t pos, size_t len = npos) const
		{
			string ret;
			if (len == npos || _size - pos <= len)
			{
				for (int i = pos; i < _size; i++)
					ret += _str[i];
			}
			else
			{
				for (int i = pos; i < pos + len; i++)
					ret += _str[i];
			}
			return ret;
		}

		// ����
		size_t size()
		{
			return _size;
		}

		size_t capacity()
		{
			return _capacity;
		}

		void resize(int n, char c = '\0')
		{
			// n < size������
			if (n < _size)
			{
				_str[n] = '\0';
				_size = n;
			}
			// n > size���޸ģ�˳��������
			else if (n > _size)
			{
				if (n > _capacity)
					reserve(n);
				for (int i = _size; i < n; i++)
				{
					_str[i] = c;
				}
				_str[n] = '\0';
				_size = n;
			}
		}
		void reserve(int n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);

				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}

		void clear()
		{
			_size = 0;
			_str[0] = '\0';
		}

		const char* c_str() const
		{
			return _str;
		}
	private:
		char* _str = nullptr;
		size_t _size = 0;
		size_t _capacity = 0;
	public:
		static const int npos;
	};
	const int string::npos = -1;

	void swap(string& s1, string& s2)
	{
		s1.swap(s2);
	}

	bool operator==(const string& s1, const string& s2)
	{
		int ret = strcmp(s1.c_str(), s2.c_str());
		return ret == 0;
	}

	bool operator<(const string& s1, const string& s2)
	{
		int ret = strcmp(s1.c_str(), s2.c_str());
		return ret < 0;
	}

	bool operator<=(const string& s1, const string& s2)
	{
		return s1 == s2 || s1 < s2;
	}

	bool operator>(const string& s1, const string& s2)
	{
		return !(s1 <= s2);
	}

	bool operator>=(const string& s1, const string& s2)
	{
		return !(s1 < s2);
	}

	bool operator!=(const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}


	ostream& operator<< (ostream& out, const string& s)
	{
		for (auto e : s)
		{
			out << e;
		}
		return out;
	}

	istream& operator>> (istream& in, string& s)
	{
		/*s.clear();
		char ch = in.get();
		while (ch != ' ' && ch != '\n')
		{
			s += ch;
			ch = in.get();
		}*/
		
		// �Ż�����������
		s.clear();
		char ch = in.get();
		char buff[128];
		int i = 0;
		while (ch != ' ' && ch != '\n')
		{
			buff[i++] = ch;
			if (i == 127)
			{
				buff[127] = '\0';
				s += buff;
				i = 0;
			}
			ch = in.get();
		}
		if (i > 0)
		{
			buff[i] = '\0';
			s += buff;
		}

		return in;
	}
	
	istream& getline (istream& in, string& s)
	{
		/*s.clear();
		char ch = in.get();
		while (ch != '\n')
		{
			s += ch;
			ch = in.get();
		}*/

		// �Ż�����������
		s.clear();
		char ch = in.get();
		char buff[128];
		int i = 0;
		while (ch != '\n')
		{
			buff[i++] = ch;
			if (i == 127)
			{
				buff[127] = '\0';
				s += buff;
				i = 0;
			}
			ch = in.get();
		}
		if (i > 0)
		{
			buff[i] = '\0';
			s += buff;
		}
		return in;
	}

	void test1()
	{
		string s1;
		string s2("hello world");
		string s3(s1);
		string s4(s2);
		string s5 = s2;
		string s6 = "hello world";

		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
		cout << s3.c_str() << endl;
		cout << s4.c_str() << endl;
		cout << s5.c_str() << endl;
		cout << s6.c_str() << endl;
	}

	void test2()
	{
		string s1 = "hello world!";
		const string s2 = "hello world!";
		for (int i = 0; i < s1.size(); i++)
			cout << s1[i];
		cout << endl;
		
		for (int i = 0; i < s1.size(); i++)
			cout << s1[i];
		cout << endl;
		cout << "--------------------------------------------------------------------------------" << endl;
		string::iterator it1 = s1.begin();
		string::const_iterator it2 = s2.begin();

		for (; it1 != s1.end(); it1++)
			cout << ++(*it1);
		cout << endl;
		for (; it2 != s2.end(); it2++)
			cout << *it2;
		cout << endl << endl;

		for (auto e : s1)
			cout << e;
		cout << endl;
		for (auto e : s2)
			cout << e;
	}

	void test3()
	{
		string s1 = "hello world";
		cout << s1.c_str() << endl;
		cout << "size:" << s1.size() << endl;
		cout << "capacity:" << s1.capacity() << endl;

		cout << endl;
		s1.resize(15, 'x');
		cout << s1.c_str() << endl;
		cout << "size:" << s1.size() << endl;
		cout << "capacity:" << s1.capacity() << endl;
		
		cout << endl;
		s1.resize(5);
		cout << s1.c_str() << endl;
		cout << "size:" << s1.size() << endl;
		cout << "capacity:" << s1.capacity() << endl;
	}

	void test4()
	{
		string s1 = "hello world";
		cout << s1.c_str() << endl;

		s1.push_back('x');
		cout << s1.c_str() << endl;

		s1.append("aaaaaa");
		cout << s1.c_str() << endl;

		s1 += 'b';
		cout << s1.c_str() << endl;

		s1 += "eeeeee";
		cout << s1.c_str() << endl;

	}

	void test5()
	{
		string s1 = "hello world";
		string s2 = "hello world";
		//s1.insert(11, 'x');
		s1.insert(11, "xxxxx");
		cout << s1.c_str() << endl;

		s2.erase(5);
		cout << s2.c_str() << endl;

	}

	void test6()
	{
		string s1 = "hello world";
		string s2 = "hello c++";

		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;

		swap(s1, s2);

		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
	}

	void test7()
	{
		string s1 = "hello world";
		int r1 = s1.find('o');
		int r2 = s1.find("wo");
		cout << s1.c_str() + r1 << endl;
		cout << s1.c_str() + r2 << endl;

		cout << endl;
		string s2 = s1.substr(6);
		cout << s2.c_str() << endl;

	}

	void test8()
	{
		string s1;
		string s2;
		string s3;
		cin >> s1 >> s2;
		getline(cin, s3);
		cout << s1 << s2 << endl;
		cout << s3 << endl;
	}
}
