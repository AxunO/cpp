#include <iostream>
using namespace std;
#include <string>
#include <thread>



//double Division(int x, int y)
//{
//	if (y == 0)
//	{
//		throw "Division by zero condition!";
//		cout << "throw 后面的代码" << endl;
//	}
//	else
//		return (double)x / (double)y;
//}
//
//void Func()
//{
//	int* p = new int(10);
//
//	int left = 0, right = 0;
//	cin >> left >> right;
//	try
//	{
//		cout << Division(left, right) << endl;
//	}
//	catch (...) 
//	{
//		cout << "delete p" << endl;
//		delete p;
//		throw;
//	}
//	cout << "delete p" << endl;
//	delete p;
//}
//
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (const char* e)
//	{
//		cout << "catched by main()：" << e << endl;
//	}
//	catch (...)
//	{
//		cout << "unknown exception!" << endl;
//	}
//	cout << "catch 后面的代码" << endl;
//	return 0;
//}


// 基类
class Exception
{
public:
	Exception(const char* str, int id)
		:_errmsg(str)
		, _id(id)
	{}

	virtual string what() const
	{
		return _errmsg;
	}
protected:
	string _errmsg;
	int _id;
};

// 派生类
class HttpServerException : public Exception
{
public:
	HttpServerException(const char* errmsg, int id, const char* type)
		:Exception(errmsg, id)
		, _type(type)
	{}

	virtual string what() const
	{
		string ret = "HttpServerException:";
		ret += _type;
		ret += ":";
		ret += _errmsg;
		return ret;
	}
private:
	const string _type;
};

// 派生类
class CacheException : public Exception
{
public:
	CacheException(const char* errmsg, int id)
		:Exception(errmsg, id)
	{}

	virtual string what() const
	{
		string ret = "CacheException:";
		ret += _errmsg;
		return ret;
	}
};

// 派生类
class SqlException : public Exception
{
public:
	SqlException(const char* errmsg, int id, const char* sql)
		:Exception(errmsg, id)
		, _sql(sql)
	{}

	virtual string what() const
	{
		string ret = "SqlException:";
		ret += _errmsg;
		ret += "->";
		ret += _sql;

		return ret;
	}
private:
	const string _sql;
};


void SQLMgr()
{
	srand(time(0));
	if (rand() % 7 == 0)
	{
		throw SqlException("权限不足", 100, "select * from name = '张三'");
	}
	else
	{
		cout << "Sql执行成功" << endl;
	}
}

void CacheMgr()
{
	srand(time(0));
	if (rand() % 5 == 0)
	{
		throw CacheException("权限不足", 100);
	}
	else if (rand() % 6 == 0)
	{
		throw CacheException("数据不存在", 101);
	}
	else
	{
		cout << "Cache获取成功" << endl;
	}
	SQLMgr();
}

void HttpServer()
{
	// ...
	srand(time(0));
	if (rand() % 3 == 0)
	{
		throw HttpServerException("请求资源不存在", 100, "get");
	}
	else if (rand() % 4 == 0)
	{
		throw HttpServerException("权限不足", 101, "post");
	}
	else
	{
		cout << "http调用成功" << endl;
	}
	CacheMgr();
}

int main()
{
	while (1)
	{
		// 使当前线程暂停一秒，避免频繁调用
		this_thread::sleep_for(chrono::seconds(1));

		try {
			HttpServer();
		}

		catch (const Exception& e) // 这里捕获父类对象就可以
		{
			// 多态
			cout << e.what() << endl;
		}
		catch (...)
		{
			cout << "Unkown Exception" << endl;
		}
	}
	return 0;
}