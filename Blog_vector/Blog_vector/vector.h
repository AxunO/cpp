#pragma once
#define _CRT_NO_SECURE_WARNINGS_

#include <iostream>
#include <assert.h>
#include <vector>
#include <string>
using namespace std;

namespace ns1
{
	template <class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		vector() {}

		~vector()
		{
			delete[] _start;
			_start = _finish = _endofstorage = nullptr;
		}

		template <class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		vector(size_t n, const T& val = T())
		{
			for (int i = 0; i < n; i++)
				push_back(val);
		}
		vector(int n, const T& val = T())
		{
			for (int i = 0; i < n; i++)
				push_back(val);
		}

		vector(const vector<T>& v)
		{
			reserve(v.capacity());
			for (auto& e : v)
				push_back(e);
		}

		vector<T>& operator= (vector<T> v)
		{
			swap(v);
			return *this;
		}

		size_t size() const
		{
			return _finish - _start;
		}

		size_t capacity() const
		{
			return _endofstorage - _start;
		}

		bool empty() const
		{
			return _start == _finish;
		}

		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t old_size = size();
				// 申请新空间，拷贝旧数据
				T* tmp = new T[n];
				// memcpy(tmp, _start, sizeof(T) * size());
				for (int i = 0; i < old_size; i++)
					tmp[i] = _start[i];
				delete[] _start;

				// 更新数据
				_start = tmp;
				_finish = tmp + old_size;
				_endofstorage = tmp + n;
			}
		}

		void resize(size_t n, const T& val = T())
		{
			if (n <= size())
			{
				// 删除数据
				_finish = _start + n;
			}
			else
			{
				// 填充数据
				reserve(n);

				while (_finish < _start + n)
				{
					*_finish = val;
					++_finish;
				}
			}
		}

		T& operator[](size_t n)
		{
			assert(n >= 0);
			assert(n < size());
			return _start[n];
		}
		const T& operator[](size_t n) const
		{
			assert(n >= 0);
			assert(n < size());
			return _start[n];
		}

		iterator begin()
		{
			return _start;
		}
		const_iterator begin() const
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}
		const_iterator end() const
		{
			return _finish;
		}

		void push_back(const T& val)
		{
			//// 检查扩容
			//if (_finish == _endofstorage)
			//{
			//	size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
			//	reserve(newcapacity);
			//}

			//// 插入数据
			//*_finish = val;
			//++_finish;

			insert(end(), val);
		}

		void pop_back()
		{
			//assert(!empty());
			//--_finish;
			erase(end() - 1);
		}

		iterator insert(iterator pos, const T& val)
		{
			assert(pos >= _start);
			assert(pos <= _finish);

			// 检查扩容
			if (_finish == _endofstorage)
			{
				// pos相对位置
				size_t len = pos - _start;

				size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
				reserve(newcapacity);

				// 更新pos
				pos = _start + len;
			}

			// 挪动数据
			iterator cur = end();
			while (cur > pos)
			{
				*cur = *(cur - 1);
				--cur;
			}

			// 更新数据
			*pos = val;
			++_finish;

			return pos;
		}

		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos < _finish);

			// 挪动数据
			iterator cur = pos + 1;
			while (cur != _finish)
			{
				*(cur - 1) = *cur;
				++cur;
			}
			--_finish;

			return pos;
		}

		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endofstorage, v._endofstorage);
		}
	private:
		iterator _start = nullptr;
		iterator _finish = nullptr;
		iterator _endofstorage = nullptr;
	};

	template <class T>
	void PrintVector(const vector<T>& v)
	{
		for (int i = 0; i < v.size(); i++)
			cout << v[i] << " ";
		cout << endl;
	}
	void test1()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		PrintVector(v1);
	}
	
	void test2()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		PrintVector(v1);
		cout << v1.size() << endl;
		cout << v1.capacity() << endl;
		// 填充
		v1.resize(20);
		PrintVector(v1);
		cout << v1.size() << endl;
		cout << v1.capacity() << endl;
		// 删除
		v1.resize(2);
		PrintVector(v1);
		cout << v1.size() << endl;
		cout << v1.capacity() << endl;
	}
	void test3()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);

		// 迭代器遍历
		vector<int>::iterator it1 = v1.begin();
		for (; it1 != v1.end(); it1++)
			cout << *it1 << " ";
		cout << endl;
		// 范围 for
		for (auto& e : v1)
			cout << e << " ";
		cout << endl;
	}
	
	void test4()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		PrintVector(v1);

		v1.pop_back();
		PrintVector(v1);
		v1.pop_back();
		PrintVector(v1);
		v1.pop_back();
		PrintVector(v1);
		v1.pop_back();
		PrintVector(v1);
		v1.pop_back();
		PrintVector(v1);
	}

	void test5()
	{
		vector<int> v1;
		v1.push_back(1);
		PrintVector(v1);
		// 头插
		v1.insert(v1.begin(), 10);
		PrintVector(v1);
		// 尾插
		v1.insert(v1.end(), 30);
		PrintVector(v1);
		// 中间插
		v1.insert(v1.begin() + 1, 20);
		PrintVector(v1);
		// 尾插
		v1.insert(v1.end(), 100);
		PrintVector(v1);
		// 尾插
		v1.insert(v1.end(), 100);
		PrintVector(v1);
		// 尾插
		v1.insert(v1.end(), 100);
		PrintVector(v1);
	}

	void test6()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);
		v1.push_back(6);
		PrintVector(v1);

		// 头删
		v1.erase(v1.begin());
		PrintVector(v1);
		// 尾删
		v1.erase(v1.end()-1);
		PrintVector(v1);
		// 中间删
		v1.erase(v1.begin()+1);
		PrintVector(v1);
	}

	void test7()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		PrintVector(v1);


		vector<int> v2;
		v2.push_back(5);
		v2.push_back(6);
		v2.push_back(7);
		v2.push_back(8);
		PrintVector(v2);

		v1.swap(v2);
		PrintVector(v1);
		PrintVector(v2);
	}

	void test8()
	{
		string s1 = "abcd";
		vector<char> v1(s1.begin(), s1.end());
		PrintVector(v1);
	}

	void test9()
	{
		/*vector<int> v1(10, 1);
		vector<int> v2(v1);*/

		vector<int> v1(10, 1);
		vector<int> v2(10, 2);
		v1 = v2;
		PrintVector(v1);
		PrintVector(v2);
	}

	void test10()
	{
		vector<string> v1;
		v1.push_back("一");
		v1.push_back("二");
		v1.push_back("三");
		v1.push_back("四");
		v1.push_back("五");

		PrintVector(v1);
	}
}