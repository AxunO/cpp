#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string.h>
#include <assert.h>
using namespace std;

namespace ns1
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		// 默认构造
		string(const char* str = "")
		{
			_size = _capacity = strlen(str);
			_str = new char[_size + 1];

			// 拷贝数据
			strcpy(_str, str);
		}
		
		// 析构
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}

		// 拷贝构造
		string(const string& s)
		{
			// 传统写法
			//_str = new char[s._capacity + 1];
			//strcpy(_str, s._str);
			//_size = s._size;
			//_capacity = s._capacity;

			// 现代写法
			string tmp(s.c_str());
			swap(tmp);
		}

		// 传统写法
		/*string& operator=(const string& s)
		{
			char* tmp = new char[s._capacity + 1];
			strcpy(tmp, _str);

			delete[] _str;
			_str = tmp;
			_size = s._size;
			_capacity = s._capacity;

			return *this;
		}*/
		
		// 现代写法
		string& operator=(string tmp)
		{
			swap(tmp);
			return *this;
		}

		// 容量
		size_t size() const
		{
			return _size;
		}

		size_t capacity() const
		{
			return _capacity;
		}

		bool empty() const
		{
			return _size == 0;
		}

		void clear()
		{
			_size = 0;
			_str[_size] = '\0';
		}

		void reserve(size_t n = 0)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);

				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}

		void resize(size_t n, char c = '\0')
		{
			// 删除
			if (n <= _size)
			{
				_str[n] = '\0';
				_size = n;
			}
			else // 填充
			{
				// 需不需要扩容，交给 reserve 去判断
				reserve(n);

				for (int i = _size; i < n; i++)
					_str[i] = c;
				_str[n] = '\0';
				_size = n;
			}
		}

		// 遍历
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}
		
		const char& operator[](size_t pos) const
		{
			assert(pos < _size);
			return _str[pos];
		}

		iterator begin()
		{
			return _str;
		}

		const_iterator begin() const
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}
		
		const_iterator end() const
		{
			return _str + _size;
		}

		// 修改
		void push_back(char c)
		{
			//// 检查扩容
			//if (_size == _capacity)
			//	reserve(_capacity == 0 ? 4 : 2 * _capacity);

			//_str[_size] = c;
			//++_size;
			//_str[_size] = '\0';

			insert(_size, c);
		}

		void append(const char* str)
		{
			/*size_t len = strlen(str);
			if (_size + len > _capacity)
				reserve(_size + len);

			strcpy(_str + _size, str);
			_size += len;*/

			insert(_size, str);
		}

		string& operator+=(char c)
		{
			push_back(c);
			return *this;
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		void insert(size_t pos, char c)
		{
			assert(pos <= _size); // 支持尾插

			// 检查扩容
			if (_size == _capacity)
				reserve(_capacity == 0 ? 4 : 2 * _capacity);

			// 挪动
			size_t end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				--end;
			}

			_str[pos] = c;
			++_size;
		}

		void insert(size_t pos, const char* str)
		{
			// 检查容量
			size_t len = strlen(str);
			if (_size + len > _capacity)
				reserve(_size + len);

			// 挪动
			size_t end = _size + len;
			while (end >= pos + len)
			{
				_str[end] = _str[end - len];
				--end;
			}

			// 更新数据
			strncpy(_str + pos, str, len);
			_size += len;
		}

		void erase(size_t pos = 0, size_t len = npos)
		{
			assert(pos < _size);

			if (len == npos || len > _size - pos) // pos + len可能会溢出
			{
				// 从 pos 开始全部删除
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				// 部分删除
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
		}

		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		size_t find(char c, size_t pos = 0) const
		{
			assert(pos < _size);

			for (int i = pos; i < _size; i++)
				if (_str[i] == c)
					return i;
			return npos;
		}

		size_t find(const char* s, size_t pos = 0) const
		{
			const char* ret = strstr(_str + pos, s);
			if (ret)
				return ret - _str; // 返回下标
			return npos;
		}

		string substr(size_t pos = 0, size_t len = npos) const
		{
			string tmp;

			// 全部截取
			if (len == npos || len > _size - pos)
			{
				for (int i = pos; i < _size; i++)
					tmp += _str[i];
			}
			else
			{
				// 部分截取
				for (int i = pos; i < pos + len; i++)
					tmp += _str[i];
			}
			return tmp;
		}

		const char* c_str() const
		{
			return _str;
		}
	private:
		char* _str = nullptr;
		size_t _size = 0;
		size_t _capacity = 0;
	public:
		static int npos;
	};
	int string::npos = -1;

	// 全局函数
	void swap(string& s1, string& s2)
	{
		s1.swap(s2);
	}

	bool operator==(const string& s1, const string& s2)
	{
		int ret = strcmp(s1.c_str(), s2.c_str());
		return ret == 0;
	}
	
	bool operator<(const string& s1, const string& s2)
	{
		int ret = strcmp(s1.c_str(), s2.c_str());
		return ret < 0;
	}
	
	bool operator<=(const string& s1, const string& s2)
	{
		return s1 == s2 || s1 < s2;
	}
	
	bool operator>(const string& s1, const string& s2)
	{
		return !(s1 <= s2);
	}

	bool operator>=(const string& s1, const string& s2)
	{
		return !(s1 < s2);
	}

	bool operator!=(const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}

	ostream& operator<<(ostream& os, const string& str)
	{
		for (auto e : str)
			os << e;
		return os;
	}

	istream& operator>> (istream& is, string& str)
	{
		str.clear();

		char ch;
		ch = is.get();

		char buff[128];
		int i = 0;
		while (ch != ' ' && ch != '\n')
		{
			// 字符放到buff
			buff[i++] = ch;
			// buff 满了，转移到string
			if (i == 127)
			{
				buff[i] = '\0';
				i = 0;
				str += buff;
			}
			ch = is.get();
		}

		// 清理buff
		if (i > 0)
		{
			buff[i] = '\0';
			str += buff;
		}

		return is;
	}

	istream& getline(istream& is, string& str)
	{
		str.clear();

		char ch;
		ch = is.get();

		char buff[128];
		int i = 0;
		while (ch != '\n')
		{
			// 字符放到buff
			buff[i++] = ch;
			// buff 满了，转移到string
			if (i == 127)
			{
				buff[i] = '\0';
				i = 0;
				str += buff;
			}
			ch = is.get();
		}

		// 清理buff
		if (i > 0)
		{
			buff[i] = '\0';
			str += buff;
		}

		return is;
	}

	void test1()
	{
		string s1;
		string s2("hello world");
		string s3(s2);
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
		cout << s3.c_str() << endl;
	}

	void test2()
	{
		/*string s1("hello world");
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl;
		cout << s1.empty() << endl;*/

		string s1("hello world");
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl;

		s1.clear();
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl;
	}

	void test3()
	{
		// 原始
		string s1("hello world");
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl << endl;

		// n < _capacity
		s1.reserve(1);
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl << endl;
		
		// n > _capacity
		s1.reserve(20);
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl << endl;
	}

	void test4()
	{
		// 原始
		string s1("hello world");
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl << endl;

		// 扩容填充
		s1.resize(20, 'x');
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl << endl;

		// 删除
		s1.resize(10);
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl << endl;
	}

	void test5()
	{
		//string s1("hello world");
		//// 遍历 + 修改
		//for (int i = 0; i < s1.size(); i++)
		//	cout << s1[i]++;
		//cout << endl;
		//// 遍历
		//for (int i = 0; i < s1.size(); i++)
		//	cout << s1[i];
		//cout << endl;
		
		//string s1("hello world");
		//string::iterator it1 = s1.begin();
		//// 遍历 + 修改
		//for (it1; it1 != s1.end(); it1++)
		//	cout << (*it1)++;
		//cout << endl;
		//// 遍历
		//for (it1 = s1.begin(); it1 != s1.end(); it1++)
		//	cout << (*it1);
		//cout << endl;

		string s1("hello world");
		for (auto e : s1)
			cout << e;
		cout << endl;

	}

	void test6()
	{
		/*string s1("hello world");
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl << endl;*/

		/*s1.push_back('x');
		cout << s1.c_str() << endl;
		cout << s1.size() << endl;
		cout << s1.capacity() << endl << endl;*/

		//string s1("hello world");
		//cout << s1.c_str() << endl;
		//cout << s1.size() << endl;
		//cout << s1.capacity() << endl << endl;


		//s1.append("xxxxx");
		//cout << s1.c_str() << endl;
		//cout << s1.size() << endl;
		//cout << s1.capacity() << endl << endl;

		string s1("hello world");
		string s2("hello world");

		s1 += 'x';
		s2 += "xxxxx";
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;

	}

	void test7()
	{
		//string s1("hello world");

		//s1.insert(0, 'a'); // 头插
		//cout << s1.c_str() << endl;

		//s1.insert(5, 'x'); // 中间插
		//cout << s1.c_str() << endl;

		//s1.insert(13, 't'); // 尾插
		//cout << s1.c_str() << endl;

		string s1("hello world");

		s1.insert(0, "aa"); // 头插
		cout << s1.c_str() << endl;

		s1.insert(5, "xx"); // 中间插
		cout << s1.c_str() << endl;

		s1.insert(15, "tt"); // 尾插
		cout << s1.c_str() << endl;
	}

	void test8()
	{
		string s1("hello world");

		// 删除中间一部分
		s1.erase(6, 2);
		cout << s1.c_str() << endl;

		// 从 pos 位置开始全部删除
		s1.erase(6);
		cout << s1.c_str() << endl;

	}

	void test9()
	{
		string s1("hello world");
		string s2("hello c++");

		swap(s1, s2);

		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
	}

	void test10()
	{
		string s1("hello world");
		size_t ret1 = s1.find('w', 5);
		size_t ret2 = s1.find("ld", 5);
		cout << s1[ret1] << endl;
		cout << s1[ret2] << endl;
	}

	void test11()
	{
		string s1("hello world");

		string s2 = s1.substr(0, 5);
		string s3 = s1.substr(6, 5);
		string s4 = s1.substr();

		cout << s2.c_str() << endl;
		cout << s3.c_str() << endl;
		cout << s4.c_str() << endl;
	}

	void test12()
	{
		/*string s1("hello world");
		cout << s1 << endl;*/
		/*string s1;
		getline(cin, s1);
		cout << s1 << endl;*/
		string s1;
		cin >> s1;
		cout << s1 << endl;
	}

	void test13()
	{
		string s1("hello world");
		string s2 = s1;
		cout << s2 << endl;
	}
}
