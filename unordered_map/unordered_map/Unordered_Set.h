#pragma once

#include "HashTable.h"

namespace ns1
{
	template <class K, class Hash = HashFunc<K>>
	class Unordered_Set
	{
		struct SetKeyOfT
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};
	public:
		typedef typename hash_bucket::HashTable<K, const K, SetKeyOfT, Hash>::iterator iterator;
		typedef typename hash_bucket::HashTable<K, const K, SetKeyOfT, Hash>::const_iterator const_iterator;

		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

		const_iterator begin() const
		{
			return _ht.begin();
		}

		const_iterator end() const
		{
			return _ht.end();
		}

		pair<iterator, bool> insert(const K& key)
		{
			return _ht.insert(key);
		}

		bool erase(const K& key)
		{
			return _ht.erase(key);
		}

		iterator find(const K& key)
		{
			return _ht.find(key);
		}
	private:
		hash_bucket::HashTable<K, const K, SetKeyOfT, Hash> _ht;
	};

	void Func(const Unordered_Set<int>& s)
	{
		Unordered_Set<int>::iterator it = s.begin();
		while (it != s.end())
		{
			//*it = 1;
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}

	void test_unordered_set()
	{
		Unordered_Set<int> s;
		s.insert(31);
		s.insert(11);
		s.insert(5);
		s.insert(15);
		s.insert(25);

		Unordered_Set<int>::iterator it = s.begin();
		while (it != s.end())
		{
			//*it = 1;
			cout << *it << " ";
			++it;
		}
		cout << endl;

		for (auto e : s)
		{
			cout << e << " ";
		}
		cout << endl;

		cout << "----------" << endl;
		int key;
		while (cin >> key)
		{
			Unordered_Set<int>::iterator ret = s.find(key);
			if (ret != s.end())
			{
				cout << *ret << endl;
			}
			else
			{
				cout << "不存在，请重新输入" << endl;
			}
		}
	}
}