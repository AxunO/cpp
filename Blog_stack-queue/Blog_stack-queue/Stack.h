#pragma once
#include <vector>

namespace ns1
{
	template <class T, class Container = vector<T> >
	class Stack
	{
	public:
		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}

		const T& top()
		{
			return _con.back();
		}

		void push(const T& val)
		{
			_con.push_back(val);
		}

		void pop()
		{
			_con.pop_back();
		}
	private:
		Container _con;
	};
}
