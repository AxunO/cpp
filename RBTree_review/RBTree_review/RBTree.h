#pragma once


enum Colour
{
	RED,
	BLACK
};

template <class K, class V>
struct RBTreeNode
{
	RBTreeNode(const pair<K, V>& kv)
		: _left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _col(RED)
	{}

	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	Colour _col;
};

template <class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	bool insert(const pair<K, V> kv)
	{
		// 树为空
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return true;
		}

		// 树不为空，寻找合适位置插入
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			if (kv.first > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
				return false; // 不允许键值重复
		}

		// 找到位置，插入新节点
		cur = new Node(kv);
		cur->_parent = parent;
		if (cur->_kv.first > parent->_kv.first)
			parent->_right = cur;
		else
			parent->_left = cur;

		// 检查是否平衡
		// 父节点存在且颜色为红，需要调整
		// 父节点不存在或者是黑色，就不需要调整
		while (parent && parent->_col == RED)
		{
			// 找出 g 和 u
			Node* grandpa = parent->_parent;
			Node* uncle = nullptr;
			if (parent == grandpa->_left)
				uncle = grandpa->_right;
			else
				uncle = grandpa->_left;

			if (uncle && uncle->_col == RED)
			{
				// 1. u 存在且为红，只需改变颜色即可
				parent->_col = uncle->_col = BLACK;
				grandpa->_col = RED;

				// 如果 g 为根节点，就结束了
				// 如果 g 不是根节点，继续向上更新
				cur = grandpa;
				parent = cur->_parent;
			}
			else if (uncle == nullptr || uncle->_col == BLACK)
			{
				// 2.u 不存在，或者 u 为黑
				
				// u 是 g 的左还是右，分两种情况
				if (uncle == grandpa->_right) // u 是 右
				{
					if (cur == parent->_left)
					{
						// c 是 p 的左
						//	   g
						//	 p   u
						// c
						// 右旋，更改颜色
						RotateR(grandpa);

						grandpa->_col = RED;
						parent->_col = BLACK;
					}
					else if (cur == parent->_right)
					{
						// c 是 p 的右
						//	   g
						//	 p   u
						//     c
						// 以 p 左旋，再以g右旋
						RotateL(parent);
						RotateR(grandpa);
						// 更改颜色

						grandpa->_col = RED;
						cur->_col = BLACK;
					}
				}
				else if (uncle == grandpa->_left) // u 是 左
				{
					if (cur == parent->_right)
					{
						// c 是 p 的右
						//    g
						// u     p
						//         c
						// 以 g 左旋
						RotateL(grandpa);
						grandpa->_col = RED;
						parent->_col = BLACK;
					}
					else if (cur == parent->_left)
					{
						// c 是 p 的左
						//    g
						// u     p
						//     c
						// 以 p 右旋，再以 g 左旋
						RotateR(parent);
						RotateL(grandpa);
						grandpa->_col = RED;
						cur->_col = BLACK;
					}
				}
				break;
			}
		}
		// 不管怎么调整，最后把根节点设为黑色
		_root->_col = BLACK;
		return true;
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		// 更改链接关系
		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;
		Node* pparent = parent->_parent;
		parent->_parent = subL;

		// 判断 parent 是根节点还是子树
		if (pparent)
		{
			// 是子树
			subL->_parent = pparent;
			if (parent == pparent->_right)
				pparent->_right = subL;
			else
				pparent->_left = subL;
		}
		else
		{
			// 是根节点
			_root = subL;
			_root->_parent = nullptr;
		}
	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		// 改变链接
		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* pparent = parent->_parent;
		parent->_parent = subR;

		// 判断 parent 是根节点还是子树
		if (pparent)
		{
			// 子树
			subR->_parent = pparent;
			if (parent == pparent->_right)
				pparent->_right = subR;
			else
				pparent->_left = subR;
		}
		else
		{
			// 根节点
			_root = subR;
			_root->_parent = nullptr;
		}
	}

	bool isBalance()
	{
		int refNum = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
				++refNum;
			cur = cur->_left;
		}

		return _isBalance(_root, 0, refNum);
	}
	void inOrder()
	{
		return _inOrder(_root);
	}
private:
	bool _isBalance(Node* root, int blackNum, int refNum)
	{
		if (root == nullptr)
		{
			if (blackNum != refNum)
			{
				cout << "存在黑色节点数量不相等的路径" << endl;
				return false;
			}
			return true;
		}

		// 查看是否是连续的红色节点
		if (root->_col == RED && root->_parent->_col == RED)
		{
			cout << root->_kv.first << "：出现连续红色节点" << endl;
			return false;
		}

		// 检测每条路径上的黑色节点数量
		if (root->_col == BLACK)
			++blackNum;

		return _isBalance(root->_left, blackNum, refNum) && _isBalance(root->_right, blackNum, refNum);
	}
	void _inOrder(Node* root)
	{
		if (root == nullptr) return;

		_inOrder(root->_left);
		cout << root->_kv.first << ": " << root->_kv.second << endl;
		_inOrder(root->_right);
	}
	Node* _root = nullptr;
};