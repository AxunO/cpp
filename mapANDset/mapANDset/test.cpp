#define _CRT_SECURE_NO_WARNINGS 1

#include <set>
#include <map>
#include <vector>
#include <string>
#include <utility>
#include <iostream>
#include <algorithm>
using namespace std;


class Solution1 {
public:
    struct kvComp
    {
        bool operator()(pair<string, int> p1, pair<string, int> p2)
        {
            return p1.second > p2.second
                || (p1.second == p2.second && p1.first < p2.first);
        }
    };

    vector<string> topKFrequent(vector<string>& words, int k)
    {
        map<string, int> m1;
        for (auto& e : words)
        {
            m1[e]++;
        }

        vector<pair<string, int>> v(m1.begin(), m1.end());
        sort(v.begin(), v.end(), kvComp());

        vector<string> ret;
        for (int i = 0; i < k; i++)
        {
            ret.push_back(v[i].first);
        }

        return ret;
    }
};


struct kvComp
{
    bool operator()(pair<string, int> p1, pair<string, int> p2)
    {
        return p1.second > p2.second ||
            (p1.second == p2.second && p1.first < p2.first);
    }
};
void test1()
{
    string str;
    getline(cin, str);

    // 大写转小写
    for (auto& e : str)
    {
        if (e >= 'A' && e <= 'Z')
            e = e + 32;
    }

    map<string, int> wordMap;
    // 取出单词
    string::iterator cur = str.begin();
    string::iterator start = str.begin();
    string::iterator end = str.begin();

    for (cur = str.begin(); cur < str.end(); cur++)
    {
        if (*cur == ' ' || *cur == '.')
        {
            end = cur;
            string tmp(start, end);
            wordMap[tmp]++;
            start = cur + 1;
        }
    }


    // 排序
    vector<pair<string, int>> v;
    for (auto& e : wordMap)
    {
        v.push_back(e);
    }

    sort(v.begin(), v.end(), kvComp());

    for (int i = 0; i < v.size(); i++)
    {
        cout << v[i].first << ":" << v[i].second << endl;
    }
}
int main()
{
    test1();
	return 0;
}