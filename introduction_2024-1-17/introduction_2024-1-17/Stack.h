#pragma once

#include <iostream>
using namespace std;

typedef struct Stack
{
	int* data;
	int size;
	int capacity;
}Stack;

void StackInit(Stack* ps, int capacity = 4);

inline void func(int i);

