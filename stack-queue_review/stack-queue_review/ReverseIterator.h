#pragma once
#include "list.h"

// 反向迭代器，
// 一个封装了正向迭代器的类
namespace ns1
{
	template <class Iterator, class Ref, class Ptr>
	struct RevereIterator
	{
		typedef RevereIterator <Iterator, Ref, Ptr> reverseIterator;

		RevereIterator(Iterator it)
			:_it(it)
		{}

		Ref operator*()
		{
			Iterator tmp(_it);
			--tmp;
			return *tmp;
		}

		Ptr operator->()
		{
			//return _it.operator->();
			return &(operator*());
		}

		// ++it
		reverseIterator& operator++()
		{
			--_it;
			return *this;
		}

		// --it
		reverseIterator& operator--()
		{
			++_it;
			return *this;
		}

		bool operator!=(const reverseIterator& rit)
		{
			return _it != rit._it;
		}
		Iterator _it;
	};
}
