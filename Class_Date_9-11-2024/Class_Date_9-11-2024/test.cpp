#include "Date.h"

int main()
{
	//Date d1(2024, 9, 11);
	//Date d2;
	//d2 = d1;
	//Date d3(2024, 9, 1);

	//cout << (d1 == d3) << endl;
	//cout << (d1 != d3) << endl;
	//cout << (d1 < d3) << endl;
	//cout << (d1 <= d3) << endl;
	//cout << (d1 > d3) << endl;
	//cout << (d1 >= d3) << endl;

	//Date d1(2024, 9, 11);
	//d1 += 10000;
	//d1.Print();

	//Date d2;
	//d2 = d1 + 10000;

	//d2.Print();

	/*Date d1(2024, 9, 11);
	d1 -= 1000;
	d1.Print();

	Date d2;
	d2 = d1 - 1000;

	d2.Print();*/

	//Date d1(2024, 9, 11);
	//Date d2 = d1++; // 9 11
	//Date d3 = ++d1; // 9 13

	//d1.Print();
	//d2.Print();
	//d3.Print();

	//Date d1(2024, 9, 11);
	//Date d2 = d1--; // 9 11
	//Date d3 = --d1; // 9 9

	//d1.Print();
	//d2.Print();
	//d3.Print();

	/*Date d1(2024, 9, 10);
	Date d2(2024, 9, 20);

	cout << (d1 - d2) << endl;*/

	Date d1, d2;
	cin >> d1 >> d2;
	cout << d1 << d2;
	return 0;
}