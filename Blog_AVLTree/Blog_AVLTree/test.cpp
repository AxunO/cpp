#include <iostream>
using namespace std;
#include "AVLTree.h"
#include <vector>
void test1()
{
	AVLTree<int, int> t;
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };

	for (auto e : a)
		t.insert({ e,e });
	t.inOrder();
	cout << "isBalance:" << t.isBalance() << endl;
}

void test2()
{
	const int N = 100000;
	vector<int> v;
	v.reserve(N);
	srand(time(0));

	for (size_t i = 0; i < N; i++)
	{
		v.push_back(rand() + i);
	}

	// 开始插入
	size_t begin1 = clock();
	AVLTree<int, int> t;
	for (auto e : v)
	{
		t.insert(make_pair(e, e));
	}
	// 插入结束
	size_t end1 = clock();

	cout << "Insert:" << end1 - begin1 << endl;
	cout << "isBalance:" << t.isBalance() << endl;
	cout << "Height:" << t.height() << endl;
	cout << "Size:" << t.size() << endl;

	size_t begin2 = clock();
	// 确定在的值
	for (auto e : v)
	{
		t.find(e);
	}
	size_t end2 = clock();

	cout << "Find:" << end2 - begin2 << endl;
}

int main()
{
	test2();
	return 0;
}