#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

typedef struct LNode
{
    int val;
    struct LNode* next;
}LNode;

LNode* NewNode(int x)
{
    LNode* tmp = (LNode*)malloc(sizeof(LNode));
    if (tmp == NULL)
    {
        perror("malloc fail");
        exit(-1);
    }

    tmp->val = x;
    return tmp;
}

// β��
void LinkListPush(LNode** pphead, int x)
{
    LNode* newnode = NewNode(x);

    if (*pphead == NULL)
    {
        *pphead = newnode;
        (*pphead)->next = *pphead;
    }
    else
    {
        LNode* cur = *pphead;
        while (cur->next != *pphead)
        {
            cur = cur->next;
        }

        cur->next = newnode;
        newnode->next = *pphead;
    }
}

// ɾ��
int LinkListDel(LNode* phead, int m)
{
    int i = 1;
    LNode* cur = phead;

    while (1)
    {
        if (cur->next == cur)
        {
            return cur->val;
        }

        if (i + 1 == m)
        {
            LNode* tmp = cur->next;
            cur->next = cur->next->next;
            free(tmp);
            i = 0;
        }
        cur = cur->next;
        i++;
    }
}

int main()
{
    int n, m;
    scanf("%d %d", &n, &m);
    LNode* phead = NULL;

    int i = 1;
    while (1)
    {
        if (i == n + 1)
        {
            break;
        }

        LinkListPush(&phead, i);
        i++;
    }

    int ret = LinkListDel(phead, m);
    printf("%d", ret);

    return 0;
}