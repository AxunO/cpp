#include "Date.h"

Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;
}

// < == <= != > >+
bool Date::operator<(const Date& d)
{
	if (_year < d._year)
		return true;
	else if (_year == d._year)
	{
		if (_month < d._month)
			return true;
		else if (_month == d._month)
		{
			if (_day < d._day)
				return true;
		}
	}
	return false;
}

bool Date::operator==(const Date& d)
{
	return _year == d._year && _month == d._month && _day == d._day;
}

bool Date::operator<=(const Date& d)
{
	return (*this < d) || (*this == d);
}

bool Date::operator!=(const Date& d)
{
	return !(*this == d);
}

bool Date::operator>(const Date& d)
{
	return !(*this <= d);
}

bool Date::operator>=(const Date& d)
{
	return !(*this < d);
}


// d+10
Date& Date::operator+=(int x)
{
	_day += x;
	// 进位处理
	while (_day > GetMonthDays(_year, _month))
	{
		_day -= GetMonthDays(_year, _month);
		_month++;
		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
	}
	return *this;
}

Date Date::operator+(int x)
{
	Date tmp(*this); // 拷贝构造
	tmp += x;
	return tmp;
}

//Date Date::operator+(int x)
//{
//	Date tmp(*this); // 拷贝消耗
//	tmp._day += x;
//
//	// 进位处理
//	while (tmp._day > GetMonthDays(tmp._year, tmp._month))
//	{
//		tmp._day -= GetMonthDays(tmp._year, tmp._month);
//		tmp._month++;
//		if (tmp._month == 13)
//		{
//			tmp._month = 1;
//			tmp._year++;
//		}
//	}
//	return tmp;
//}
//
//Date& Date::operator+=(int x)
//{
//	*this = *this + x; // 调用+，而+有拷贝消耗
//	return *this;
//}

// d-10
Date& Date::operator-=(int x)
{
	_day -= x;
	while (_day <= 0)
	{
		_month--;
		if (_month == 0)
		{
			_month = 12;
			_year--;
		}
		_day += GetMonthDays(_year, _month);
	}

	return *this;
}

Date Date::operator-(int x)
{
	Date tmp(*this);
	tmp -= x;
	return tmp;
}

// ++
Date& Date::operator++()
{
	*this += 1;
	return *this;
}
Date Date::operator++(int)
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}

//--
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}
Date Date::operator--(int)
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}

// d1-d2
int Date::operator-(Date& d)
{
	Date max(*this);
	Date min(d);
	int flag = 1;

	if (d > *this)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int cnt = 0;
	while (min != max)
	{
		min++;
		cnt++;
	}

	return cnt * flag;
}

// <<
ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "/" << d._month << "/" << d._day;
	return out;
}
// >>
istream& operator>>(istream& in, Date& d)
{
	while (1)
	{
		cout << "请输入年月日：";
		in >> d._year >> d._month >> d._day;
		if (d.CheckInvalid())
			break;
		else
			cout << "输入日期无效，请重新输入日期" << endl;
	}
	return in;
}