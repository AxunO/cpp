#include "string.h"

void test1()
{
	string s1;
	string s2("hello world");
	string s3(s2);

	cout << s1 << endl;
	cout << s2 << endl;
	cout << s3 << endl;
}

void test2()
{
	string s1("hello world");
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;
	cout << s1 << endl;

	s1.resize(20, 'x');
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;
	cout << s1 << endl;

}

void test3()
{
	string s1("hello world");
	/*for (int i = 0; i < s1.size(); i++)
		cout << s1[i];
	cout << endl;*/

	/*string::iterator it1 = s1.begin();
	for (it1; it1 != s1.end(); it1++)
		cout << *it1;
	cout << endl;*/

	for (auto e : s1)
		cout << e;
	cout << endl;
}


int main()
{
	ns1::test13();
	
}