#define _CRT_SECURE_NO_WARNINGS 1

#include "Stack.h"

//命名空间
//#include <stdio.h>
//#include <stdlib.h>
//
//namespace ns1
//{
//	int rand = 1;
//}
//int main()
//{
//	printf("%d\n", ns1::rand);
//	printf("%p\n", rand);
//	return 0;
//}

//#include <iostream>
//#include <stdio.h>
//
//namespace ns1
//{
//	int x = 1;
//}
//
//namespace ns2
//{
//	int x = 2;
//}
//int main()
//{
//	printf("ns1-x：%d\n", ns1::x);
//	printf("ns2-x：%d\n", ns2::x);
//	return 0;
//}


//#include <stdio.h>
//
//namespace N
//{
//	int a = 10;
//	char b = 'b';
//}
//
//using namespace N;
//
//int main()
//{
//	printf("%d\n", a);
//	printf("%c\n", b);
//	return 0;
//}


// C++的输入与输出

//int main()
//{
//	int a = 0;
//
//	cin >> a ;
//	std::cout << "a:" << a << std::endl;
//
//	return 0;
//}


// 缺省参数

//void Func(int a = 0)	// 缺省值为0
//{
//	cout << a << endl;
//}
//
//int main()
//{
//	Func();	//不传实参，形参为缺省值
//	Func(10);	//传实参，形参为实参的拷贝
//}

//void Func(int a = 10, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl << endl;
//}
//
//int main()
//{
//	Func(1, 2, 3);
//	Func(1, 2);
//	Func(1);
//	Func();
//}

//void Func(int a, int b = 20, int c = 30)
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl << endl;
//}
//
//int main()
//{
//	Func(1, 2, 3);
//	Func(1, 2);
//	Func(1);
//}

//int main()
//{
//	Stack st;
//	StackInit(&st);
//	return 0;
//}


// 函数重载
// 整数相加
//int Add(int a, int b)
//{
//	cout << "int Add(int a, int b)" << endl;
//	return a + b;
//}
//
//// 浮点数相加
//double Add(double a, double b)
//{
//	cout << "double Add(double a, double b)" << endl;
//	return a + b;
//}
//
//// 零参数
//void f()
//{
//	cout << "f()" << endl;
//}
//// 1参数
//void f(int a)
//{
//	cout << "f(int a)" << endl;
//}
//
////参数顺序不同
//void func(int a, char b)
//{
//	cout << "func(int a, char b)" << endl;
//}
//
//void func(char a, int b)
//{
//	cout << "func(char a, int b)" << endl;
//}
//int main()
//{
//	Add(1, 2);
//	Add(1.1, 2.2);
//
//	/*f();
//	f(1);*/
//
//	/*func(1, 'b');
//	func('a', 2);*/
//}

////////////////////////////////////////////////////////////////////////////////////
// 引用
//int main()
//{
//	int a = 0;
//	int& b = a;
//
//	cout << &a << endl;
//	cout << &b << endl;
//
//	a++;
//	b++;
//
//	cout << a << endl;
//	cout << b << endl;
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int& b = a;
//	int& c = a;
//	int& d = c; // 引用也可以有引用
//
//	cout << &a << endl;
//	cout << &b << endl;
//	cout << &c << endl;
//	cout << &d << endl;
//
//	a++;
//	b++;
//	c++;
//	d++;
//
//	cout << a << endl;
//	cout << b << endl;
//	cout << c << endl;
//	cout << d << endl;
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int b = 1;
//	int& c = a;
//
//	c = b;
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	const int& ra = a;
//
//	cout << "a:" << a << endl;
//	cout << "ra:" << ra << endl;
//
//	return 0;
//}

//void Swap(int* x, int* y)
//{
//	int tmp = *x;
//	*x = *y;
//	*y = tmp;
//}

//void Swap(int& x, int& y)
//{
//	int tmp = x;
//	x = y;
//	y = tmp;
//}
//
//int main()
//{
//	int a = 1;
//	int b = 2;
//	cout << "a:" << a << " b:" << b << endl;
//
//	Swap(a, b);
//	cout << "a:" << a << " b:" << b << endl;
//	return 0;
//}

//int& func()
//{
//	int a = 1;
//	return a;
//}
//int main()
//{
//	int &ret = func();
//	return 0;
//}

//typedef struct SList
//{
//	int* data;
//	int size;
//	int capacity;
//}SList;
//
//// 初始化
//void SLInit(SList* ps)
//{
//	ps->data = (int*)malloc(sizeof(int) * 4);
//	ps->capacity = 4;
//	ps->size = 0;
//}
//
//// 尾插
//void SLPushBack(SList* ps, int x)
//{
//	ps->data[ps->size] = x;
//	ps->size++;
//}
//
//// 取得pos位置的值
//int& SLGet(SList* ps, int pos)
//{
//	return ps->data[pos];
//}
//
//// 修改pos位置的值
//void SLModify(SList* ps, int pos, int x)
//{
//	ps->data[pos] = x;
//}
//
//int main()
//{
//	SList sl;
//	SLInit(&sl);
//
//	// 尾插
//	SLPushBack(&sl, 1);
//	SLPushBack(&sl, 2);
//	SLPushBack(&sl, 3);
//	SLPushBack(&sl, 4);
//
//	// 遍历查看
//	for (int i = 0; i < 4; i++)
//	{
//		cout << SLGet(&sl, i) << " ";
//	}
//	cout << endl;
//
//	// 偶数位乘二
//	for (int i = 0; i < 4; i++)
//	{
//		if (SLGet(&sl,i) % 2 == 0)
//		{
//			SLGet(&sl, i) *= 2;
//		}
//	}
//
//	// 遍历查看
//	for (int i = 0; i < 4; i++)
//	{
//		cout << SLGet(&sl, i) << " ";
//	}
//	cout << endl;
//}

//int main()
//{
//	int a = 0;
//
//	// 引用
//	int& ra = a;
//	// 指针
//	int* pa = &a;
//	return 0;
//}


////////////////////////////////////////////////////////////////////////////////
// 内联
#define ADD(a,b) ((a) + (b))

inline int Add(int x, int y)
{
	return x + y;
}

//int Add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	int a = 1;
//	int b = 1;
//	int ret = Add(a, b);
//	cout << "ret:" << ret << endl;
//}

//int main()
//{
//	func(10);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	auto aa = 0;
//	char b = 'a';
//	auto ab = 'a';
//	double c = 1.1;
//	auto ac = 1.1;
//
//	cout << typeid(a).name() << endl;
//	cout << typeid(aa).name() << endl;
//	cout << typeid(b).name() << endl;
//	cout << typeid(ab).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(ac).name() << endl;
//	return 0;
//}

//int main()
//{
//	int a = 0;
//
//	// 指针
//	auto pa1 = &a;
//	auto* pa2 = &a;
//
//	// 引用
//	auto& ra = a;
//
//	cout << typeid(pa1).name() << endl;
//	cout << typeid(pa2).name() << endl;
//	cout << typeid(ra).name() << endl;
//}

//void TestAuto(auto a)
//{
//    cout << a << endl;
//}

//int main()
//{
//    //TestAuto(10);
//
//    int arr[] = { 1,2,3 };
//    auto arr2[] = { 4,5,6 };
//    return 0;
//}


//int main()
//{
//	/*int array[] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
//	{
//		cout << array[i] << " ";
//	}
//	cout << endl;*/
//
//	int array[] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (auto& i : array)
//	{
//		i *= 2;
//		cout << i << " ";
//	}
//	cout << endl;
//}

//void f(int)
//{
//	cout << "f(int)" << endl;
//}
//
//void f(int*)
//{
//	cout << "f(int*)" << endl;
//}
//
//int main()
//{
//	f(0);
//	f(NULL);
//}

void f(int)
{
	cout << "f(int)" << endl;
}

void f(int*)
{
	cout << "f(int*)" << endl;
}

int main()
{
	f(0);
	f(nullptr);
}