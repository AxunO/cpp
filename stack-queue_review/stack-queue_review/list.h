#pragma once

#include "ReverseIterator.h"
namespace ns1
{
	template <class T>
	struct ListNode
	{
		ListNode(const T& val = T())
			:_next(nullptr)
			,_prev(nullptr)
			,_data(val)
		{}
		ListNode* _next;
		ListNode* _prev;
		T _data;
	};

	template <class T, class Ref, class Ptr>
	struct ListIterator
	{
		typedef ListNode<T> Node;

		ListIterator(Node* node)
			:_node(node)
		{}

		// 引用
		Ref operator*()
		{
			return _node->_data;
		}
		// 指针
		Ptr operator->()
		{
			return &(_node->_data);
		}

		// ++it
		ListIterator& operator++()
		{
			_node = _node->_next;
			return *this;
		}
		// it++
		ListIterator operator++(int)
		{
			ListIterator tmp(_node);
			_node = _node->_next;
			return tmp;
		}

		// --it
		ListIterator& operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		// it--
		ListIterator operator--(int)
		{
			ListIterator tmp(_node);
			_node = _node->prev;
			return tmp;
		}

		bool operator==(const ListIterator& it)
		{
			return _node == it._node;
		}

		bool operator!=(const ListIterator& it)
		{
			return _node != it._node;
		}

		Node* _node;
	};

	template <class T>
	class list
	{
		typedef ListNode<T> Node;
	public:
		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const T&, const T*> const_iterator;
		typedef RevereIterator<iterator, T&, T*> reverse_iterator;
		//typedef RevereIterator<const_iterator, const T&, const T*> const_reverse_iterator;

		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		iterator begin()
		{
			return _head->_next;
		}
		
		iterator end()
		{
			return _head;
		}
		// const迭代器
		const_iterator begin() const
		{
			return _head->_next;
		}
		
		const_iterator end() const
		{
			return _head;
		}

		// 开头节点
		void empty_init()
		{
			_head = new Node;
			_head->_next = _head;
			_head->_prev = _head;
			_size = 0;
		}
		// 构造
		list()
		{
			empty_init();
		}
		~list()
		{
			clear();
			delete _head;
		}

		// 拷贝构造
		list(const list& x)
		{
			empty_init();
			for (auto& e : x)
				push_back(e);
		}

		list& operator=(list tmp) // 传值传参
		{
			swap(tmp);
			return *this;
		}

		size_t size() const
		{
			return _size;
		}

		bool empty()
		{
			return _size == 0;
		}

		iterator insert(iterator pos, const T& val)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* newnode = new Node(val);

			// prev newnode cur
			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;
			++_size;

			// 返回新插入第一个元素的迭代器
			return newnode;
		}

		iterator erase(iterator pos)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* next = cur->_next;

			// pre next
			prev->_next = next;
			next->_prev = prev;
			delete cur;

			--_size;
			return next;
		}

		void push_back(const T& val)
		{
			insert(end(), val);
		}
		void push_front(const T& val)
		{
			insert(begin(), val);
		}
		
		void pop_back()
		{
			erase(--end());
		}
		void pop_front()
		{
			erase(begin());
		}

		void swap(list<T>& lt)
		{
			std::swap(_head, lt._head);
			std::swap(_size, lt._size);
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}
	private:
		Node* _head;
		size_t _size;
	};


	void PrintList(const list<int>& lt);
	//void test1()
	//{
	//	list<int> lt1;
	//	lt1.push_back(1);
	//	lt1.push_back(2);
	//	lt1.push_back(3);
	//	lt1.push_back(4);

	//	// 输出
	//	list<int>::iterator it = lt1.begin();
	//	while (it != lt1.end())
	//	{
	//		cout << *it << " ";
	//		++it;
	//	}
	//	cout << endl;
	//}

	//
	//void test2()
	//{
	//	//list<int> lt1;
	//	//// 头插
	//	//lt1.insert(lt1.begin(), 10);
	//	//// 尾插
	//	//lt1.insert(lt1.end(), 30);
	//	//// 中间插
	//	//// 迭代器没有重载 +- 操作，要使用++
	//	//lt1.insert(++lt1.begin(), 20);

	//	list<int> lt1;
	//	lt1.push_back(1);
	//	lt1.push_back(2);
	//	lt1.push_back(3);
	//	lt1.push_back(4);
	//	lt1.push_back(5);
	//	PrintList(lt1);

	//	//头删
	//	lt1.erase(lt1.begin());
	//	PrintList(lt1);
	//	// 尾删
	//	lt1.erase(--lt1.end());
	//	PrintList(lt1);
	//	// 中间
	//	lt1.erase(++lt1.begin());
	//	PrintList(lt1);
	//}

	//void test3()
	//{
	//	list<int> lt1;
	//	lt1.push_back(1);
	//	lt1.push_back(2);
	//	lt1.push_back(3);
	//	lt1.push_back(4);

	//	list<int> lt2;
	//	lt2.push_back(5);
	//	lt2.push_back(6);
	//	lt2.push_back(7);
	//	lt2.push_back(8);

	//	lt1.swap(lt2);
	//	PrintList(lt1);
	//	PrintList(lt2);

	//}

	//void test4()
	//{
	//	list<int> lt1;
	//	lt1.push_back(1);
	//	lt1.push_back(2);
	//	lt1.push_back(3);
	//	lt1.push_back(4);
	//	PrintList(lt1);

	//	lt1.clear();
	//	PrintList(lt1);
	//}

	//struct A
	//{
	//	A(int a1 = 0, int a2 = 0)
	//		:_a1(a1)
	//		,_a2(a2)
	//	{}

	//	int _a1;
	//	int _a2;
	//};

	//void test5()
	//{
	//	list<A> lt1;
	//	A aa1(1, 1);
	//	lt1.push_back(aa1); // 有名对象
	//	lt1.push_back(A(2, 2)); // 匿名对象
	//	lt1.push_back({3, 3}); // 多参数的构造函数的隐式类型转换
	//	lt1.push_back({4, 4}); // 多参数的构造函数的隐式类型转换

	//	// 输出
	//	list<A>::iterator it = lt1.begin();
	//	while (it != lt1.end())
	//	{
	//		// cout << (*it)._a1 << ":" << (*it)._a2 << endl;
	//		cout << it->_a1 << ":" << it->_a2 << endl;
	//		++it;
	//	}
	//}

	void PrintList(list<int>& lt)
	{
		// 输出
		list<int>::iterator it = lt.begin(); // const迭代器
		while (it != lt.end())
		{
			//(*it) += 10;
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}

	void test6()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);

		list<int> lt2;
		lt2 = lt1;
		PrintList(lt2);
	}
}
