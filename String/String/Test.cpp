﻿#define _CRT_SECURE_NO_WARNINGS 1

#include "String.h"


//void Test1()
//{
//	string s1("hello world");
//	string s2 = s1;
//
//	cout << s1.c_str() << endl;
//	cout << s2.c_str() << endl;
//
//	if (s1.c_str() == s2.c_str())
//	{
//		cout << 1 << endl;
//	}
//	else
//	{
//		cout << 0 << endl;
//	}
//}
//int main()
//{
//	Test1();
//	return 0;
//}


// 字符串转换数字
//#include <cmath>
//class Solution {
//public:
//    int myAtoi(string str)
//    {
//        string retstr;
//        int pm = 1;
//
//        // 到非空格位置
//        string::iterator it1 = str.begin();
//        while (*it1 == ' ' && it1 != str.end())
//        {
//            ++it1;
//        }
//
//        // 正负号
//        if (*it1 == '-')
//        {
//            pm = -1;
//            ++it1;
//        }
//        else if (*it1 == '+')
//        {
//            pm = 1;
//            ++it1;
//        }
//
//        // 跳过无用的0
//        while (*it1 == '0' && it1 != str.end())
//        {
//            ++it1;
//        }
//
//        // int flag = 1;
//        while (it1 != str.end())
//        {
//            if (*it1 > '9' || *it1 < '0')
//            {
//                break;
//            }
//
//            retstr += *it1;
//            it1++;
//        }
//
//        if (retstr.size() > 10)
//        {
//            if (pm == 1)
//            {
//                return INT_MAX;
//            }
//            if (pm == -1)
//            {
//                return INT_MIN;
//            }
//        }
//
//        // 字符串转换
//        int end = retstr.size() - 1;
//        long ret = 0;
//        int count = 0;
//        while (end >= 0)
//        {
//            ret += pow(10, count) * (retstr[end--] - '0');
//            count++;
//        }
//
//        ret *= pm;
//        if (ret > INT_MAX)
//        {
//            return INT_MAX;
//        }
//        if (ret < INT_MIN)
//        {
//            return INT_MIN;
//        }
//
//        return ret;
//    }
//};


// 字符串相加
//class Solution2 {
//public:
//    string addStrings(string num1, string num2) {
//        int pos1 = num1.size() - 1, pos2 = num2.size() - 1;
//        string retstr;
//        int ret = 0;
//        int carry = 0;
//
//        while (pos1 >= 0 || pos2 >= 0)
//        {
//            int val1;
//            int val2;
//            if (pos1 < 0)
//            {
//                val1 = 0;
//            }
//            else
//            {
//                val1 = num1[pos1--] - '0';
//            }
//
//            if (pos2 < 0)
//            {
//                val2 = 0;
//            }
//            else
//            {
//                val2 = num2[pos2--] - '0';
//            }
//
//            ret = (val1 + val2 + carry) % 10;
//            carry = (val1 + val2 + carry) / 10;
//            retstr += (ret + '0');
//        }
//
//        if (carry == 1)
//        {
//            retstr += '1';
//        }
//
//        reverse(retstr.begin(), retstr.end());
//        return retstr;
//    }
//};
//
//class Solution3 {
//public:
//    int firstUniqChar(string s) {
//        int array[26];
//        memset(array, 0, sizeof(int) * 26);
//        string::iterator it = s.begin();
//        while (it != s.end())
//        {
//           array[*it - 'a']++;
//            it++;
//        }
//        for (int i = 0; i < 26; i++)
//        {
//            if (array[i] == 1)
//            {
//                return s.find(array[i], 0);
//            }
//        }
//
//        return -1;
//    }
//};


//int main()
//{
//    string strText = "How are you?";
//
//    string strSeparator = " ";
//
//    string strResult;
//
//    int size_pos = 0;
//
//    int size_prev_pos = 0;
//
//    while ((size_pos = strText.find_first_of(strSeparator, size_pos)) != string::npos)
//
//    {
//
//        strResult = strText.substr(size_prev_pos, size_pos - size_prev_pos);
//
//        cout << strResult << " ";
//
//        size_prev_pos = ++size_pos;
//
//    }
//
//    if (size_prev_pos != strText.size())
//
//    {
//
//        strResult = strText.substr(size_prev_pos, size_pos - size_prev_pos);
//
//        cout << strResult << " ";
//
//    }
//
//    cout << endl;
//
//    return 0;
//}


//class Solution4
//{
//public:
//    string reverseStr(string s, int k)
//    {
//        int start = 0, cur = 0;
//        int count = 1;
//        while (cur < s.size() - 1)
//        {
//            count++;
//            cur++;
//
//            if (count == 2 * k)
//            {
//                int end = start + k - 1;
//                while (start < end)
//                {
//                    swap(s[start++], s[end--]);
//                }
//                start = cur + 1;
//                cur = start;
//                count = 1;
//            }
//        }
//
//        string edstr = s.substr(start);
//        int size = edstr.size();
//        if (size < k && size > 0)
//        {
//            int end = size - 1;
//            while (start < end)
//            {
//                swap(s[start++], s[end--]);
//            }
//        }
//        else if (size < 2 * k && size > 0 && (size > k || size == k))
//        {
//            int end = start + k - 1;
//            while (start < end)
//            {
//                swap(s[start++], s[end--]);
//            }
//        }
//        return s;
//    }
//};
//
//class Solution5 {
//public:
//    string multiply(string num1, string num2)
//    {
//        if (num1 == "0" || num2 == "0")
//        {
//            return "0";
//        }
//
//        if (num1.size() < num2.size())
//        {
//            swap(num1, num2);
//        }
//
//        string ans;
//        int pos1 = num1.size() - 1;
//        int pos2 = num2.size() - 1;
//        int next = 0;
//        int count = 0;
//        while (pos2 >= 0)
//        {
//            string retstr;
//            for (int i = 0; i < count; i++)
//            {
//                retstr += '0';
//            }
//
//            pos1 = num1.size() - 1;
//            next = 0;
//            while (pos1 >= 0)
//            {
//                int val1 = num1[pos1--] - '0';
//                int val2 = num2[pos2] - '0';
//                retstr += (val1 * val2 + next) % 10 + '0';
//                next = (val1 * val2 + next) / 10;
//            }
//
//            if (next != 0)
//            {
//                retstr += (next + '0');
//            }
//
//            reverse(retstr.begin(), retstr.end());
//            ans = addStrings(ans, retstr);
//
//            count++;
//            pos2--;
//        }
//        return ans;
//
//    }
//
//    string addStrings(string num1, string num2) {
//        int pos1 = num1.size() - 1, pos2 = num2.size() - 1;
//        string retstr;
//        int ret = 0;
//        int carry = 0;
//
//        while (pos1 >= 0 || pos2 >= 0)
//        {
//            int val1;
//            int val2;
//            if (pos1 < 0)
//            {
//                val1 = 0;
//            }
//            else
//            {
//                val1 = num1[pos1--] - '0';
//            }
//
//            if (pos2 < 0)
//            {
//                val2 = 0;
//            }
//            else
//            {
//                val2 = num2[pos2--] - '0';
//            }
//
//            ret = (val1 + val2 + carry) % 10;
//            carry = (val1 + val2 + carry) / 10;
//            retstr += (ret + '0');
//        }
//
//        if (carry == 1)
//        {
//            retstr += '1';
//        }
//
//        reverse(retstr.begin(), retstr.end());
//        return retstr;
//    }
//};

int main()
{
    bit::Test9();
}