#pragma once

namespace key
{
	template <class K>
	struct BSTreeNode
	{
		BSTreeNode(const K& key = K())
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
		{}

		BSTreeNode<K>* _left;
		BSTreeNode<K>* _right;
		K _key;
	};


	template <class K>
	class BSTree
	{
		typedef BSTreeNode<K> Node;
	public:
		bool insert(const K& key)
		{
			// 树为空
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}

			// 树不为空
			// 寻找合适插入位置
			Node* cur = _root, * parent = nullptr;
			while (cur)
			{
				if (key > cur->_key)
				{
					// 插入值 > 当前节点
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					// 插入值 < 当前节点
					parent = cur;
					cur = cur->_left;
				}
				else
					return false; // 不允许插入重复值
			}
			// 找到插入位置
			cur = new Node(key);
			// 判断 cur 是 parent 的左子树还是右子树，链接
			if (cur->_key > parent->_key)
				parent->_right = cur;
			else
				parent->_left = cur;
			return true;
		}

		bool find(const K& key)
		{
			// 树为空
			if (_root == nullptr)
				return false;

			// 树不为空
			Node* cur = _root;
			while (cur)
			{
				if (key > cur->_key)
				{
					// key值 > 当前节点
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					// 插入值 < 当前节点
					cur = cur->_left;
				}
				else
					return true; // 找到
			}
			// 找不到
			return false;
		}

		bool erase(const K& key)
		{
			// 树为空
			if (_root == nullptr)
				return false;

			// 树非空，找到指定值并删除
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					// 找到，删除
					// 1.a cur 的左子树为空，parent 接管 cur 的右子树
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							// 判断 cur 是左子树还是右子树
							if (cur == parent->_left)
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}
						delete cur;
					}
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							// 1.b cur 的右子树为空，parent接管 cur 的左子树
							// 判断 cur 为左还是右
							if (cur == parent->_left)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}
						delete cur;
					}
					else
					{
						// 2. cur左右不为空，替换法
						// 找到cur右子树的最左节点 rightMin，还有它的父节点 rightMinParent
						Node* rightMinParent = cur;
						Node* rightMin = cur->_right;
						while (rightMin->_left)
						{
							rightMinParent = rightMin;
							rightMin = rightMin->_left;
						}
						// 找到 rightMin，与 cur 交换
						swap(rightMin->_key, cur->_key);
						// 判断 rightMin 是 rightMinParent 的左子树还是右子树
						if (rightMin == rightMinParent->_left)
							rightMinParent->_left = rightMin->_right;
						else
							rightMinParent->_right = rightMin->_right;
						// 删除
						delete rightMin;
					}
					return true;
				}
			}
			// 找不到
			return false;
		}
		void inOrder()
		{
			_inOrder(_root);
			cout << endl;
		}
	private:
		void _inOrder(Node* root)
		{
			if (root == nullptr) return;

			_inOrder(root->_left);
			cout << root->_key << " ";
			_inOrder(root->_right);
		}
		Node* _root = nullptr;
	};
}

namespace key_val
{
	template <class K, class V>
	struct BSTreeNode
	{
		BSTreeNode(const K& key = K(), const V& val = V())
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
			, _val(val)
		{}

		BSTreeNode<K, V>* _left;
		BSTreeNode<K, V>* _right;
		K _key;
		V _val;
	};


	template <class K, class V>
	class BSTree
	{
		typedef BSTreeNode<K, V> Node;
	public:
		bool insert(const K& key, const V& val)
		{
			// 树为空
			if (_root == nullptr)
			{
				_root = new Node(key, val);
				return true;
			}

			// 树不为空
			// 寻找合适插入位置
			Node* cur = _root, * parent = nullptr;
			while (cur)
			{
				if (key > cur->_key)
				{
					// 插入值 > 当前节点
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					// 插入值 < 当前节点
					parent = cur;
					cur = cur->_left;
				}
				else
					return false; // 不允许插入重复值
			}
			// 找到插入位置
			cur = new Node(key, val);
			// 判断 cur 是 parent 的左子树还是右子树，链接
			if (cur->_key > parent->_key)
				parent->_right = cur;
			else
				parent->_left = cur;
			return true;
		}

		Node* find(const K& key)
		{
			// 树为空
			if (_root == nullptr)
				return nullptr;

			// 树不为空
			Node* cur = _root;
			while (cur)
			{
				if (key > cur->_key)
				{
					// key值 > 当前节点
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					// 插入值 < 当前节点
					cur = cur->_left;
				}
				else
					return cur; // 找到
			}
			// 找不到
			return nullptr;
		}

		bool erase(const K& key)
		{
			// 树为空
			if (_root == nullptr)
				return false;

			// 树非空，找到指定值并删除
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					// 找到，删除
					// 1.a cur 的左子树为空，parent 接管 cur 的右子树
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							// 判断 cur 是左子树还是右子树
							if (cur == parent->_left)
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}
						delete cur;
					}
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							// 1.b cur 的右子树为空，parent接管 cur 的左子树
							// 判断 cur 为左还是右
							if (cur == parent->_left)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}
						delete cur;
					}
					else
					{
						// 2. cur左右不为空，替换法
						// 找到cur右子树的最左节点 rightMin，还有它的父节点 rightMinParent
						Node* rightMinParent = cur;
						Node* rightMin = cur->_right;
						while (rightMin->_left)
						{
							rightMinParent = rightMin;
							rightMin = rightMin->_left;
						}
						// 找到 rightMin，与 cur 交换
						swap(rightMin->_key, cur->_key);
						// 判断 rightMin 是 rightMinParent 的左子树还是右子树
						if (rightMin == rightMinParent->_left)
							rightMinParent->_left = rightMin->_right;
						else
							rightMinParent->_right = rightMin->_right;
						// 删除
						delete rightMin;
					}
					return true;
				}
			}
			// 找不到
			return false;
		}
		void inOrder()
		{
			_inOrder(_root);
			cout << endl;
		}
	private:
		void _inOrder(Node* root)
		{
			if (root == nullptr) return;

			_inOrder(root->_left);
			cout << root->_key << " ";
			_inOrder(root->_right);
		}
		Node* _root = nullptr;
	};
}
