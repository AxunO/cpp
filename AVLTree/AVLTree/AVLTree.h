#pragma once

#include <iostream>
#include <assert.h>
#include <vector>
using namespace std;

template<class K, class V>
struct AVLTreeNode
{
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	int _bf;

	AVLTreeNode(const pair<K, V> kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _bf(0)
	{}
};


template<class K, class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	bool Insert(const pair<K, V> kv)
	{
		// 为空
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* cur = _root;
		Node* parent = _root;
		while (cur)
		{
			if (kv.first > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
				return false;
		}

		// new新节点，改变链接关系
		cur = new Node(kv);
		cur->_parent = parent;
		if (kv.first > parent->_kv.first)
		{
			parent->_right = cur;
		}
		else
		{
			parent->_left = cur;
		}

		// 更新平衡因子
		while (parent)
		{
			if (cur == parent->_right)
			{
				parent->_bf++;
			}
			else
			{
				parent->_bf--;
			}
			
			// 检查平衡因子
			if (parent->_bf == 0)
			{
				// 更新结束
				break;
			}
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				// 0 -> 1 -1
				// 表示高度发生变化，继续向上更新
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				// -1 1 -> -2 2
				// 不符合 AVLTree 条件，需要旋转

				// 右右，左旋
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateL(parent);
				}
				// 左左，右旋
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					RotateR(parent);
				}
				// 右左，先右旋，再左旋
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);
				}
				// 左右，先左旋，再右旋
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);
				}

				break;
			}
			else
			{
				// 理论上不会有这种情况
				assert(false);
			}
		}

		return true;
	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = parent->_right->_left;

		parent->_right = subRL;
		// subRL可能不存在
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* pparent = parent->_parent;
		parent->_parent = subR;

		if (parent == _root)
		{
			_root = subR;
			subR->_parent = nullptr;
		}
		else
		{
			if (parent == pparent->_right)
			{
				pparent->_right = subR;
			}
			else
			{
				pparent->_left = subR;
			}

			subR->_parent = pparent;
		}
		parent->_bf = subR->_bf = 0;
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;
		Node* pparent = parent->_parent;
		parent->_parent = subL;

		if (parent == _root)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (parent == pparent->_right)
				pparent->_right = subL;
			else
				pparent->_left = subL;

			subL->_parent = pparent;
		}
		parent->_bf = subL->_bf = 0; 
	}

	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;
		
		RotateR(subR);
		RotateL(parent);

		subRL->_bf = 0;
		if (bf == -1)
		{
			subR->_bf = 1;
			parent->_bf = 0;
		}
		else if (bf == 1)
		{
			subR->_bf = 0;
			parent->_bf = -1;
		}
		else
		{
			subR->_bf = 0;
			parent->_bf = 0;
		}
	}

	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;

		RotateL(subL);
		RotateR(parent);

		subLR->_bf = 0;
		if (bf == -1)
		{
			subL->_bf = 0;
			parent->_bf = 1;
		}
		else if (bf == 1)
		{
			subL->_bf = -1;
			parent->_bf = 0;
		}
		else
		{
			subL->_bf = 0;
			parent->_bf = 0;
		}
	}

	Node* Find(const K& k)
	{
		Node* cur = _root;
		while (cur)
		{
			if (k > cur->_kv.first)
			{
				cur = cur->_right;
			}
			else if (k < cur->_kv.first)
			{
				cur = cur->_left;
			}
			else
				return cur;
		}
		return nullptr;
	}

	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	bool isBalanced()
	{
		return _isBalanced(_root);
	}

	int Helight()
	{
		return _Helight(_root);
	}

	int Size()
	{
		return _Size(_root);
	}
private:
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;

		_InOrder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_InOrder(root->_right);
	}

	bool _isBalanced(Node* root)
	{
		if (root == nullptr)
			return true;

		// 计算左右树高度差
		int helightL = _Helight(root->_left);
		int helightR = _Helight(root->_right);
		if (abs(helightL - helightR) >= 2)
			return false;

		// 检查平衡因子
		if (helightR - helightL != root->_bf)
			return false;

		return _isBalanced(root->_left) && _isBalanced(root->_right);
	}

	int _Helight(Node* root)
	{
		if (root == nullptr)
			return 0;
		return max(_Helight(root->_left), _Helight(root->_right)) + 1;
	}

	int _Size(Node* root)
	{
		if (root == nullptr)
			return 0;

		return _Size(root->_left) + _Size(root->_right) + 1;
	}
private:
	Node* _root = nullptr;
};

void test1()
{
	// int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	AVLTree<int, int> t1;

	for (auto e : a)
	{
		if (e == 10)
		{
			int x = 1;
		}
		t1.Insert({ e,e });
		cout << "Insert:" << e << "->" << t1.isBalanced() << endl;
	}

	t1.InOrder();
	cout << t1.isBalanced() << endl;
}

void test2()
{
	const int N = 1000000;
	vector<int> v;
	v.reserve(N);
	srand(time(0));

	for (int i = 0; i < N; i++)
	{
		v.push_back(rand() + i);
	}

	AVLTree<int, int> t;
	// 插入数据
	int begin1 = clock();
	for (auto e : v)
	{
		t.Insert(make_pair(e, e));
	}
	int end1 = clock();

	// 确定数据
	int begin2 = clock();
	for (auto e : v)
	{
		t.Find(e);
	}
	int end2 = clock();

	cout << "Helight:" << t.Helight() << endl;
	cout << "Size:" << t.Size() << endl;
	cout << "Insert:" << end1 - begin1 << endl;
	cout << "Find:" << end2 - begin2 << endl;
}