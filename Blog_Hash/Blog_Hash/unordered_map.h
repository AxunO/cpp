#pragma once
#include "HashTable.h"
namespace myUM
{
	// 转换 key 为整型
	template <class K>
	struct HashFunc
	{
		size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};
	// 特化
	template<>
	struct HashFunc<string>
	{
		size_t operator()(const string& s)
		{
			size_t ret = 0;
			for (auto& e : s)
			{
				ret = ret * 131 + e;
			}
			return ret;
		}
	};

	template <class K, class V, class Hash = HashFunc<K>>
	class unordered_map
	{
		struct MapKeyOfT
		{
			const K& operator()(const pair<K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename hash_bucket::HashTable<K, pair<const K, V>, MapKeyOfT, Hash>::iterator iterator;
		typedef typename hash_bucket::HashTable<K, pair<const K, V>, MapKeyOfT, Hash>::const_iterator const_iterator;

		iterator begin()
		{
			return _ht.begin();
		}
		const_iterator begin() const
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}
		const_iterator end() const
		{
			return _ht.end();
		}

		pair<iterator, bool> insert(const pair<K, V>& kv)
		{
			return _ht.insert(kv);
		}

		iterator find(const K& key)
		{
			return _ht.find(key);
		}

		bool erase(const K& key)
		{
			return _ht.erase(key);
		}

		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = insert(make_pair(key, V()));
			return ret.first->second;
		}
	private:
		hash_bucket::HashTable<K, pair<const K, V>, MapKeyOfT, Hash> _ht;
	};


	void test_unordered_map1()
	{
		// insert
		unordered_map<string, int> ht;
		ht.insert(make_pair("hash", 1));
		ht.insert(make_pair("map", 2));
		ht.insert(make_pair("table", 3));
		// find
		auto ret = ht.find("hash");
		cout << ret->first << ":" << ret->second << endl;
		// erase
		ht.erase("hash");
		cout << "erase:hash" << endl;

		ret = ht.find("hash");
		if (ret == ht.end())
			cout << "hash 已删除" << endl;
	}
	void test_unordered_map2()
	{
		// operator[]
		string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜",
	"苹果", "香蕉", "苹果", "香蕉","苹果","草莓", "苹果","草莓" };
		unordered_map<string, int> countMap;
		for (auto& e : arr)
		{
			countMap[e]++;
		}
		// 迭代器
		unordered_map<string, int>::iterator it = countMap.begin();
		while (it != countMap.end())
		{
			//it->first += 'x'; // key不能修改
			it->second += 1;  // value可以修改
			cout << it->first << ":" << it->second << endl;
			++it;
		}
		cout << endl;
		// 范围 for
		for (auto& kv : countMap)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
		cout << endl;

		/*cout << "-------------" << endl;
		string in;
		while (cin >> in)
		{
			unordered_map<string, int>::iterator ret = countMap.find(in);
			if (ret != countMap.end())
			{
				cout << ret->first << ":" << ret->second << endl;
			}
			else
			{
				cout << in << " 不存在，请重新输入" << endl;
			}
		}*/
	}
}