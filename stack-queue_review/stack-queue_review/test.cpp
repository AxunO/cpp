#include <iostream>
using namespace std;

#include "stack.h"
#include "queue.h"
#include "list.h"
void testStack()
{
	ns1::stack<int> st1;
	st1.push(1);
	st1.push(2);
	st1.push(3);
	st1.push(4);

	ns1::stack<int> st2;
	st2.push(5);
	st2.push(6);
	st2.push(7);
	st2.push(8);

	st1.swap(st2);

	while (!st1.empty())
	{
		cout << st1.top() << " ";
		st1.pop();
	}
	cout << endl;
	while (!st2.empty())
	{
		cout << st2.top() << " ";
		st2.pop();
	}
	cout << endl;
}

void testQueue()
{
	ns1::queue<int> q1;
	ns1::queue<int> q2;
	q1.push(4);
	q1.push(3);
	q1.push(2);
	q1.push(1);

	//q2.swap(q1);
	while (!q1.empty())
	{
		cout << q1.front() << " ";
		q1.pop();
	}
	cout << endl;
	while (!q2.empty())
	{
		cout << q2.front() << " ";
		q2.pop();
	}
	cout << endl;
}

void reverseIterator()
{
	ns1::list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);

	ns1::list<int>::reverse_iterator rit = lt1.rbegin();
	while (rit != lt1.rend())
	{
		cout << *rit << endl;
		++rit;
	}
}


//template <class T>
//class Greater
//{
//public:
//	bool operator()(const T& a, const T& b)
//	{
//		return a > b;
//	}
//};

void testPQ()
{
	ns1::priority_queue<int, vector<int>, ns1::Greater<int>> pq; 
	pq.push(5);
	pq.push(1);
	pq.push(10);
	pq.push(4);
	pq.push(9);
	pq.push(7);
	pq.push(6);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;

}
int main()
{
	testPQ();
	return 0;
}