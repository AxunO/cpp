#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <stdlib.h>
using namespace std;

//struct Stack
//{
//	int* _data;
//	int _capacity;
//	int _size;
//};
//
//void StackInit(struct Stack* ps, int capacity)
//{
//	ps->_data = (int*)malloc(sizeof(int) * capacity);
//	ps->_capacity = capacity;
//	ps->_size = 0;
//}


struct Stack
{
	void Init(int capacity)
	{
		_data = (int*)malloc(sizeof(int) * capacity);
		_capacity = capacity;
		_size = 0;
	}

	int* _data;
	int _capacity;
	int _size;
};

class Date
{
public:
	void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void Print()
	{
		cout << _year << '/' << _month << '/' << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

class A
{
public:
	void Print()
	{
		cout << _a << endl;
	}
private:
	int _a;
};

int main()
{
	A* a = nullptr;
	a->Print();
	return 0;
}
