#pragma once

#include <iostream>
using namespace std;

// 日期类
class Date
{
public:

	// 获取某年某月的天数
	int GetMonthDays(int year, int month)
	{
		int monthdays[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
		{
			return 29;
		}
		return monthdays[month];
	}

	// 打印
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}

	// 全缺省构造函数
	Date(int year = 2024, int month = 1, int day = 30);

	// 析构函数
	~Date()
	{}

	// 拷贝构造函数-浅拷贝
	Date(const Date& d);

	// 赋值运算符重载
	Date& operator=(const Date& d);


	// 日期 += 天数
	Date& operator+=(int day);

	// 日期 + 天数
	Date operator+(int day);

	// 日期 -= 天数
	Date& operator-=(int day);

	// 日期 - 天数
	Date operator-(int day);

	// 前置++  先++ 再返回
	Date& operator++();

	// 后置++ 先返回，再++
	Date operator++(int);

	// 前置--
	Date& operator--();

	// 后置--
	Date operator--(int);

	bool operator==(const Date& d);
	bool operator!=(const Date& d);
	bool operator<(const Date& d);
	bool operator<=(const Date& d);
	bool operator>(const Date& d);
	bool operator>=(const Date& d);

	// 日期 - 日期
	int operator-(const Date& d);
private:
	int _year;
	int _month;
	int _day;
};
