#pragma once

#include <list>
#include <deque>
namespace ns1
{
	template <class T, class Container = deque<T>>
	class queue
	{
	public:
		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}

		const T& front() const
		{
			return _con.front();
		}

		const T& back() const
		{
			return _con.back();
		}

		void push(const T& val)
		{
			_con.push_back(val);
		}

		void pop()
		{
			_con.pop_front();
		}

		void swap(queue<T, Container>& q)
		{
			_con.swap(q._con);
		}
	private:
		Container _con;
	};

	// 仿函数模板
	template <class T>
	class Less
	{
	public:
		bool operator()(const T& a, const T& b)
		{
			return a < b;
		}
	};

	template <class T>
	class Greater
	{
	public:
		bool operator()(const T& a, const T& b)
		{
			return a > b;
		}
	};

	// 优先级队列
	// 仿函数控制大小堆
	template <class T, class Container = vector<int>, class Compare = Less<T>>
	class priority_queue
	{
	public:

		void ajust_down(int parent)
		{
			int child = parent * 2 + 1;
			Compare com;
			while (child < size())
			{
				// 判断右孩子
				//if (child + 1 < size() && _con[child] < _con[child + 1])
				if (child + 1 < size() && com(_con[child], _con[child + 1]))
					++child;

				// 判断大小，调整父子关系
				// 大堆，父节点小->当孩子
				//if (_con[parent] < _con[child])
				if (com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
					break;
			}
		}

		void ajust_up(int child)
		{
			Compare com;
			int parent = (child - 1) / 2;
			while (child > 0)
			{
				// 大堆，父节点小->当孩子
				//if (_con[parent] < _con[child])
				if (com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
					break;
				child = parent;
			}
		}

		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}

		const T& top()
		{
			return _con[0];
		}

		void push(const T& val)
		{
			_con.push_back(val);
			ajust_up(size() - 1);
		}

		void pop()
		{
			swap(_con[0], _con[size() - 1]);
			_con.pop_back();
			ajust_down(0);
		}
	private:
		Container _con;
	};
}