#include <iostream>
using namespace std;
#include "Array.h"
//void my_swap(int& left, int& right)
//{
//	int tmp = left;
//	left = right;
//	right = tmp;
//}
//
//void my_swap(double& left, double& right)
//{
//	double tmp = left;
//	left = right;
//	right = tmp;
//}
//
//void my_swap(char& left, char& right)
//{
//	char temp = left;
//	left = right;
//	right = temp;
//}

template <class T>
void my_swap(T& left, T& right)
{
	T tmp = left;
	left = right;
	right = tmp;
}

// 非模板函数
//int Add(const int& x, const int& y)
//{
//	cout << "Add(const int& x, const int& y)" << endl;
//	return x + y;
//}
//// 函数模板
//template <class T1, class T2>
//T1 Add(const T1& x, const T2& y)
//{
//	cout << "Add(const T1& x, const T2& y)" << endl;
//	return x + y;
//}

void test1()
{
	int a = 1, b = 2;
	my_swap(a, b);
	cout << "a:" << a << endl;
	cout << "b:" << b << endl;

	double c = 1.1, d = 2.2;
	my_swap(c, d);
	cout << "c:" << c << endl;
	cout << "d:" << d << endl;

	char e = 'e', f = 'd';
	my_swap(e, f);
	cout << "e:" << e << endl;
	cout << "f:" << f << endl;
}

//void test2()
//{
//	int a = 1;
//	double b = 1.1;
//	//Add(a, (int)b);
//	Add<int>(a, b);
//}
//
//void test3()
//{
//	int a = 1, b = 1;
//	Add(a, b);
//
//	double d = 1.1;
//	Add(a, d);
//}

template <class T>
class A
{
public:
	A(const T& val = T())
		:_a(val)
	{}
private:
	T _a;
};

void test4()
{
	A<int> a(1);
	A<double> b(1.1);
}

//void test5()
//{
//	Array<int, 10> a1;
//	Array<double, 5> a2;
//	cout << a1[1] << endl;
//}

template <class T>
bool Less(T left, T right)
{
	return left < right;
}

bool Less(int* left, int* right)
{
	return *left < *right;
}

//template <>
//bool Less<int*>(int* left, int* right)
//{
//	return *left < *right;
//}

void test6()
{
	/*cout << Less(1, 2) << endl;
	cout << Less(2.2, 1.1) << endl;*/

	int* p1 = new int(5);
	int* p2 = new int(3);
	cout << Less(p1, p2) << endl;
}

// 基础模板
template <class T1, class T2>
class Data
{
public:
	Data() 
	{
		cout << "Data<T1, T2>" << endl;
	}
private:
	T1 _d1;
	T2 _d2;
};
// 全特化
template <>
class Data<int, char>
{
public:
	Data()
	{
		cout << "Data<int, char>" << endl;
	}
private:
	int _d1;
	char _d2;
};
// 偏特化，将第二个参数特化
template <class T1>
class Data<T1, char>
{
public:
	Data()
	{
		cout << "Data<T1, char>" << endl;
	}
private:
	T1 _d1;
	char _d2;
};
// 偏特化，将两个参数特化为指针类型
template <class T1, class T2>
class Data<T1*, T2*>
{
public:
	Data()
	{
		cout << "Data<T1*, T2*>" << endl;
	}
private:
	T1* _d1;
	T2* _d2;
};

void test7()
{
	Data<int, int> d1;
	Data<int, char> d2;
	Data<double, char> d3;
	Data<int*, int*> d4;
	Data<double*, double*> d5;

}

void test8()
{
	Array<int, 10> a1;
	cout << a1.size() << endl;
}
int main()
{
	test8();
	return 0;
}