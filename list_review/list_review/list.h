#pragma once
#include <iostream>
#include <assert.h>
using namespace std;
 
namespace ns1
{
	// 节点类
	template <class T>
	struct ListNode
	{
		ListNode(const T& val = T())
			:_next(nullptr)
			,_prev(nullptr)
			,_data(val)
		{}

		ListNode* _next;
		ListNode* _prev;
		T _data;
	};

	// 迭代器类
	//template <class T>
	//struct ListIterator
	//{
	//	typedef ListNode<T> Node;
	//	typedef ListIterator<T> iterator;

	//	ListIterator(Node* node)
	//		:_node(node)
	//	{}

	//	// *it
	//	T& operator*()
	//	{
	//		return _node->_data;
	//	}

	//	// it->
	//	T* operator->()
	//	{
	//		return &(_node->_data);
	//	}

	//	// ++it
	//	iterator& operator++()
	//	{
	//		_node = _node->_next;
	//		return *this;
	//	}
	//	// it++
	//	iterator operator++(int)
	//	{
	//		iterator tmp(_node);
	//		_node = _node->_next;
	//		return tmp;
	//	}
	//	// --it
	//	iterator& operator--()
	//	{
	//		_node = _node->_prev;
	//		return *this;
	//	}
	//	// it--
	//	iterator operator--(int)
	//	{
	//		iterator tmp(_node);
	//		_node = _node->_prev;
	//		return tmp;
	//	}

	//	bool operator==(const iterator& it)
	//	{
	//		return _node == it._node;
	//	}

	//	bool operator!=(const iterator& it)
	//	{
	//		return _node != it._node;
	//	}

	//	Node* _node;
	//};
	//
	//template <class T>
	//struct ListConstIterator
	//{
	//	typedef ListNode<T> Node;
	//	typedef ListConstIterator<T> iterator;

	//	ListConstIterator(Node* node)
	//		:_node(node)
	//	{}

	//	// *it
	//	const T& operator*() 
	//	{
	//		return _node->_data;
	//	}

	//	// it->
	//	const T* operator->()
	//	{
	//		return &(_node->_data);
	//	}

	//	// ++it
	//	iterator& operator++()
	//	{
	//		_node = _node->_next;
	//		return *this;
	//	}
	//	// it++
	//	iterator operator++(int)
	//	{
	//		iterator tmp(_node);
	//		_node = _node->_next;
	//		return tmp;
	//	}
	//	// --it
	//	iterator& operator--()
	//	{
	//		_node = _node->_prev;
	//		return *this;
	//	}
	//	// it--
	//	iterator operator--(int)
	//	{
	//		iterator tmp(_node);
	//		_node = _node->_prev;
	//		return tmp;
	//	}

	//	bool operator==(const iterator& it)
	//	{
	//		return _node == it._node;
	//	}

	//	bool operator!=(const iterator& it)
	//	{
	//		return _node != it._node;
	//	}

	//	Node* _node;
	//};

	template <class T, class Ref, class Ptr>
	struct ListIterator
	{
		typedef ListNode<T> Node;
		typedef ListIterator<T, Ref, Ptr> iterator;

		ListIterator(Node* node)
			:_node(node)
		{}

		// *it
		Ref operator*()
		{
			return _node->_data;
		}

		// it->
		Ptr operator->()
		{
			return &(_node->_data);
		}

		// ++it
		iterator& operator++()
		{
			_node = _node->_next;
			return *this;
		}
		// it++
		iterator operator++(int)
		{
			iterator tmp(_node);
			_node = _node->_next;
			return tmp;
		}
		// --it
		iterator& operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		// it--
		iterator operator--(int)
		{
			iterator tmp(_node);
			_node = _node->_prev;
			return tmp;
		}

		bool operator==(const iterator& it)
		{
			return _node == it._node;
		}

		bool operator!=(const iterator& it)
		{
			return _node != it._node;
		}

		Node* _node;
	};

	// list 类
	template <class T>
	class list
	{
		typedef ListNode<T> Node;
	public:
		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const T&, const T*> const_iterator;

		iterator begin()
		{
			return iterator(_head->_next);
		}

		iterator end()
		{
			return iterator(_head);
		}
		
		const_iterator begin() const
		{
			return const_iterator(_head->_next);
		}

		const_iterator end() const
		{
			return const_iterator(_head);
		}

		void empty_init()
		{
			_head = new Node;
			_head->_prev = _head;
			_head->_next = _head;
			_size = 0;
		}
		
		list()
		{
			empty_init();
		}

		list(const list<T>& lt)
		{
			empty_init();
			for (auto& e : lt)
				push_back(e);
		}

		list<T>& operator=(list<T> lt)
		{
			swap(lt);
			return *this;
		}

		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		size_t size() const
		{
			return _size;
		}

		bool empty() const
		{
			return _size == 0;
		}

		void push_back(const T& val)
		{
			//Node* tail = _head->_prev;
			//Node* newnode = new Node(val);

			//// tail newnode
			//tail->_next = newnode;
			//newnode->_prev = tail;
			//newnode->_next = _head;
			//_head->_prev = newnode;

			//++_size;

			insert(end(), val);
		}
		
		void pop_back()
		{
			//Node* tail = _head->_prev;
			//Node* prev = tail->_prev;

			//// prev tail head
			//prev->_next = _head;
			//_head->_prev = prev;
			//delete tail;

			//--_size;

			erase(--end());
		}

		void push_front(const T& val)
		{
			insert(begin(), val);
		}

		void pop_front()
		{
			erase(begin());
		}

		iterator insert(iterator pos, const T& val)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* newnode = new Node(val);

			// prev newnode cur
			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;

			++_size;
			return iterator(newnode);
		}

		iterator erase(iterator pos)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* next = cur->_next;

			// prev cur next
			prev->_next = next;
			next->_prev = prev;
			delete cur;

			--_size;
			return iterator(next);
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}

		void swap(list<T>& lt)
		{
			std::swap(_head, lt._head);
			std::swap(_size, lt._size);
		}

	private:
		Node* _head;
		size_t _size;
	};

	template <class T>
	void PrintList(const list<T>& lt)
	{
		typename list<T>::const_iterator it = lt.begin();
		while (it != lt.end())
		{
			//*it += 10;
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}

	void test1()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);
		lt1.push_back(5);
		lt1.push_back(6);

		PrintList(lt1);

		lt1.pop_back();
		lt1.pop_back();
		lt1.pop_back();
		lt1.pop_back();

		PrintList(lt1);
	}

	void test2()
	{
		list<int> lt1;
		lt1.push_front(1);
		lt1.push_front(2);
		lt1.push_front(3);
		lt1.push_front(4);
		lt1.push_front(5);

		PrintList(lt1);

		lt1.pop_front();
		lt1.pop_front();
		lt1.pop_front();

		PrintList(lt1);
	}

	void test3()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);
		lt1.insert(++lt1.begin(), 10);
		PrintList(lt1);

		lt1.erase(++lt1.begin());
		PrintList(lt1);

	}

	struct A
	{
		A(int a1 = 0, int a2 = 0)
			:_a1(a1)
			, _a2(a2)
		{}
		int _a1;
		int _a2;
	};

	void test4()
	{
		list<A> lt1;
		A aa(1, 1);
		lt1.push_back(aa);
		lt1.push_back(A({ 2,2 }));
		lt1.push_back({ 3,3 });
		lt1.push_back({ 4,4 });

		list<A>::iterator it = lt1.begin();
		while (it != lt1.end())
		{
			cout << (*it)._a1 << ":" << (*it)._a2 << endl;
			cout << it.operator->()->_a1 << ":" << it.operator->()->_a2 << endl;
			cout << it->_a1 << ":" << it->_a2 << endl;
			++it;
		}
	}

	void test5()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);

		PrintList(lt1);
	}

	void test6()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);

		list<int> lt2;
		lt2.swap(lt1);

		/*for (auto& e : lt1)
			cout << e << " ";
		cout << endl;*/
		list<int>::iterator it = lt1.begin();
		while (it != lt1.end())
		{
			cout << *it << " ";
		}
		cout << endl;
		
		for (auto& e : lt2)
			cout << e << " ";
		cout << endl;
	}
}
