#pragma once

#include <iostream>
using namespace std;

enum Color
{
	RED,
	BLACK
};

template <class K, class V>
struct RBTreeNode
{
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	Color _col;

	RBTreeNode(const pair<K,V>& kv)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_kv(kv)
		,_col(RED)
	{}
};

template<class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	bool Insert(const pair<K,V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = BLACK;
			return true;
		}

		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (kv.first > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		cur = new Node(kv);
		cur->_col = RED;
		cur->_parent = parent;
		if (kv.first > parent->_kv.first)
		{
			cur = parent->_right;
		}
		else
		{
			cur = parent->_left;
		}

		// 检查颜色
		// 父亲存在，且父亲为红色，那么就需要调整
		while (parent && parent->_col == RED)
		{
			Node* grandpa = parent->_parent;
			if (parent == grandpa->_left)
			{
				// 父亲是爷爷的左，叔叔是爷爷的右
				Node* uncle = grandpa->_right;

				// 唯一的变量就是叔叔
				// 情况一：叔叔存在，为红色
				if (uncle && uncle->_col == RED)
				{
					// 父亲、叔叔变黑，爷爷变红
					parent->_col = BLACK;
					uncle->_col = BLACK;
					grandpa->_col = RED;

					// 更新 cur parent
					cur = grandpa;
					parent = cur->_parent;
				}
				else
				{
					// 叔叔不存在 || 叔叔存在且为黑
					// cur为p的左
					if (cur == parent->_left)
					{
						//		g
						//	p		u
						//c
						// 右旋，p变黑，g变红
						RotateR(grandpa);
						parent->_col = BLACK;
						grandpa->_col = RED;
					}
					else
					{
						// cur为p的右
						//		g
						//	p		u
						//	  c
						// 先左旋，再右旋，变色
						// 注意：旋转后，需要变色的节点 cur 变黑，grandpa 变红

						RotateL(parent);
						RotateR(grandpa);
						cur->_col = BLACK;
						grandpa->_col = RED;
					}
					break;
				}
			}
			else
			{
				// 父亲是爷爷的右，叔叔是爷爷的左
				Node* uncle = grandpa->_left;

				if (uncle && uncle->_col == RED)
				{
					// 情况一：叔叔存在且为红
					// 变色即可
					parent->_col = BLACK;
					uncle->_col = BLACK;
					grandpa->_col = RED;

					// 继续向上处理
					cur = grandpa;
					parent = cur->_parent;
				}
				else
				{
					// 情况二：叔叔不存在/叔叔存在且为黑

					if (cur == parent->_right)
					{
						// cur 为 parent 的右
						//	g
						//u	  p
						//      c
						// g 左旋，p变黑，g变红
						RotateL(grandpa);
						parent->_col = BLACK;
						grandpa->_col = RED;
					}
					else
					{
						// cur 为 parent 的左
						//	g
						//u	  p
						//   c
						// p 右旋，g 左旋，c变黑，g 变红
						RotateR(parent);
						RotateL(grandpa);
						cur->_col = BLACK;
						grandpa->_col = RED;
					}
					break;
				}
			}
		}
		_root->_col = BLACK;
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		Node* pparent = parent->_parent;
		subL->_right = parent;
		parent->_parent = subL;
		if (parent == _root)
		{
			subL->_parent = nullptr;
			_root = subL;
		}
		else
		{
			if (parent == pparent->_right)
				pparent->_right = subL;
			else
				pparent->_left = subL;

			subL->_parent = pparent;
		}
	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* pparent = parent->_parent;
		parent->_parent = subR;

		if (parent == _root)
		{
			subR->_parent = nullptr;
			_root = subR;
		}
		else
		{
			if (parent == pparent->_left)
				pparent->_left = subR;
			else
				pparent->_right = subR;

			subR->_parent = pparent;
		}
	}

	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	bool IsValidRBTree()
	{
		// 空树也是红黑树
		if (_root == nullptr)
			return true;

		// 检查根的颜色
		if (_root->_col == RED)
		{
			cout << "根是红色的" << endl;
			return false;
		}

		// 统计一条路径上的黑色节点数
		int refNum = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
				refNum++;
			cur = cur->_left;
		}

		return _IsValidRBTree(_root, 0, refNum);
	}
private:
	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_InOrder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_InOrder(root->_right);
	}

	bool _IsValidRBTree(Node* root, int blackNum, int refNum)
	{
		// 走到叶子，判断黑色节点数量是否相等
		if (root == nullptr)
		{
			if (blackNum != refNum)
			{
				cout << "存在黑色节点数量不相等的路径" << endl;
				return false;
			}

			return true;
		}

		// 检查连续父子节点是否都是红色
		Node* parent = root->_parent;
		if (parent && root->_col == RED && parent->_col == RED)
		{
			cout << "存在连续的红色节点" << endl;
			return false;
		}

		if (root->_col == BLACK)
			blackNum++;
		return _IsValidRBTree(root->_left, blackNum, refNum)
			&& _IsValidRBTree(root->_right, blackNum, refNum);
	}
	Node* _root = nullptr;
};


void TestRBTree1()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	//int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14,8, 3, 1, 10, 6, 4, 7, 14, 13 };
	RBTree<int, int> t1;
	for (auto e : a)
	{
		if (e == 10)
		{
			int i = 0;
		}

		// 1、先看是插入谁导致出现的问题
		// 2、打条件断点，画出插入前的树
		// 3、单步跟踪，对比图一一分析细节原因
		t1.Insert({ e,e });

		cout << "Insert:" << e << "->" << t1.IsValidRBTree() << endl;
	}

	t1.InOrder();

	cout << t1.IsValidRBTree() << endl;
}
