#include <iostream>
#include <string>
using namespace std;

#include "BSTree.h"

void test1()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	key::BSTree<int> t1;

	for (auto e : a)
		t1.insert(e);

	t1.inOrder();
	cout << "find(10)��" << t1.find(10) << endl;
	cout << "find(11)��" << t1.find(11) << endl;

	t1.erase(3);
	t1.inOrder();
}
void test2()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	key::BSTree<int> t1;

	for (auto e : a)
		t1.insert(e);
	t1.inOrder();

	for (auto e : a)
	{
		t1.erase(e);
		t1.inOrder();
	}
}

void testSpell()
{
	// �����ʿ�������
	key::BSTree<string> t;
	t.insert("string");
	t.insert("tree");
	t.insert("node");
	t.insert("word");

	// ���ƴд
	string s;
	while (cin >> s)
	{
		if (t.find(s))
			cout << "ƴд��ȷ" << endl;
		else
			cout << "ƴд����" << endl;
	}
}

void testKV()
{
	key_val::BSTree<string, string> t;
	t.insert("string", "�ַ���");
	t.insert("node", "�ڵ�");
	t.insert("tree", "��");

	string s;
	while (cin >> s)
	{
		auto ret = t.find(s);
		if (ret != nullptr)
			cout << ret->_key << "��" << ret->_val << endl;
		else
			cout << "��Ǹ�����ʿ����޴˵���" << endl;
	}
}
int main()
{
	testKV();
	return 0;
}