#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <string>
using namespace std;

//class Person
//{
//protected:
//	string _name;
//	string _sex;
//	int _age;
//};
//
//class Student :public Person
//{
//public:
//	void Init(string name, string sex, int age, int sid)
//	{
//		_name = name;
//		_sex = sex;
//		_age = age;
//		_stuid = sid;
//	}
//protected:
//	int _stuid;
//};
//
//class A
//{
//public:
//	void fun()
//	{
//		cout << "fun()->A" << endl;
//	}
//};
//
//class B :public A
//{
//public:
//	void fun(int i)
//	{
//		cout << "fun(int i)->B" << endl;
//	}
//};


//class Person
//{
//public:
//	// 默认构造
//	Person(const char* name = "张三")
//		:_name(name)
//	{
//		cout << "Person()" << endl;
//	}
//	// 拷贝构造
//	Person(const Person& p)
//	{
//		cout << "Person(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//	}
//	// 赋值重载
//	Person& operator=(const Person& p)
//	{
//		cout << "Person& operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//		return *this;
//	}
//	// 析构
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
//
//class Student :public Person
//{
//public:
//	// 构造
//	Student(const char* name, int num)
//		:Person(name)
//		,_num(num)
//	{
//		cout << "Student()" << endl;
//	}
//	// 拷贝构造
//	Student(const Student& s)
//		:Person(s)	// 将父类当作整体，调用父类拷贝构造
//		,_num(s._num)
//	{
//		cout << "Student(const Student& s)" << endl;
//	}
//	// 赋值重载
//	Student& operator=(const Student& s)
//	{
//		cout << "Student& operator=(const Student& s)" << endl;
//		if (this != &s)
//		{
//			// 加作用域限定，防止调用自身
//			Person::operator=(s);
//			_num = s._num;
//		}
//		return *this;
//	}
//	// 析构
//	~Student()
//	{
//		Person::~Person();
//		// 访问父类成员
//		cout << _name << endl;
//		cout << "~Student()" << endl;
//	}
//protected:
//	int _num; // 学号
//};

//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name;
//};
//class Student : public Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	int _stuid;
//};
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuid << endl;
//}

//class Person
//{
//public:
//	Person()
//	{
//		_count++;
//	}
//protected:
//	string _name;
//public:
//	// 静态成员，用来计数
//	static int _count;
//};
//
//int Person::_count = 0;
//
//class Student :public Person
//{
//protected:
//	int _stuid;
//};
//
//class Graduate :public Student
//{
//protected:
//	int _course; // 研究科目
//};
//int main()
//{
//	Person p;
//	Student s;
//	Graduate g;
//	// 查看地址
//	cout << &Person::_count << endl;
//	cout << &Student::_count << endl;
//	cout << &Graduate::_count << endl;
//	// 查看_count的值
//	cout << "Person::_count->" << Person::_count << endl;
//	// 在子类中将_count置零
//	Student::_count = 0;
//	cout << "Person::_count->" << Person::_count << endl;
//	return 0;
//}

//class Person
//{
//public:
//	string _name;
//};
//
//class Student : public Person
//{
//protected:
//	int _stuid;
//};
//
//class Teacher :public Person
//{
//protected:
//	int _id;
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _course;
//};
//
//class A
//{
//public:
//	int _a;
//};
//
////class B : public A
//// 在腰部使用虚拟继承
//class B : virtual public A
//{
//public:
//	int _b;
//};
//
//// 在腰部使用虚拟继承
////class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	// B类
//	d.B::_a = 1;
//	d._b = 2;
//	// C类
//	d.C::_a = 3;
//	d._c = 4;
//
//	d._d = 5;
//
//	return 0;
//}

class A
{
protected:
	int _a;
};

// B中有A，即为组合
class B
{
protected:
	A _A;
};