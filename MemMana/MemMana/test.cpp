#include <iostream>
using namespace std;


//struct ListNode
//{
//	ListNode(int val)
//		:_val(val)
//		,_next(nullptr)
//	{}
//
//	int _val;
//	ListNode* _next;
//};

//struct ListNode* CreateNode(int x)
//{
//	ListNode* newnode = (struct ListNode*)malloc(sizeof(struct ListNode));
//	if (newnode == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//
//	newnode->_val = x;
//	newnode->_next = NULL;
//	return newnode;
//}

class ListNode
{
public:
	ListNode(int val)
		:_val(val)
		, _next(nullptr)
	{}
	~ListNode()
	{
		cout << "~ListNode()" << endl;
	}
private:
	int _val;
	ListNode* _next;
};


class A
{
public:
	A(int val = 0)
		:_a(val)
	{
		cout << "A(int val = 0)" << endl;
	}

	~A()
	{
		cout << "~A()" << endl;
	}
	
private:
	int _a;
};
int main()
{
	//int* p1 = new int(10);
	//cout << *p1 << endl;
	//delete p1;

	//int* p1 = new int[3] {1, 2, 3};
	//delete[] p1;

	//ListNode* ln1 = new ListNode(10);
	//delete ln1;

	//A* pa = new A[10];
	//delete pa;

	A* pa = (A*)malloc(sizeof(A));
	new(pa)A(10);
	return 0;
}