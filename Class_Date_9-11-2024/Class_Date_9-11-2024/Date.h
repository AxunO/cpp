#pragma once

#include <iostream>
using namespace std;

class Date
{
public:
	bool CheckInvalid();
	// 默认构造
	Date(int year = 1, int month = 1, int day = 1);
	//// 析构函数
	//~Date();
	//// 拷贝构造，特殊构造
	//Date(const Date& d);
	//// 赋值重载
	//Date& operator=(const Date& d);

	//运算符重载
	bool operator==(const Date& d);
	bool operator!=(const Date& d);
	bool operator<(const Date& d);
	bool operator<=(const Date& d);
	bool operator>(const Date& d);
	bool operator>=(const Date& d);

	// d+10
	Date& operator+=(int x);
	Date operator+(int x);

	// d-10
	Date& operator-=(int x);
	Date operator-(int x);

	// ++d
	Date& operator++();
	// d++
	Date operator++(int);
	// --d
	Date& operator--();
	// d--
	Date operator--(int);

	// d1 - d2
	int operator-(const Date& d);

	int GetMonthDay(int year, int month)
	{
		int months[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
			return 29;
		return months[month];
	}

	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}


	friend ostream& operator<<(ostream& out, const Date& d);
	friend istream& operator>>(istream& in, Date& d);
private:
	int _year;
	int _month;
	int _day;
};

// <<
ostream& operator<<(ostream& out, const Date& d);
// >>
istream& operator>>(istream& in, Date& d);

