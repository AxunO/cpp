#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
using namespace std;

class Solution1
{
public:
    int numDecodings(string s)
    {
        int n = s.size();
        // 防止越界
        if (n == 1)
        {
            if (s[0] - '0' >= 1 && s[0] - '0' <= 26)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        vector<int> dp(n);
        // 初始化
        // dp[0]
        if (s[0] - '0' >= 1 && s[0] - '0' <= 26)
        {
            dp[0] = 1;
        }
        else
        {
            dp[0] = 0;
        }
        // dp[1]
        int uni = (s[0] - '0') * 10 + (s[1] - '0');
        if (s[1] - '0' >= 1 && s[1] - '0' <= 26)
        {
            dp[1] += dp[0];
        }
        if (uni <= 26 && uni >= 10)
        {
            dp[1] += 1;
        }

        // 填表
        for (int i = 2; i < n; i++)
        {
            // 单独解码
            int enc1 = s[i] - '0';
            if (enc1 >= 1 && enc1 <= 26)
            {
                dp[i] += dp[i - 1];
            }
            else
            {
                dp[i] += 0;
            }

            // dp[i] 与 dp[i-1]联合解码
            int enc2 = (s[i - 1] - '0') * 10 + (s[i] - '0');
            if (enc2 >= 10 && enc2 <= 26)
            {
                dp[i] += dp[i - 2];
            }
            else
            {
                dp[i] += 0;
            }
        }
        return dp[n - 1];
    }
};

class Solution2
{
public:
    int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid)
    {
        size_t m = obstacleGrid.size();
        size_t n = obstacleGrid[0].size();

        // 建表，带有虚拟节点
        vector<vector<int>> dp(m + 1, vector<int>(n + 1));

        // 初始化
        dp[0][1] = 1;

        // 填表
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                if (obstacleGrid[i - 1][j - 1] == 1)
                    dp[i][j] = 0;
            }
        }
        return dp[m][n];
    }
};

// LCR 166. 珠宝的最高价值
class Solution3 {
public:
    int jewelleryValue(vector<vector<int>>& frame)
    {
        int m = frame.size();
        int n = frame[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1));

        // 填表
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]) + frame[i - 1][j - 1];
            }
        }
        return dp[m][n];
    }
};

// 931. 下降路径最小和
class Solution4 {
public:
    int minFallingPathSum(vector<vector<int>>& matrix)
    {
        int m = matrix.size();
        int n = matrix[0].size();

        vector<vector<int>> dp(m + 1, vector<int>(n + 2, INT_MAX));
        // 初始化
        for (int i = 0; i < n + 2; i++)
            dp[0][i] = 0;
        // 填表
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j < n + 1; j++)
            {
                dp[i][j] = min(min(dp[i - 1][j - 1], dp[i - 1][j]),
                    min(dp[i - 1][j], dp[i - 1][j + 1]))
                    + matrix[i - 1][j - 1];
            }
        }

        // 返回最后一行的最小值
        int minum = dp[m][1];
        for (int i = 2; i <= n + 1; i++)
        {
            if (dp[m][i] < minum)
                minum = dp[m][i];
        }
        return minum;
    }
};

// 64. 最小路径和
class Solution5 {
public:
    int minPathSum(vector<vector<int>>& grid)
    {
        // dp[i][j]表示到达ij位置时，最小数字和
        // dp[i][j] = min(dp[i-1][j], dp[i][j-1]) + g[i][j]

        int m = grid.size();
        int n = grid[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));

        // 初始化
        dp[0][1] = 0;
        // 填表
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i - 1][j - 1];
            }
        }
        return dp[m][n];
    }
};

// 174. 地下城游戏
class Solution6 {
public:
    int calculateMinimumHP(vector<vector<int>>& dungeon)
    {
        // dp[i][j]表示以ij为开始位置，到达终点所需最低健康点数
        // dp[i][j] + d[i][j] >= min(dp[i][j+1], dp[i+1][j])
        // dp[i][j] >= min(dp[i][j+1], dp[i+1][j]) - d[i][j]
        // 防止dp[i][j]为零或负数，dp[i][j] = max(1, dp[i][j])

        int m = dungeon.size();
        int n = dungeon[0].size();

        vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
        // 初始化
        dp[m - 1][n] = 1;
        dp[m][n - 1] = 1;
        // 填表
        for (int i = m - 1; i >= 0; i--)
        {
            for (int j = n - 1; j >= 0; j--)
            {
                dp[i][j] = min(dp[i + 1][j], dp[i][j + 1]) - dungeon[i][j];
                // 防止为零或负
                dp[i][j] = max(1, dp[i][j]);
            }
        }
        return dp[0][0];
    }
};

// 面试题 17.16. 按摩师
class Solution7 {
public:
    int massage(vector<int>& nums)
    {
        // dp[i]表示？在i位置时，预约的最长时间
        // dp[i]接了，dp[i-1]不能接
        // dp[i]不接，dp[i-1]可接可不接
        // dp[i] 细化
        // f[i], i位置接了，预约的最长时间
        // g[i], i位置没接，预约的最长时间
        // f[i] = g[i-1] + nums[i]
        // g[i] = max(f[i-1], g[i-1])

        int n = nums.size();
        if (n == 0)
            return 0;
        vector<int> f(n);
        vector<int> g(n);

        // 初始化
        f[0] = nums[0];
        g[0] = 0;

        // 填表
        for (int i = 1; i < n; i++)
        {
            f[i] = g[i - 1] + nums[i];
            g[i] = max(f[i - 1], g[i - 1]);
        }

        return max(f[n - 1], g[n - 1]);
    }
};
void test1()
{
    vector<vector<int>> f(3, vector<int>(3));
    // frame = [[1,3,1],[1,5,1],[4,2,1]]
    f[0][0] = 1;
    f[0][1] = 3;
    f[0][2] = 1;
    f[1][0] = 1;
    f[1][1] = 5;
    f[1][2] = 1;
    f[2][0] = 4;
    f[2][1] = 2;
    f[2][2] = 1;

    Solution3 s;
    cout << s.jewelleryValue(f) << endl;
}

void test2()
{
    vector<vector<int>> dp(3, vector<int>(4));
    size_t sz1 = dp.size();
    size_t sz2 = dp[0].size();
    cout << sz1 << endl;
    cout << sz2 << endl;
}
int main()
{
    vector<int> v(3);
    v[0] = 10;
}