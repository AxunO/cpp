#include "vector.h"


void test1()
{
	// 无参
	vector<int> v1;
	// n val
	vector<int> v2(10, 1);
	// 拷贝构造
	vector<int> v3(v2);
	// 迭代器区间
	string s("abcd");
	vector<int> v4(s.begin(), s.end());

	for (auto& e : v1)
		cout << e << " ";
	cout << endl;
	
	for (auto& e : v2)
		cout << e << " ";
	cout << endl;
	
	for (auto& e : v3)
		cout << e << " ";
	cout << endl;
	
	for (auto& e : v4)
		cout << e << " ";
	cout << endl;
}
int main()
{
	ns1::test10();
	return 0;
}