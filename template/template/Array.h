#pragma once
// Array.h
// 声明
template <class T, size_t N = 10>
class Array
{
public:
	size_t size();
private:
	T _array[N];
	size_t _size = N;
};
// 定义
template <class T, size_t N> // 声明与定义的缺省值不可以同时给
size_t Array<T, N>::size() // 声明类域
{
	return _size;
}