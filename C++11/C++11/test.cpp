#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
#include <vector>
#include <string>
#include <assert.h>
#include <list>
#include "list.h"
#include <algorithm>
#include <functional>
#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>

void test1()
{
	/*struct A
	{
		int _x;
		int _y;
	};*/
	//// {} 初始化结构体
	//A a = { 1 };
	//// {} 初始化数组
	//int arr[] = { 1,2,3,4,5 };

	class A
	{
	public:
		A(const int& a) :_a(a) {}
	private:
		int _a;
	};

	class Date
	{
	public:
		Date(const int& year, const int& month, const int& day)
			:_year(year)
			,_month(month)
			,_day(day)
		{}
	private:
		int _year;
		int _month;
		int _day;
	};
	// 单参数隐式类型转换
	A a = { 1 };
	// 多参数隐式类型转换
	Date d = { 2024,1,1 };
}


void test2()
{
	vector<int> v1 = { 0,1,2,3,5,4,6,7,8,9 };
	for (auto& e : v1)
		cout << e << " ";
	cout << endl;

	auto il = { 1,2,3 };
	cout << typeid(il).name() << endl;
}

void test3()
{
	int a = 10;
	auto& ra = a; // 引用
	auto pa = &a; // 指针
	cout << typeid(ra).name() << endl;
	cout << typeid(pa).name() << endl;

}

auto autofunc2()
{
	string::iterator it;
	return it;
}
auto autofunc1()
{
	return autofunc2();
}
void test4()
{
	//auto ret = autofunc1();

	int a = 10;
	decltype(a) b = 10; // 将 b 的类型声明为 a 的类型
	//cout << typeid(a).name() << endl;
	//cout << typeid(b).name() << endl;

	string s1;
	string::iterator it = s1.begin();

	// 创建一个 string::iterator 类型的 vector
	// 正常情况下应该这样写 vector<string::iterator> v
	vector<decltype(it)> v;

	vector<int> v1 = { 0,1,2,3,5,4,6,7,8,9 };
	for (auto& e : v1)
		cout << e << " ";
	cout << endl;

}

void fmin()
{

}
void test5()
{
	//// a 是左值，10 是右值？
	//int a = 10;
	//// a 是左值还是右值？
	//int b = a;

	// a,b,c 都是左值
	// 10, 1+2, a+b 都是右值
	//int a = 10;
	//int b = 1 + 2;
	//int c = a + b;
	// 左值取地址
	//int* pa = &a;
	//int* pb = &b;
	// 右值不可以取地址
	//&10;
	//&(1 + 2);
	//&(a + b);
	//// 右值不可以出现在赋值左边
	//10 = 1;
	//a + b = 1;

	// a,b,c是左值
	//int a = 10;
	//int b = 20;
	//int* c = new int(20);
	//// 左值引用
	//int& ra = a;
	//int& rb = b;
	//int*& rc = c;
	//// 右值
	//10;
	//a + b;
	//fmin(a, b);
	//// 右值引用
	//int&& rr1 = 10;
	//int&& rr2 = a + b;
	//int&& rr3 = fmin(a, b);
	//rr1 = 20;

	// 将右值赋给左值引用
	//int& r1 = 10; // 不可以
	//const int& r1 = 10; // 可以
	//// 将左值赋给右值引用
	//int a = 1;
	//int&& rr1 = move(a);
	//rr1 = 10;

	// 左值引用只可以引用左值，不可以引用右值
	//int a = 10;
	//int& r1 = a;
	//int& r2 = 10; // 不可以，10 是右值

	// const左值引用即可引用左值，也可以引用右值
	//const int& r3 = a;
	//const int& r4 = 10;


	// 右值引用只可引用右值，不可引用左值
	int a = 10;
	int&& r1 = 10;
	// 可以引用 move 之后的左值
	int&& r2 = move(a);


}


namespace ns1
{
	class string
	{
	public:
		typedef char* iterator;
		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			cout << "string(char* str) -- 构造" << endl;

			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		// s1.swap(s2)
		void swap(string& ss)
		{
			::swap(_str, ss._str);
			::swap(_size, ss._size);
			::swap(_capacity, ss._capacity);
		}

		// 拷贝构造
		// 左值
		string(const string& s)
			:_str(nullptr)
		{
			cout << "string(const string& s) -- 深拷贝" << endl;

			_str = new char[s._capacity + 1];
			strcpy(_str, s._str);
			_size = s._size;
			_capacity = s._capacity;
		}
		// 移动构造  -- 移动将亡值对象的资源
		// 右值(将亡值)
		string(string&& s)
			:_str(nullptr)
		{
			cout << "string(string&& s) -- 移动构造" << endl;
			swap(s);
		}

		// 赋值重载
		// s3 = 左值
		string& operator=(const string& s)
		{
			cout << "string& operator=(string s) -- 深拷贝" << endl;
			char* tmp = new char[s._capacity + 1];
			strcpy(tmp, s._str);

			delete[] _str;
			_str = tmp;
			_size = s._size;
			_capacity = s._capacity;

			return *this;
		}

		// 移动赋值
		// s = 将亡值
		string& operator=(string&& s)
		{
			cout << "string(string&& s) -- 移动赋值" << endl;
			swap(s);

			return *this;
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
		}

		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}
		}

		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}

			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}

		//string operator+=(char ch)
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		const char* c_str() const
		{
			return _str;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity; // 不包含最后做标识的\0
	};

	ns1::string to_string(int value)
	{
		bool flag = true;
		if (value < 0)
		{
			flag = false;
			value = 0 - value;
		}

		ns1::string str;
		while (value > 0)
		{
			int x = value % 10;
			value /= 10;

			str += ('0' + x);
		}

		if (flag == false)
		{
			str += '-';
		}

		std::reverse(str.begin(), str.end());

		return str;
	}
}

//// 传值，会触发拷贝构造
//void func1(ns1::string s)
//{}
//// 传引用，不用拷贝
//void func2(const ns1::string& s)
//{}
//int main()
//{
//	//ns1::string s("hello");
//	//func1(s);
//	//func2(s);
//	//// string& operator+=(char ch)，返回引用，不用拷贝
//	//s += '!';
//
//	/*ns1::string ret;
//	ns1::string ret2 = ns1::to_string(1234);
//	ret += '1';*/
//
//	/*ns1::string s;
//	s = "123456";*/
//
//
//	ns1::list<ns1::string> l;
//	cout << "--------------------" << endl;
//	// 插入左值，拷贝构造
//	ns1::string s1("12345");
//	l.push_back(s1);
//	// 插入右值，移动构造
//	l.push_back("12345");
//	return 0;
//}

//void fun(int& t) {	cout << "左值引用" << endl; }
//void fun(const int& t) { cout << "const左值引用" << endl; }
//void fun(int&& t) {	cout << "右值引用" << endl; }
//void fun(const int&& t) {	cout << "const右值引用" << endl; }
//
//template <class T>
//void PerfectForward(T&& t)
//{
//	fun(forward<T>(t));
//}

//int main()
//{
//	PerfectForward(10); // 右值
//
//	int a = 1;
//	PerfectForward(a); // 左值
//	PerfectForward(move(a)); // 右值
//
//	const int b = 1;
//	PerfectForward(b); // const左值
//	PerfectForward(move(b)); // const右值
//	return 0;
//}


//int main()
//{
//	/*auto add = [](int a, int b)->int {return a + b; };
//
//	cout << add(1, 2) << endl;*/
//
//
//	int a = 1, b = 2, c = 3;
//	auto lam = [&]() mutable
//		{
//			++a;
//			++b;
//			++c;
//		};
//
//	lam();
//	return 0;
//}

//class A
//{
//public:
//	A(const char* str, int a)
//		:_str(str)
//		,_a(a)
//	{}
//	// 没写析构，拷贝构造/赋值，就会自动生成移动构造
//	// 写析构，不会生成移动，使用 default 强制生成
//	A(A&& a) = default;
//	A& operator=(A&& a) = default;
//	~A() {}
//private:
//	ns1::string _str;
//	int _a;
//};
//
//// 该类对象只能在堆生成
//// 使用 delete 把构造和拷贝构造ban掉
//class HeapOnly
//{
//public:
//	static HeapOnly* CreateObj(const char* str, int a)
//	{
//		return new HeapOnly(str, a);
//	}
//	HeapOnly(const HeapOnly& hp) = delete;
//	HeapOnly(HeapOnly&& hp) = default;
//private:
//	HeapOnly(const char* str, int a)
//		:_str(str)
//		,_a(a)
//	{}
//	ns1::string _str;
//	int _a;
//};
//int main()
//{
//	/*A a1("1234", 1);
//	A a2(move(a1));
//	a1 = move(a2);*/
//
//	HeapOnly* h1 = HeapOnly::CreateObj("1234", 6);
//	HeapOnly(move(*h1));
//	return 0;
//}


// 可变参数模板

// 递归展开
//void _cpp_print()
//{
//	cout << endl;
//}
//
//template <class T, class ... Args>
//void _cpp_print(const T& val, Args...args)
//{
//	cout << val << " ";
//	_cpp_print(args...);
//}
//
//// 逗号表达式展开
//template <class T>
//void print_args(const T& val)
//{
//	cout << val << " ";
//}
//// 函数模板-可变参数包
//template <class ... Args>
//void cpp_print(Args...args)
//{
//	// 1. 递归展开
//	//_cpp_print(args...);
//
//	// 2. 逗号表达式展开
//	int arr[] = { (print_args(args), 0)... };
//	cout << endl;
//}
//
//
////
////int main()
////{
////	cpp_print(1, 1.1, std::string("hello"));
////	return 0;
////}
//
//// emplace
//int main()
//{
//	// 没区别
//	std::list<pair<ns1::string, int>> lt;
//
//	pair<ns1::string, int> kv1("xxxxx", 1);
//	lt.push_back(kv1);
//	lt.push_back(move(kv1));
//
//	cout << endl;
//
//	// 直接传pair的对象效果跟push_back系列是一样的
//	pair<ns1::string, int> kv2("xxxxx", 1);
//	lt.emplace_back(kv2);
//	lt.emplace_back(move(kv2));
//
//	cout << endl;
//
//	lt.emplace_back("hello", 1);
//	return 0;
//}

// 类的新功能
// 默认移动构造和移动赋值
//class A
//{
//public:
//	A(const char* str = "", int a = 0)
//		:_str(str)
//		, _a(a)
//	{}
//	A(const A& A)
//		:_str(A._str)
//		, _a(A._a)
//	{}
//	A& operator=(A& A) = default; // 强制生成拷贝赋值
//
//	A(A&& A) = default; // 强制生成移动构造
//	A& operator=(A&& A) = default; // 强制生成移动赋值
//private:
//	ns1::string _str; // 自定义类型
//	int _a; // 类型
//};

//class A
//{
//public:
//	A(const char* str = "", int a = 0)
//		:_str(str)
//		, _a(a)
//	{}
//	A(const A& A) = delete; // 禁止生成拷贝构造
//
//private:
//	ns1::string _str; // 自定义类型
//	int _a; // 类型
//};
//
//int main()
//{
//	A a1("hello", 1);
//
//	A a2(a1); // 左值，拷贝构造
//	A a3(move(a1)); // 右值，移动构造
//
//	A a4, a5;
//	a4 = a2; // 左值，拷贝赋值
//	a5 = move(a3); // 右值，移动赋值
//
//	return 0;
//}

//class A
//{
//private:
//	int _a = 10;
//	string _str = "hello";
//};
//
//int main()
//{
//	A a;
//}

// 可变参数模板

// 递归终止函数
void _cpp_print() // 重载，零个参数
{
	cout << endl;
}

// 递归展开函数
template <class T, class ...Args>
void _cpp_print(const T& val, Args... args)
{
	cout << val << " ";
	// 进入下一层递归
	_cpp_print(args...);
}


// 逗号表达式展开
template <class T>
void print_arg(const T& val)
{
	cout << val << " ";
}

template <class ...Args>
void cpp_print(Args... args)
{
	int arr[] = { (print_arg(args), 0)... };
	cout << endl;
}

//int main()
//{
//	cpp_print(1, 1.1, string("hello"));
//	return 0;
//}

//int main()
//{
//	//std::list <pair<ns1::string, int>> lt;
//	//// push
//	//pair<ns1::string, int> p1 = { "1111", 1 };
//	//lt.push_back(p1); // 左值
//	//lt.push_back(move(p1)); // 右值f
//
//	//cout << endl;
//
//	//// emplace
//	//pair<ns1::string, int> p2 = { "2222", 2 };
//	//lt.emplace_back(p2); // 左值
//	//lt.emplace_back(move(p2)); // 右值
//
//	//cout << endl;
//	//lt.emplace_back("3333", 3);
//
//
//	ns1::list <pair<ns1::string, int>> lt;
//	cout << "------------------------------------" << endl;
//	// push
//	lt.push_back({ "1111", 1 }); // 左值
//	lt.push_back(make_pair("2222", 2)); // 右值
//
//	cout << endl;
//
//	// emplace
//	lt.emplace_back("3333", 3); // 左值
//	lt.emplace_back(make_pair("4444", 4)); // 右值
//
//	return 0;
//}


struct Goods
{
	string _name; // 名字
	double _price; // 价格
	int _evaluate; // 评价
	Goods(const char* str, double price, int evaluate)
		:_name(str)
		, _price(price)
		, _evaluate(evaluate)
	{}
};

struct ComparePriceLess
{
	bool operator()(const Goods& g1, const Goods& g2)
	{
		return g1._price < g2._price;
	}
};

//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2, 3 }, { "菠萝", 1.5, 4 } };
//	// sort(v.begin(), v.end(), ComparePriceLess());
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {return g1._price < g2._price; });
//	return 0;
//}

//int main()
//{
//	int a = 1, b = 2, c = 3;
//	auto f1 = [&, b] {}; // 传值捕获 b，传引用捕获除了b的所有变量
//	auto f2 = [=, &c] {}; // 传引用捕获 c，传值捕获除了c的所有变量
//	return 0;
//}
//struct A
//{
//	A(const int& a) :_a(a) {}
//	void PrintA()
//	{
//		auto f1 = [this] {cout << this->_a << endl; };
//		f1();
//	}
//	int _a = 0;
//};
//int main()
//{
//	A a(10);
//	a.PrintA();
//	return 0;
//}


//struct intAdd
//{
//	int operator()(int& x, int& y)
//	{
//		return x + y;
//	}
//};
//int main()
//{
//	int a = 1, b = 2;
//
//	// 函数对象
//	intAdd add1;
//	add1(a, b);
//
//	// lambda
//	auto add2 = [](int& x, int& y) {return x + y; };
//	add2(a, b);
//	return 0;
//}


//// 函数
//int f(int x, int y)
//{
//	return x + y;
//}
//
//// 仿函数
//struct functor
//{
//	int operator()(int x, int y)
//	{
//		return x + y;
//	}
//};
//int main()
//{
//	// 函数指针
//	function<int(int, int)> func1 = f;
//
//	// 函数对象
//	function<int(int, int)> func2 = functor();
//
//	// lambda
//	function<int(int, int)> func3 = [](int x, int y) {return x + y; };
//
//	// 调用
//	cout << func1(1, 2) << endl;
//	cout << func2(3, 4) << endl;
//	cout << func3(5, 6) << endl;
//
//	cout << typeid(func1).name() << endl;
//	cout << typeid(func2).name() << endl;
//	cout << typeid(func3).name() << endl;
//	return 0;
//}

//template<class F, class T>
//T useF(F f, T x)
//{
//	// f代表可调用对象，x代表其参数
//	static int count = 0;
//	cout << "count:" << ++count << endl;
//	cout << "count:" << &count << endl;
//	return f(x);
//}
//
//double f(double i)
//{
//	return i / 2;
//}
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};
//int main()
//{
//	// function 包装
//	function<double(double)> func1 = f;
//	function<double(double)> func2 = Functor();
//	function<double(double)> func3 = [](double d)->double { return d / 4; };
//	
//	// 实例化模板
//	cout << useF(func1, 11.11) << endl;
//	cout << useF(func2, 11.11) << endl;
//	cout << useF(func3, 11.11) << endl;
//	return 0;
//}


//class A
//{
//public:
//	static int addInt(int x, int y)
//	{
//		return x + y;
//	}
//	double addDouble(double x, double y)
//	{
//		return x + y;
//	}
//private:
//	int _a = 0;
//};
//
//int main()
//{
//	// 静态
//	function<int(int, int)> func1 = A::addInt;
//	cout << func1(1, 1) << endl;
//
//	// 普通
//	function<double(A*, double, double)> func2 = &A::addDouble;
//	A a;
//	cout << func2(&a, 1.1, 1.1) << endl;
//
//	function<double(A, double, double)> func3 = &A::addDouble;
//	cout << func3(A(), 4.5, 4.5) << endl;
//	return 0;
//}



//int Sub(int x, int y)
//{
//	return x - y;
//}
//int main()
//{
//	auto b1 = bind(Sub, placeholders::_2, placeholders::_1);
//	cout << b1(10, 1) << endl;
//	return 0;
//}

//void fx(string name, int hp, int mp)
//{
//	cout << name << "->血量：" << hp << " 蓝量：" << mp << endl;
//}
//
//int main()
//{
//	auto fA = bind(fx, "角色A", placeholders::_1, placeholders::_2);
//	fA(100, 80);
//	fA(60, 40);
//	fA(40, 100);
//
//	function<void(int, int)> func1 = bind(fx, "角色B", placeholders::_1, placeholders::_2);
//	func1(100, 100);
//	return 0;
//}

//void ThreadFunc(int a)
//{
//	cout << "Thread1，" << a << endl;
//}
//class TF
//{
//public:
//	void operator()()
//	{
//		cout << "Thread3" << endl;
//	}
//};
//int main()
//{
//	// 线程函数为函数指针
//	thread t1; // 没有启动线程
//	t1 = thread(ThreadFunc, 10); // 匿名对象，移动赋值
//	//// 线程函数为lambda表达式
//	//thread t2([] {cout << "Thread2" << endl; });
//	//// 线程函数为函数对象
//	//TF tf;
//	//thread t3(tf);
//	t1.join();
//	/*t2.join();
//	t3.join();*/
//	cout << "Main thread!" << endl;
//	return 0;
//}


//#include <thread>
//void ThreadFunc1(int& x)
//{
//	x += 10;
//}
//void ThreadFunc2(int* x)
//{
//	*x += 10;
//}
//int main()
//{
//	int a = 10;
//	// 在线程函数中对a修改，不会影响外部实参，因为：线程函数参数虽然是引用方式，但其实际引用的是线程栈中的拷贝
//	/*thread t1(ThreadFunc1, a);
//	t1.join();
//	cout << a << endl;*/
//
//	// 如果想要通过形参改变外部实参时，必须借助std::ref()函数
//	thread t2(ThreadFunc1, std::ref(a));
//	t2.join();
//	cout << a << endl;
//
//	//// 地址的拷贝
//	thread t3(ThreadFunc2, &a);
//	t3.join();
//	cout << a << endl;
//	return 0;
//}

//atomic_long sum = 0;
//void fun(size_t num)
//{
//	for (size_t i = 0; i < num; ++i)
//		sum++; // 原子操作
//}
//int main()
//{
//	cout << "Before joining, sum = " << sum << std::endl;
//
//	thread t1(fun, 10000);
//	thread t2(fun, 10000);
//	t1.join();
//	t2.join();
//
//	cout << "After joining, sum = " << sum << std::endl;
//	return 0;
//}
//


void two_thread_print()
{
	std::mutex mtx;
	condition_variable c;
	int n = 100;
	bool flag = true; // 保证第一个打印的一定是 0

	thread t1([&]() {
	int i = 0;
	while (i < n)
	{
		unique_lock<mutex> lock(mtx);
		c.wait(lock, [&]()->bool {return flag; }); // 保证第一个打印的一定是 0
		cout << i << endl;
		flag = false;
		i += 2; // 偶数
		c.notify_one();
	}
	});

	thread t2([&]() {
		int j = 1;
		while (j < n)
		{
			unique_lock<mutex> lock(mtx);
			c.wait(lock, [&]()->bool {return !flag; }); // 保证第一个打印的一定是 0
			cout << j << endl;
			j += 2; // 奇数
			flag = true;
			c.notify_one();
		}
		});

	t1.join();
	t2.join();
}
int main()
{
	two_thread_print();
	return 0;
}