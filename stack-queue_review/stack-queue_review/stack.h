#pragma once

#include <vector>
#include <deque>
namespace ns1
{
	template <class T, class Container = deque<T>>
	class stack
	{
	public:
		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}

		const T& top() const
		{
			return _con.back();
		}

		void push(const T& val)
		{
			_con.push_back(val);
		}

		void pop()
		{
			_con.pop_back();
		}

		void swap(stack& st)
		{
			_con.swap(st._con);
		}
	private:
		Container _con;
	};
}
