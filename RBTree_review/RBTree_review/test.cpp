#include <iostream>
using namespace std;
#include "RBTree.h"


void test1()
{
	//int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	//int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14, 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	RBTree<int, int> t;

	for (auto& e : a)
	{
		if (e == 1)
			int x = 1;
		t.insert({ e,e });

	}
	t.inOrder();
	cout << t.isBalance() << endl;
}

int main()
{
	test1();
	return 0;
}