﻿#pragma once

namespace ns1
{
	template <class K, class V>
	class map
	{
		struct MapKeyOfT
		{
			const K& operator()(const pair<K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, pair<const K, V>, MapKeyOfT>::iterator iterator;
		typedef typename RBTree<K, pair<const K, const V>, MapKeyOfT>::const_iterator const_iterator;

		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}

		const_iterator begin() const
		{
			return _t.begin();
		}

		const_iterator end() const
		{
			return _t.end();
		}

		iterator find(const K& key)
		{
			return _t.find(key);
		}

		pair<iterator, bool> insert(const pair<K, V>& kv)
		{
			return _t.insert(kv);
		}

		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = insert(make_pair(key, V()));
			iterator it = ret.first;
			return it->second;
		}
	private:
		RBTree<K, pair<const K, V>, MapKeyOfT> _t;
	};

	void test_map1()
	{
		// 测试迭代器
		map<string, int> m;
		m.insert({ "苹果",1 });
		m.insert({ "香蕉",1 });
		m.insert({ "梨",1 });
		m.insert({ "苹果",3 });

		map<string, int>::iterator it = m.begin();
		while (it != m.end())
		{
			it->second += 1;

			cout << it->first << ":" << it->second << endl;
			++it;
		}
		cout << endl;
	}

	void test_map2()
	{
		// 统计次数
		string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜",
"苹果", "香蕉", "苹果", "香蕉","苹果","草莓", "苹果","草莓" };
		map<string, int> countMap;
		for (auto& e : arr)
		{
			countMap[e]++;
		}

		for (auto& kv : countMap)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
		cout << endl;
	}

	void test_map3()
	{
		map<string, int> m1;
		m1.insert({ "苹果",1 });
		m1.insert({ "香蕉",1 });
		m1.insert({ "梨",1 });
		m1.insert({ "苹果",3 });

		// 拷贝构造
		map<string, int> copy(m1);
		for (auto& e : copy)
			cout << e.first << ":" << e.second << endl;
		cout << "---------" << endl;
		// 赋值重载
		map<string, int> m2;
		m2 = m1;
		for (auto& e : m2)
			cout << e.first << ":" << e.second << endl;

	}
}
