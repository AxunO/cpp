#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

class A {};
class B : public A {};

//class Person
//{
//public:
//	virtual Person& BuyTickets()
//	{
//		cout << "Person->全价" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual Student& BuyTickets()
//	{
//		cout << "Student->半价" << endl;
//	}
//};

//class Person
//{
//public:
//	virtual A& BuyTickets()
//	{
//		cout << "Person->全价" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual B& BuyTickets()
//	{
//		cout << "Student->半价" << endl;
//	}
//};

//class Person
//{
//public:
//	virtual void BuyTickets()
//	{
//		cout << "Person->全价" << endl;
//	}
//
//	virtual ~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual void BuyTickets()
//	{
//		cout << "Student->半价" << endl;
//	}
//
//	virtual ~Student()
//	{
//		// 清理开辟的空间
//		cout << "delete[]" << _ptr << endl;
//		delete[] _ptr;
//		cout << "~Student()" << endl;
//	}
//protected:
//	// 开辟空间
//	int* _ptr = new int[10];
//};

//void func1(Person* p)
//{
//	p->BuyTickets();
//}
//
//void func2(Person& p)
//{
//	p.BuyTickets();
//}

//class Person
//{
//public:
//	virtual void BuyTickets()
//	{
//		cout << "Person->全价" << endl;
//	}
//protected:
//	int _p = 1;
//};
//
//class Student : public Person
//{
//public:
//	void BuyTickets()
//	{
//		cout << "Student->半价" << endl;
//	}
//protected:
//	int _s = 2;
//};
//
//void func(Person& p)
//{
//	p.BuyTickets();
//}

//int main()
//{
//	//Person p;
//	//Student s;
//
//	Person* p1 = new Person;
//	Person* p2 = new Student;
//
//	delete p1;
//	delete p2;
//}

//int main()
//{
//	Person p;
//	Student s;
//
//	func(p);
//	func(s);
//}

//class Car final
//{
//public:
//	virtual void drive() final
//	{
//		cout << "Car->drive()" << endl;
//	}
//};
//class Benz : public Car
//{
//public:
//	virtual void drive()
//	{
//		cout << "Benz->drive()" << endl;
//	}
//};

//class Car
//{
//public:
//	virtual void drive() = 0
//	{
//		cout << "Car->drive()" << endl;
//	}
//};
//class Benz : public Car
//{
//public:
//	virtual void drive()
//	{
//		cout << "Benz->drive()" << endl;
//	}
//};
//
//int main()
//{
//	Car* cBenz = new Benz;
//	cBenz->drive();
//}


//class Base
//{
//public:
//	virtual void func()
//	{
//		cout << "func()" << endl;
//	}
//protected:
//	int _b = 1;
//};

//typedef void(*VFPTR)();
//
//void PrintVFPTR(VFPTR* vf)
//{
//	for (int i = 0; i < 3; i++)
//	{
//		// 打印函数的地址
//		printf("%p->", vf[i]);
//
//		// 取出函数地址，调用函数
//		VFPTR pvf = vf[i];
//		(*pvf)();
//	}
//}
//class Base
//{
//public:
//	virtual void func1()
//	{
//		cout << "Base::func1()" << endl;
//	}
//	virtual void func2()
//	{
//		cout << "Base::func2()" << endl;
//	}
//	void func3()
//	{
//		cout << "Base::func3()" << endl;
//	}
//protected:
//	int _b = 1;
//};
//
//class Derive : public Base
//{
//public:
//	virtual void func1()
//	{
//		cout << "Derive::func1()" << endl;
//	}
//	virtual void func4()
//	{
//		cout << "Drive::func4()" << endl;
//	}
//protected:
//	int _d = 2;
//};
//
//int main()
//{
//	//Base b;
//	//Derive d;
//	////cout << "sizeof(Base)->" << sizeof(Base) << endl;
//
//	Derive d;
//	VFPTR* ptr = (VFPTR*)(*((int*)&d));
//	PrintVFPTR(ptr);
//	return 0;
//}
//class Person
//{
//public:
//	virtual void BuyTickets()
//	{
//		cout << "Person->全价" << endl;
//	}
//protected:
//	int _p = 1;
//};
//
//class Student : public Person
//{
//public:
//	void BuyTickets()
//	{
//		cout << "Student->半价" << endl;
//	}
//protected:
//	int _s = 2;
//};
//
//void func(Person& p)
//{
//	p.BuyTickets();
//}
//
//int main()
//{
//	//Person p;
//	//func(p);
//
//	Student s;
//	func(s);
//	s.BuyTickets();
//	return 0;
//}

class Base1 {
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
private:
	int b1;
};
class Base2 {
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
	int b2;
};
class Derive : public Base1, public Base2 {
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
private:
	int d1;
};

typedef void(*VFPTR) ();
void PrintVTable(VFPTR vTable[])
{
	cout << " 虚表地址>" << vTable << endl;
	for (int i = 0; vTable[i] != nullptr; ++i)
	{
		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
		VFPTR f = vTable[i];
		f();
	}
	cout << endl;
}
int main()
{
	Derive d;
	VFPTR* vTableb1 = (VFPTR*)(*(int*)&d);
	PrintVTable(vTableb1);

	Base2* pf = &d;
	VFPTR* vTableb2 = (VFPTR*)(*(int*)pf);
	PrintVTable(vTableb2);
	return 0;
}