#pragma once
#include <iostream>
#include <vector>
using namespace std;



namespace OpenAddress
{

	// 转换 key 为整型
	template <class K>
	struct HashFunc
	{
		size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};
	// 特化
	template<>
	struct HashFunc<string>
	{
		size_t operator()(const string& s)
		{
			size_t ret = 0;
			for (auto& e : s)
			{
				ret = ret * 131 + e;
			}
			return ret;
		}
	};

	enum STATE
	{
		EMPTY,
		EXIST,
		DELETE
	};
	template <class K, class V, class Hash = HashFunc<K>>
	struct HashData
	{
		pair<K, V> _kv;
		STATE _sta = EMPTY; // 数据的状态
	};

	template <class K, class V, class Hash>
	class HashTable
	{
	public:
		HashTable()
		{
			_table.resize(10); // 哈希表初始大小
		}

		bool insert(const pair<K, V>& kv)
		{
			// 检查键值对是否已经存在
			if (find(kv.first))
				return false;
			// 扩容
			if (_n * 10 / _table.size() == 7)
			{
				HashTable<K, V, Hash> newHT;
				newHT._table.resize(_table.size() * 2);

				for (int i = 0; i < _table.size(); i++)
				{
					if (_table[i]._sta == EXIST)
					{
						newHT.insert(_table[i]._kv);
					}
				}

				_table.swap(newHT._table);
			}

			// 插入，除留余数法计算哈希地址
			size_t hashi = Hash()(kv.first) % _table.size();

			//线性探测
			while (_table[hashi]._sta == EXIST)
			{
				++hashi;
				// 防止越界，要进行%操作，在哈希表范围之内寻找
				hashi %= _table.size();
			}

			// 找到的位置是删除状态或者是空状态，插入元素
			_table[hashi]._kv = kv;
			_table[hashi]._sta = EXIST;
			++_n;

			return true;
		}

		HashData<K, V>* find(const K& key)
		{
			size_t hashi = Hash()(key) % _table.size();

			// 线性探测
			while (_table[hashi]._sta != EMPTY)
			{
				// 元素存在，且 key 与要找的 key 相同
				if (_table[hashi]._sta == EXIST && _table[hashi]._kv.first == key)
					return &_table[hashi];

				++hashi;
				hashi %= _table.size();
			}
			// 找不到
			return nullptr;
		}

		bool erase(const K& key)
		{
			HashData<K, V>* ret = find(key);
			if (ret == nullptr) // 数据不存在
				return false;
			
			// 存在，删除
			ret->_sta = DELETE;
			--_n;
			return true;
		}

	private:
		vector<HashData<K, V>> _table; // 哈希表
		size_t _n = 0; // 有效元素的数量
	};

	/*void testAdd1()
	{
		int arr[] = { 1, 37, 26, 4, 15, 99, 44, 12, 17};

		HashTable<int, int> ht;
		for (auto e : arr)
			ht.insert(make_pair(e, e));

		cout << ht.find(37) << endl;
		ht.erase(37);
		cout << ht.find(37) << endl;
		cout << ht.find(99) << endl;
	}
	void testAdd2()
	{
		HashTable<string, int> ht;
		ht.insert(make_pair("string", 1));
		ht.insert(make_pair("hash", 2));
		ht.insert(make_pair("table", 3));
	}*/
}

// 2.哈希桶
namespace hash_bucket
{
	template <class T>
	struct HashNode
	{
		HashNode(const T& data) 
			:_data(data)
			, _next(nullptr) 
		{}

		T _data;
		HashNode<T>* _next;
	};

	// 声明
	template <class K, class T, class KeyOfT, class Hash>
	class HashTable;
	// 迭代器
	template <class K, class T, class Ref, class Ptr, class KeyOfT, class Hash>
	struct __HTIterator
	{
		typedef HashNode<T> Node;
		typedef __HTIterator<K, T, Ref, Ptr, KeyOfT, Hash> Self;
		typedef HashTable<K, T, KeyOfT, Hash> HashTable;

		__HTIterator(Node* node, const HashTable* pht)
			:_node(node)
			, _pht(pht)
		{}

		Node* _node;
		const HashTable* _pht;

		//*it
		Ref operator*()
		{
			return _node->_data;
		}
		// it->
		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& it)
		{
			return _node != it._node;
		}
		bool operator==(const Self& it)
		{
			return _node == it._node;
		}

		Self& operator++()
		{
			// 1.中间节点
			if (_node->_next)
			{
				_node = _node->_next;
			}
			// 2. 最后节点
			else
			{
				// 计算当前桶位置
				KeyOfT kot;
				size_t hashi = Hash()(kot(_node->_data)) % _pht->_table.size();
				++hashi;

				// 向后寻找非空桶
				for (; hashi < _pht->_table.size(); hashi++)
				{
					if (_pht->_table[hashi])
						break;
				}
				
				// 判断是走完没找到，还是找到后break了
				if (hashi == _pht->_table.size())
					_node = nullptr; // 走完
				else
					_node = _pht->_table[hashi]; // 找到
			}
			return *this;
		}
	};

	template <class K, class T, class KeyOfT, class Hash>
	class HashTable
	{
		// 声明迭代器为友元类
		template <class K, class T, class Ref, class Ptr, class KeyOfT, class Hash>
		friend struct __HTIterator;

		typedef HashTable<K, T, KeyOfT, Hash> Self;
		typedef HashNode<T> Node;
	public:
		typedef __HTIterator<K, T, T&, T*, KeyOfT, Hash> iterator;
		typedef __HTIterator<K, T, const T&, const T*, KeyOfT, Hash> const_iterator;

		iterator begin()
		{
			// 寻找第一个非空桶
			for (int i = 0; i < _table.size(); i++)
				if (_table[i])
					return iterator(_table[i], this);
			// 找不到
			return end();
		}
		const_iterator begin() const
		{
			// 寻找第一个非空桶
			for (int i = 0; i < _table.size(); i++)
				if (_table[i])
					return iterator(_table[i], this);
			// 找不到
			return end();
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}
		const_iterator end() const
		{
			return iterator(nullptr, this);
		}

		HashTable()
		{
			_table.resize(10, nullptr);
		}

		~HashTable()
		{
			// 删除每个桶中的节点
			for (int i = 0; i < _table.size(); i++)
			{
				Node* cur = _table[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_table[i] = nullptr;
			}
		}

		// h2(h1)
		HashTable(const Self& ht)
		{
			// 调整大小
			_table.resize(ht._table.size());

			// 拷贝链表
			Node* copyHead = new Node(T()); // 拷贝链表的哨兵位
			for (int i = 0; i < ht._table.size(); i++)
			{
				Node* cur = ht._table[i]; // 遍历桶
				Node* copyCur = copyHead; // 遍历拷贝链表

				// 遍历，拷贝
				while (cur)
				{
					// 拷贝
					copyCur->_next = new Node(cur->_data);

					// 向下走
					cur = cur->_next;
					copyCur = copyCur->_next;
				}
				// 将拷贝的桶的头节点放到 h2
				_table[i] = copyHead->_next;
				copyHead->_next = nullptr;
			}
			_n = ht._n;
			delete copyHead;
		}

		// h2 = h1
		Self& operator=(Self t)
		{
			_table.swap(t._table);
			_n = t._n;
			return *this;
		}

		pair<iterator, bool> insert(const T& data)
		{
			KeyOfT kot;
			// 检查 key 是否重复
			iterator ret = find(kot(data));
			if (ret != end())
				return make_pair(ret, false);
			// 扩容
			if (_n == _table.size())
			{
				// 创建新表
				vector<Node*> newHT(_table.size()*2, nullptr);

				// 将旧表节点重新映射
				for (int i = 0; i < _table.size(); i++)
				{
					// 遍历桶
					Node* cur = _table[i];
					while (cur)
					{
						// 记录旧表当前桶中下一个节点
						Node* next = cur->_next;

						// 将当前节点映射到新表
						size_t hashi = Hash()(kot(cur->_data)) % newHT.size();
						cur->_next = newHT[hashi];
						newHT[hashi] = cur;
						
						// 更新cur，旧表当前桶的下一节点
						cur = next;
					}
					// 旧表置空
					_table[i] = nullptr;
				}

				// 新表映射完成，新旧表交换
				_table.swap(newHT);
			}
			// 插入
			// 计算哈希地址
			size_t hashi= Hash()(kot(data)) % _table.size();

			// new 新节点，头插
			Node* newnode = new Node(data);
			newnode->_next = _table[hashi];
			_table[hashi] = newnode;
			++_n;

			return make_pair(iterator(newnode, this), true);
		}

		iterator find(const K& key)
		{
			KeyOfT kot;
			// 确定桶
			size_t hashi = Hash()(key) % _table.size();
			Node* cur = _table[hashi];
			// 遍历桶
			while (cur)
			{
				if (kot(cur->_data) == key)
					return iterator(cur, this);
				cur = cur->_next;
			}
			// 找不到
			return end();
		}

		bool erase(const K& key)
		{
			// 计算哈希地址
			size_t hashi = Hash()(key) % _table.size();

			// 遍历桶
			KeyOfT kot;
			Node* cur = _table[hashi];
			Node* prev = nullptr; // 前一个节点
			while (cur)
			{
				if (kot(cur->_data) != key)
				{
					prev = cur;
					cur = cur->_next;
				}
				else // 找到目标
				{
					// 情况1，删除的是头节点
					if (prev == nullptr)
						_table[hashi] = cur->_next;
					// 情况2，删除中间节点
					else
						prev->_next = cur->_next;
					delete cur;
					return true;
				}
			}

			return false;
		}
	private:
		vector<Node*> _table;
		size_t _n = 0;
	};

	//void testBucket1()
	//{
	//	int arr[] = { 1, 37, 26, 4, 15, 99, 44, 12, 17, 68, 1000, 13};

	//	HashTable<int, int> ht;
	//	for (int e : arr)
	//		ht.insert(make_pair(e, e));
	//	HashTable<int, int> ht2(ht); // 拷贝构造


	//	cout << ht.find(37) << endl;
	//	ht.erase(37);
	//	cout << ht.find(37) << endl;
	//	cout << ht.find(17) << endl;
	//	cout << "----------------" << endl;
	//	cout << ht2.find(37) << endl;
	//	ht2.erase(37);
	//	cout << ht2.find(37) << endl;
	//	cout << ht2.find(17) << endl;
	//}
}