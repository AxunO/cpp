#pragma once

#include <iostream>
#include <assert.h>
#include <vector>
#include <string>
using namespace std;

namespace bit
{
	template<class T>
	class vector
	{
	public:

		// construct
		vector()
		{}

		// v1(v2)
		vector(const vector<T>& v)
		{
			/*const_iterator it = v.begin();
			while (it < v.end())
			{
				push_back(*it);
				it++;
			}*/
			reserve(v.capacity());
			for (auto& e : v)
			{
				push_back(e);
			}
		}

		// v1 = v2
		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}

		// v1({1,2})
		vector(initializer_list<T> il)
		{
			reserve(il.size());
			for (auto& e : il)
			{
				push_back(e);
			}
		}

		// v1(v2.begin(), v2.end())
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		// v1(n, val)
		vector(size_t n, const T& val = T())
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}
		vector(int n, const T& val = T())
		{
			reserve(n);
			for (int i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		~vector()
		{
			delete[] _start;
			_start = _finish = _endOfStorage = nullptr;
		}

		// iterator
		typedef T* iterator;
		typedef const T* const_iterator;

		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}

		const_iterator begin() const
		{
			return _start;
		}
		const_iterator end() const
		{
			return _finish;
		}


		// capacity
		size_t size() const
		{
			return _finish - _start;
		}

		size_t capacity() const
		{
			return _endOfStorage - _start;
		}

		bool empty() const
		{
			return _start == _endOfStorage;
		}

		void reserve(size_t n)
		{
			if (n > capacity())
			{
				int old_size = size();
				T* tmp = new T[n];
				for (size_t i = 0; i < size(); i++)
				{
					tmp[i] = _start[i];
				}

				delete[] _start;
				_start = tmp;
				_finish = tmp + old_size;
				_endOfStorage = tmp + n;
			}
		}

		void resize(size_t n, const T& val = T())
		{
			// 扩大size&&初始化
			if (n > size())
			{
				reserve(n);
				_finish = _start + n;

				iterator it = begin();
				while (it < end())
				{
					*it = val;
					it++;
				}
			}
			else
			{
				// 删除
				_finish = _start + n;
			}
		}


		// access
		T& operator[](size_t pos)
		{
			return _start[pos];
		}
		const T& operator[](size_t pos) const
		{
			return _start[pos];
		}
		

		// modify
		void push_back(const T& val)
		{
			//// 检查容量
			//if (size() == capacity())
			//{
			//	size_t newsize = (size() == 0 ? 4 : 2 * size());
			//	reserve(newsize);
			//}

			//_start[size()] = val;
			//_finish++;

			insert(_finish, val);
		}

		void pop_back()
		{
			/*assert(!empty());
			_finish--;*/

			erase(_finish - 1);
		}

		iterator insert(iterator pos, const T& val)
		{
			assert(pos >= _start && pos <= _finish);

			// 检查容量
			if (size() == capacity())
			{
				int len = pos - _start;
				size_t newsize = (size() == 0 ? 4 : 2 * size());
				reserve(newsize);
				// 更新pos迭代器
				pos = _start + len;
			}

			// 挪动数据
			iterator it = _finish - 1;
			while (it >= pos)
			{
				*(it + 1) = *(it);
				it--;
			}

			*pos = val;
			_finish++;

			return pos;
		}

		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos < _finish);

			iterator it = pos + 1;
			while (it < _finish)
			{
				*(it - 1) = *it;
				it++;
			}
			_finish--;

			return pos;
		}

		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endOfStorage, v._endOfStorage);
		}

	private:
		iterator _start = nullptr;
		iterator _finish = nullptr;
		iterator _endOfStorage = nullptr;
	};

	void Print(vector<int> v)
	{
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test1()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);
		v1.push_back(6);
		v1.push_back(7);
		v1.push_back(8);
		v1.push_back(9);

		v1.pop_back();
		v1.pop_back();
		v1.pop_back();
		v1.pop_back();

		bit::Print(v1);
	}

	void test2()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(4);
		v1.push_back(5);
		v1.push_back(4);


		// 删除偶数
		vector<int>::iterator it = v1.begin();
		while (it != v1.end())
		{
			if (*it %2 == 0)
			{
				it = v1.erase(it);
			}
			else
			{
				it++;
			}
		}

		bit::Print(v1);
	}

	void test3()
	{
		vector<int> v1;
		vector<int> v2;

		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);

		v2.push_back(4);
		v2.push_back(5);
		v2.push_back(6);

		Print(v1);
		Print(v2);

		v1.swap(v2);

		Print(v1);
		Print(v2);
	}

	void test4()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		vector<int> v2(v1);
		vector<int> v3;
		v3 = v2;

		Print(v1);
		Print(v2);
		Print(v3);
	}

	void test5()
	{
		vector<int> v1({ 1,2,3,4,5 });
		vector<int> v2;
		v2 = { 6,7,8,9,10 };

		Print(v1);
		Print(v2);
	}

	void test6()
	{
		string s1("abc");
		vector<int> v1(s1.begin(), s1.end());

		vector<int> v2;
		v2.push_back(1);
		v2.push_back(2);
		v2.push_back(3);
		v2.push_back(4);

		vector<int> v3(v2.begin() + 1, v2.end() - 1);

		Print(v1);
		Print(v2);
		Print(v3);
	}

	void test7()
	{
		vector<int> v1(10, 10);
		vector<int> v2(10, 'a');
		vector<int> v3(10u, 1);

		Print(v1);
		Print(v2);
		Print(v3);
	}
}