#pragma once
#include <list>
namespace ns1
{
	template <class T, class Container = list<T>>
	class Queue
	{
	public:
		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}

		const T& front()
		{
			return _con.front();
		}

		const T& back()
		{
			return _con.back();
		}

		void push(const T& val)
		{
			_con.push_back(val);
		}

		void pop()
		{
			_con.pop_front();
		}
	private:
		Container _con;
	};

	template <class T>
	class less
	{
	public:
		bool operator()(const T& a, const T& b)
		{
			return a < b;
		}
	};

	template <class T>
	class greater
	{
	public:
		bool operator()(const T& a, const T& b)
		{
			return a > b;
		}
	};

	// 优先级队列 大堆
	template <class T, class Container = vector<T>, class Compare = less<int>>
	class Priority_queue
	{
	public:
		// 向上调整
		void ajust_up(int child)
		{
			int parent = (child - 1) / 2; // 确定父节点
			while (child > 0)
			{
				//if (_con[parent] < _con[child])
				if (Compare()(_con[parent], _con[child]))
				{
					// 父节点比子节点小/大，交换
					swap(_con[parent], _con[child]);
					// 更新父子
					child = parent;
					parent = (child - 1) / 2;
				}
				else
					break;
			}
		}

		void push(const T& val)
		{
			_con.push_back(val);
			ajust_up(_con.size() - 1); // 从插入元素开始向上调整
		}
		// 向下调整
		void ajust_down(int parent)
		{
			int child = parent * 2 + 1; // 先假定只有左孩子

			while (child < _con.size())
			{
				//if (child + 1 < _con.size() && _con[child] < _con[child + 1])
				if (child + 1 < _con.size() && Compare()(_con[child], _con[child + 1]))
					child++; // 右孩子存在，且右孩子比左孩子大/小，更新孩子节点为右孩子

				//if (_con[parent] < _con[child])
				if (Compare()(_con[parent], _con[child]))
				{
					// 父节点比子节点小/大，交换
					swap(_con[parent], _con[child]);
					// 更新父子
					parent = child;
					child = parent * 2 + 1;
				}
			}
		}

		void pop()
		{
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			ajust_down(0); // 从堆顶开始向下调整
		}

		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}

		const T& top()
		{
			return _con[0];
		}

	private:
		Container _con;
	};
}