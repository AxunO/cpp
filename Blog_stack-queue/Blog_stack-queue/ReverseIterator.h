#pragma once

namespace ns1
{
	template <class Iterator, class Ref, class Ptr>
	struct ReverseIterator
	{
		typedef ReverseIterator<Iterator, Ref, Ptr> self;
		Iterator _it;

		// 构造
		ReverseIterator(Iterator it)
			:_it(it)
		{}

		Ref operator*()
		{
			// 解引用下一个位置
			Iterator tmp = _it;
			return *(--tmp);
		}

		Ptr operator->()
		{
			return &(operator*());
		}

		self& operator++()
		{
			--_it;
			return *this;
		}
		
		self& operator--()
		{
			++_it;
			return *this;
		}

		bool operator!=(const self& rit)
		{
			return _it != rit._it;
		}
	};
}