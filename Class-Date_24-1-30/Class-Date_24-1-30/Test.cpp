#define _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

int main()
{
	Date d1(2002,5,11);
	d1.Print();
	
	Date d2 = d1 + 30000;
	d2.Print();

	/*cout << (d1 == d2) << endl;
	cout << (d1 != d2) << endl;
	cout << (d1 < d2) << endl;
	d2++;
	cout << (d1 < d2) << endl;
	cout << (d1 <= d2) << endl;
	cout << (d1 >= d2) << endl;
	cout << (d2 >= d1) << endl;*/

	/*cout << d1 - d2 << endl;
	d1++;
	cout << d1 - d2 << endl;*/

}