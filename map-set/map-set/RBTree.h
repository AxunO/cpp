#pragma once

enum Color
{
	RED,
	BLACK
};

template <class T>
struct RBTreeNode
{
	RBTreeNode(const T& data)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
		, _col(RED)
	{}
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	T _data;
	Color _col;
};

template <class T, class Ref, class Ptr>
struct __RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef __RBTreeIterator<T, Ref, Ptr> Self;

	// 使用节点指针构造
	__RBTreeIterator(Node* node)
		:_node(node)
	{}
	// 迭代器底层就是一个 Node 节点指针
	Node* _node;

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &(_node->_data);
	}

	Self& operator++()
	{
		// 存在右子树，返回右子树最左节点迭代器
		if (_node->_right)
		{
			Node* leftMin = _node->_right;
			while (leftMin && leftMin->_left)
				leftMin = leftMin->_left;

			_node = leftMin;
		}
		else
		{
			// 右子树不存在，向上找 孩子是父节点左子树 的祖先节点
			Node* cur = _node;
			Node* parent = cur->_parent;

			// 当 cur 是根节点时，parent 为空，循环终止
			// 当找到符合条件的根节点时，循环也会终止
			while (parent && cur == parent->_right)
			{
				cur = parent;
				parent = parent->_parent;
			}

			_node = parent;
		}

		return *this;
	}

	Self& operator--()
	{
		// 左子树存在，寻找左子树最右节点
		if (_node->_left)
		{
			Node* rightMax = _node->_left;
			while (rightMax && rightMax->_right)
				rightMax = rightMax->_right;

			_node = rightMax;
		}
		else
		{
			// 左子树不存在，找 孩子是父节点右子树 的那个祖先
			Node* cur = _node;
			Node* parent = cur->_parent;

			while (parent && cur == parent->_left)
			{
				cur = parent;
				parent = parent->_parent;
			}

			_node = parent;
		}

		return *this;
	}

	bool operator!=(const Self& s)
	{
		return _node != s._node;
	}
};

template <class K, class T, class KeyOfT>
class RBTree
{
	typedef RBTreeNode<T> Node;
public:
	typedef __RBTreeIterator<T, T&, T*> iterator;
	typedef __RBTreeIterator<T, const T&, const T*> const_iterator;

	iterator begin()
	{
		Node* leftMin = _root;
		while (leftMin && leftMin->_left)
			leftMin = leftMin->_left;

		return iterator(leftMin);
	}

	iterator end()
	{
		return iterator(nullptr);
	}
	
	const_iterator begin() const
	{
		Node* leftMin = _root;
		while (leftMin && leftMin->_left)
			leftMin = leftMin->_left;

		return const_iterator(leftMin);
	}

	const_iterator end() const
	{
		return const_iterator(nullptr);
	}

	RBTree() = default;
	~RBTree()
	{
		destroy(_root);
		_root = nullptr;
	}

	RBTree(const RBTree<K, T, KeyOfT>& t)
	{
		_root = copy(t._root);
	}

	RBTree<K, T, KeyOfT> operator=(RBTree<K, T, KeyOfT> t)
	{
		swap(_root, t._root);
		return *this;
	}

	iterator find(const K& key)
	{
		// 树为空
		if (_root == nullptr)
			return end();

		// 树不为空
		KeyOfT kot;
		Node* cur = _root;
		while (cur)
		{
			if (key > kot(cur->_data))
				cur = cur->_right;
			else if (key < kot(cur->_data))
				cur = cur->_left;
			else
				return iterator(cur);
		}

		// 找不到
		return end();
	}

	pair<iterator,bool> insert(const T& data)
	{
		// 树为空
		if (_root == nullptr)
		{
			// 直接插入到根节点，返回
			_root = new Node(data);
			_root->_col = BLACK;
			return make_pair(iterator(_root), true);
		}

		// 树不为空，寻找合适位置插入
		Node* parent = nullptr;
		Node* cur = _root;
		KeyOfT kot;
		while (cur)
		{
			if (kot(data) > kot(cur->_data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (kot(data) < kot(cur->_data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else
				return make_pair(iterator(cur), false); // 不允许重复键值
		}

		// 插入新节点
		cur = new Node(data);
		cur->_parent = parent;
		if (kot(cur->_data) > kot(parent->_data))
			parent->_right = cur;
		else
			parent->_left = cur;
		// 记录新插入的节点，最后返回
		Node* newnode = cur;

		// 检查是否违反红黑树性质
		// parent 是红色就需要调整
		// parent 不存在或者是黑色就结束
		while (parent && parent->_col == RED)
		{
			// 寻找 g 和 u
			Node* grandpa = parent->_parent;
			Node* uncle = nullptr;
			// 区分 u 是 g 的左还是右，情况2需要用到
			if (parent == grandpa->_left)
				uncle = grandpa->_right;
			else
				uncle = grandpa->_left;

			// 情况1，u 存在且为红
			if (uncle && uncle->_col == RED)
			{
				// p u 变黑，g变红
				parent->_col = uncle->_col = BLACK;
				grandpa->_col = RED;

				// 更新 cur 和 p
				// 需不需要继续向上更新，交给 while 条件判断
				// cur 是根节点，结束
				// cur 是子树，且父节点为红色，继续调整
				cur = grandpa;
				parent = grandpa->_parent;
			}
			// 情况2，u不存在或者存在且为黑
			else if (uncle == nullptr || uncle->_col == BLACK)
			{
				// u 为右树版本
				if (uncle == grandpa->_right)
				{
					// 情况2.a，cur 为左树
					if (cur == parent->_left)
					{
						//     g
						//  p     u
						// c
						// 右旋，改颜色
						RotateR(grandpa);
						parent->_col = BLACK;
						grandpa->_col = RED;
					}
					// 情况 2.b，cur 为右树
					else if (cur == parent->_right)
					{
						//     g
						//  p     u
						//    c
						// 以p左旋，再以g右旋，改颜色
						RotateL(parent);
						RotateR(grandpa);

						cur->_col = BLACK;
						grandpa->_col = RED;
					}
				}
				// u 为左树版本
				else if (uncle == grandpa->_left)
				{
					// 情况2.a，cur 为 p 的右树
					if (cur == parent->_right)
					{
						//    g
						// u     p
						//        c
						// 以 g 左旋，更改颜色
						RotateL(grandpa);

						parent->_col = BLACK;
						grandpa->_col = RED;
					}
					// 情况2.b，cur 为 p 的左树
					else if (cur == parent->_left)
					{
						//    g
						// u     p
						//     c
						// 以 p 右旋，再以 g 右旋，更改颜色
						RotateR(parent);
						RotateL(grandpa);

						cur->_col = BLACK;
						grandpa->_col = RED;
					}
				}
				// 情况2结束后就不需要调整了
				break;
			}
		}
		// 不管如何调整，根节点必是黑色
		_root->_col = BLACK;
		return make_pair(iterator(newnode), true);
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		// 改变链接
		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		subL->_right = parent;
		Node* pparent = parent->_parent;
		parent->_parent = subL;

		// 检查 p 是根节点还是子树
		if (pparent)
		{
			// 子树
			subL->_parent = pparent;
			if (parent == pparent->_right)
				pparent->_right = subL;
			else
				pparent->_left = subL;
		}
		else
		{
			// 根节点
			_root = subL;
			_root->_parent = nullptr;
		}
	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		// 更改链接关系
		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* pparent = parent->_parent;
		parent->_parent = subR;

		// 检查 p 是子树还是根
		if (pparent)
		{
			// 子树
			subR->_parent = pparent;
			if (parent == pparent->_right)
				pparent->_right = subR;
			else
				pparent->_left = subR;
		}
		else
		{
			// 根节点
			_root = subR;
			_root->_parent = nullptr;
		}
	}

	bool isBalance()
	{
		// 计算任一路径黑色节点数
		int refNum = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
				++refNum;
			// 计算最左路径黑色节点数
			cur = cur->_left;
		}
		return _isBalance(_root, 0, refNum);
	}

	
private:
	void destroy(Node* root)
	{
		if (root == nullptr) return;

		destroy(root->_left);
		destroy(root->_right);
		delete root;
		root = nullptr;
	}

	Node* copy(Node* root)
	{
		if (root == nullptr) return nullptr;

		// 拷贝根节点
		Node* copynode = new Node(root->_data);
		copynode->_col = root->_col;

		// 拷贝左子树，链接
		copynode->_left = copy(root->_left);
		if (copynode->_left) // 左子树存在，链接
			copynode->_left->_parent = copynode;

		// 拷贝右子树
		copynode->_right = copy(root->_right);
		if (copynode->_right) // 右子树存在，链接
			copynode->_right->_parent = copynode;

		return copynode;
	}

	bool _isBalance(Node* root, int blackNum, int refNum)
	{
		if (root == nullptr)
		{
			// 到达红黑树叶子节点
			// 判断黑色节点数量是否相同
			if (blackNum != refNum)
			{
				cout << "存在黑色节点数量不同的路径" << endl;
				return false;
			}
			return true;
		}

		// 存在连续红色节点
		if (root->_col == RED && root->_parent->_col == RED)
		{
			KeyOfT kot;
			cout << kot(root->_data) << "存在连续红色节点" << endl;
			return false;
		}

		// 记录黑色节点数
		if (root->_col == BLACK)
			++blackNum;
		
		// 判断左右子树是否是红黑树
		return _isBalance(root->_left, blackNum, refNum) && _isBalance(root->_right, blackNum, refNum);
	}
	
	Node* _root = nullptr;
};