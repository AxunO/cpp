#define  _CRT_SECURE_NO_WARNINGS 1;
#include <iostream>
using namespace std;
#include <exception>
#include <memory>
#include <functional>
#include "shared_ptr.h"
#include <list>
#include <thread>
#include <mutex>

// 智能指针
//template <class T>
//class SmartPtr
//{
//public:
//	SmartPtr(T* ptr)
//		:_ptr(ptr)
//	{}
//
//	~SmartPtr()
//	{
//		delete _ptr;
//	}
//
//	T* get()
//	{
//		return _ptr;
//	}
//
//	T& operator*()
//	{
//		return *_ptr;
//	}
//
//	T* operator->()
//	{
//		return _ptr;
//	}
//
//	T& operator[](size_t i)
//	{
//		return _ptr[i];
//	}
//private:
//	T* _ptr;
//};
//
//class A
//{
//public:
//	A(const int& a1 = 1, const int& a2 = 1)
//		:_a1(a1)
//		,_a2(a2)
//	{}
//
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a1 = 1;
//	int _a2 = 2;
//};
//
//
//int div()
//{
//	int a, b;
//	cin >> a >> b;
//
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//
//void func()
//{
//	//SmartPtr<A> sp1(new A(1, 1));
//	//SmartPtr<A> sp2(new A(2, 2));
//	SmartPtr<int> sp1(new int[10]);
//	SmartPtr<int> sp2(new int[20]);
//	div();
//}

//int main()
//{
//	try
//	{
//		func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}



// 循环引用，导致内存泄漏
//struct Node
//{
//	weak_ptr<Node> _next;
//	weak_ptr<Node> _prev;
//
//	int _val;
//
//	~Node()
//	{
//		cout << "~Node()" << endl;
//	}
//};

//int main()
//{
//	ns1::shared_ptr<A> sp1(new A(1, 1));
//	ns1::shared_ptr<A> sp2(sp1);
//
//	ns1::shared_ptr<A> sp3(new A(2, 2));
//	sp3 = sp2;
//	return 0;
//}

// 循环引用
//int main()
//{
//	shared_ptr<Node> sp1(new Node);
//	shared_ptr<Node> sp2(new Node);
//
//	cout << sp1.use_count() << endl;
//	cout << sp2.use_count() << endl;
//
//	sp1->_next = sp2;
//	sp2->_prev = sp1;
//
//	cout << sp1.use_count() << endl;
//	cout << sp2.use_count() << endl;
//	return 0;
//}

// 删除器

//template <class T>
//struct DeleteArrayFunc
//{
//	void operator()(T* ptr)
//	{
//		delete[] ptr;
//	}
//};
//
//template <class T>
//struct FreeFunc
//{
//	void operator()(T* ptr)
//	{
//		cout << "free" << endl;
//		free(ptr);
//	}
//};
//int main()
//{
//	//ns1::shared_ptr<A> sp1(new A[10]);
//	// 仿函数
//	shared_ptr<A> sp1(new A[10], DeleteArrayFunc<A>());
//	shared_ptr<int> sp2((int*)malloc(4), FreeFunc<int>());
//
//	// lambda
//	shared_ptr<A> sp3(new A[10], [](A* p) {cout << "lambda delete[]" << endl; delete[] p; });
//
//	return 0;
//}

// 实现删除器
//int main()
//{
//	// 仿函数
//	ns1::shared_ptr<A> sp1(new A[10], DeleteArrayFunc<A>());
//	ns1::shared_ptr<int> sp2((int*)malloc(4), FreeFunc<int>());
//
//	// lambda
//	ns1::shared_ptr<A> sp3(new A[10], [](A* p) {cout << "lambda delete[]" << endl; delete[] p; });
//	ns1::shared_ptr<FILE> sp4(fopen("text.txt", "w"), [](FILE* fp) {cout << "fclose" << endl; fclose(fp); });
//
//	return 0;
//}



//--------------------------------------------------------------------------------------------------------------------------------
// blog


class A
{
public:
	A(const int& a1 = 1, const int& a2 = 1)
		:_a1(a1)
		,_a2(a2)
	{}

	~A()
	{
		cout << "~A()" << endl;
	}
private:
	int _a1 = 1;
	int _a2 = 2;
};


template <class T>
class SmartPtr
{
public:
	SmartPtr(T* ptr)
		:_ptr(ptr)
	{}

	~SmartPtr()
	{
		delete _ptr;
	}

	T& operator*()
	{
		return *_ptr;
	}

	T* operator->()
	{
		return _ptr;
	}
private:
	T* _ptr;
};
int div()
{
	int a, b;
	cin >> a >> b;
	if (b == 0)
		throw invalid_argument("除0错误");
	return a / b;
}

void Func()
{
	SmartPtr<A> sp1(new A);
	SmartPtr<A> sp2(new A);
	cout << div() << endl;
}

//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}


//struct Date
//{
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	SmartPtr<Date> sp1(new Date);
//	SmartPtr<Date> sp2(sp1);
//
//	return 0;
//}

//int main()
//{
//	ns1::shared_ptr<A> s1(new A(1, 1));
//	cout << s1.use_count() << endl;
//
//	// 拷贝构造
//	ns1::shared_ptr<A> s2(s1);
//	cout << s1.use_count() << endl;
//
//	// 拷贝赋值
//	ns1::shared_ptr<A> s3 = s1;
//	cout << s1.use_count() << endl;
//
//	return 0;
//}


struct ListNode
{
	weak_ptr<ListNode> _prev;
	weak_ptr<ListNode> _next;
	int _val;

	~ListNode()
	{
		cout << "~ListNode()" << endl;
	}
};


//int main()
//{
//	// new 两个节点，交给 shared_ptr 管理
//	shared_ptr<ListNode> node1(new ListNode);
//	shared_ptr<ListNode> node2(new ListNode);
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	// 链接两节点
//	node1->_next = node2;
//	node2->_prev = node1;
//
//	cout << node1.use_count() << endl;
//	cout << node2.use_count() << endl;
//
//	return 0;
//}

// 删除器

//// 仿函数
//template <class T>
//struct DeleteArrayFunc
//{
//	void operator()(T* p)
//	{
//		cout << "delete[]" << endl;
//		delete[] p;
//	}
//};
//int main()
//{
//	// 仿函数
//	ns1::shared_ptr<A> sp1(new A[10], DeleteArrayFunc<A>());
//	// lambda
//	// malloc free
//	ns1::shared_ptr<int> sp2((int*)malloc(4), [](int* p) 
//		{
//			cout << "free" << endl;
//			free(p);
//		});
//	// fopen fclose
//	ns1::shared_ptr<FILE> sp3(fopen("text.txt", "w"), [](FILE* fp)
//		{
//			cout << "fclose" << endl;
//			fclose(fp);
//		});
//	return 0;
//}


// 线程安全问题
mutex gmtx;
void func(ns1::shared_ptr<list<int>> sp, int n)
{
	ns1::shared_ptr<list<int>> copy(sp); // 多线程拷贝智能指针

	gmtx.lock();
	for (int i = 0; i < n; i++)
	{
		sp->emplace_back(i);
	}
	gmtx.unlock();
}
int main()
{
	ns1::shared_ptr<list<int>> sp(new list<int>); // 链表

	thread t1(func, sp, 1000); // 插入 1000 节点
	thread t2(func, sp, 2000); // 插入 2000 节点

	t1.join();
	t2.join();

	cout << sp->size() << endl;
	cout << "ptr count: " << sp.use_count() << endl;
	return 0;
}

