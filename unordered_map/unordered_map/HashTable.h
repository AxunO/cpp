#pragma once
#include <vector>


// 仿函数，将不能取模操作的类型转换为返回一个数字
template <class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return size_t(key);
	}
};
// 特化
template <>
struct HashFunc<string>
{
	size_t operator()(const string& s)
	{
		size_t ret = 0;
		for (char ch : s)
		{
			ret *= 131;
			ret += ch;
		}
		return ret;
	}
};

// 哈希表-开放地址法
namespace openaddress
{
	enum STATE
	{
		EXIST,
		EMPTY,
		DELETE
	};

	template <class K, class V>
	struct HashData
	{
		pair<K, V> _kv;
		STATE _sta = EMPTY;
	};

	template <class K, class V, class Hash = HashFunc<K>>
	class HashTable
	{
	public:
		HashTable()
		{
			_table.resize(10);
		}

		bool insert(const pair<K, V>& kv)
		{
			if (find(kv.first)) // 不允许键值重复
				return false;
			// 扩容
			//数据越多，越容易冲突，插入效率就越低，载荷因子 = 元素个数/哈希表长度
			if (_n * 10 / _table.size() == 7)
			{
				HashTable<K, V, Hash> newHT;
				newHT._table.resize(_table.size() * 2);

				// 将旧表数据重新负载到新表
				for (size_t i = 0; i < _table.size(); i++)
				{
					if (_table[i]._sta == EXIST)
						newHT.insert(_table[i]._kv);
				}

				_table.swap(newHT._table);
			}

			// 除留余数法，寻找插入位置
			Hash hs;
			size_t hashi = hs(kv.first) % _table.size();

			// 线性探测，解决冲突
			// 只要当前位置有数据，即状态为 EXIST，就向后寻找
			while (_table[hashi]._sta == EXIST)
			{
				++hashi;
				// 防止越界，一直在数组size范围内寻找
				hashi %= _table.size();
			}

			// 插入数据
			_table[hashi]._kv = kv;
			_table[hashi]._sta = EXIST;
			++_n;

			return true;
		}

		HashData<K, V>* find(const K& key)
		{
			Hash hs;
			size_t hashi = hs(key) % _table.size();
			// 线性探测
			while (_table[hashi]._sta != EMPTY)
			{
				// 第一个条件，防止返回已经删除的元素
				if (_table[hashi]._sta == EXIST && _table[hashi]._kv.first == key)
					return &_table[hashi];

				++hashi;
				hashi %= _table.size();
			}

			return nullptr;
		}

		bool erase(const K& key)
		{
			HashData<K, V>* ret = find(key);
			if (ret == nullptr)
				return false;

			ret->_sta = DELETE;
			--_n;
			return true;
		}
	private:
		vector<HashData<K, V>> _table;
		size_t _n;
	};
}

// 哈希表-哈希桶
namespace hash_bucket
{
	// 数据节点
	template<class T>
	struct HashNode
	{
		HashNode(const T& data)
			:_data(data)
		{}

		T _data;
		HashNode<T>* _next = nullptr;
	};

	// 迭代器
	// 哈希表和迭代器互相需要，所以要在前面声明一下
	template<class K, class T, class KeyOfT, class Hash>
	class HashTable;

	template<class K, class T, class Ref, class Ptr, class KeyOfT, class Hash>
	struct __HTIterator
	{
		typedef HashNode<T> Node;
		typedef HashTable<K, T, KeyOfT, Hash> HashTable;
		typedef __HTIterator<K, T, Ref, Ptr, KeyOfT, Hash> Self;

		__HTIterator(Node* node, const HashTable* pht)
			:_node(node)
			, _pht(pht)
		{}

		Ref operator*()
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& it)
		{
			return _node != it._node;
		}

		Self& operator++()
		{
			// 1.当前桶有下一个节点
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else
			{
				// 2.当前桶没有下一个节点，需要找到下一个桶的第一个节点
				KeyOfT kot;
				Hash hs;
				// 获取当前位置下标
				size_t hashi = hs(kot(_node->_data)) % _pht->_table.size();
				++hashi;
				// 寻找下一个非空桶
				for (; hashi < _pht->_table.size(); hashi++)
				{
					if (_pht->_table[hashi])
						break;
				}

				// 判断是走完了哈希表还是找到了下一个桶
				if (hashi == _pht->_table.size())
					_node = nullptr; // 走完
				else
					_node = _pht->_table[hashi]; // 找到
			}
			return *this;
		}

		Node* _node;
		const HashTable* _pht;
	};
	
	// 哈希表
	template<class K, class T, class KeyOfT, class Hash>
	class HashTable
	{
		// 声明迭代器为哈希表的友元
		template<class K, class T, class Ref, class Ptr, class KeyOfT, class Hash>
		friend struct __HTIterator;

		// typedefs
		typedef HashNode<T> Node;
		typedef HashTable<K, T, KeyOfT, Hash> Self;
		
	public:
		typedef __HTIterator<K, T, T&, T*, KeyOfT, Hash> iterator;
		typedef __HTIterator<K, T, const T&, const T*, KeyOfT, Hash> const_iterator;

		iterator begin()
		{
			// 返回第一个非空哈希桶的第一个节点
			for (int i = 0; i < _table.size(); i++)
			{
				if (_table[i])
					return iterator(_table[i], this);
			}

			// 找不到
			return end();
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}
		
		const_iterator begin() const
		{
			// 返回第一个非空哈希桶的第一个节点
			for (int i = 0; i < _table.size(); i++)
			{
				if (_table[i])
					return const_iterator(_table[i], this);
			}
			
			// 找不到
			return end();
		}

		const_iterator end() const
		{
			return const_iterator(nullptr, this);
		}

		HashTable()
		{
			_table.resize(10, nullptr);
			_n = 0;
		}

		HashTable(const Self& ht)
		{
			this->_table.resize(ht._table.size());

			for (int i = 0; i < ht._table.size(); i++)
			{
				Node* cur = ht._table[i];
				Node* copyHead = new Node({0, 0});
				Node* ret = copyHead;
				while (cur)
				{
					//insert(cur->_kv);
					copyHead->_next = new Node(cur->_kv);
					copyHead = copyHead->_next;
					cur = cur->_next;
				}
				_table[i] = ret->_next;
				delete copyHead;
			}
			this->_n = ht._n;
		}

		Self& operator=(Self ht)
		{
			_table.swap(ht._table);
			this->_n = ht._n;
			return *this;
		}

		~HashTable()
		{
			for (int i = 0; i < _table.size(); i++)
			{
				Node* cur = _table[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_table[i] = nullptr;
			}
		}

		pair<iterator, bool> insert(const T& data)
		{
			KeyOfT kot;
			Hash hs;

			// 检查是否已存在
			iterator it = find(kot(data));
			if (it != end()) // 不允许重复
				return make_pair(it, false);

			// 扩容
			if (_n == _table.size())
			{
				vector<Node*> newHT(_table.size() * 2, nullptr);

				// 如果使用现代写法，调用 newHT 的insert，会 new 新节点。复制旧表节点，又删除旧表，浪费空间，浪费时间
				// 不如手动写，充分发挥链表优势，直接将旧表节点重新负载到新表
				for (int i = 0; i < _table.size(); i++) // 遍历旧表
				{
					// 遍历桶
					Node* cur = _table[i];
					while (cur)
					{
						Node* next = cur->_next;
						// 头插
						size_t hashi = hs(kot(cur->_data)) % newHT.size();
						cur->_next = newHT[hashi];
						newHT[hashi] = cur;

						cur = next;
					}
					_table[i] = nullptr;
				}

				_table.swap(newHT);
			}
			// 头插节点
			size_t hashi = hs(kot(data)) % _table.size();
			Node* newnode = new Node(data);
			newnode->_next = _table[hashi];
			_table[hashi] = newnode;
			++_n;

			return make_pair(iterator(newnode, this), true);
		}

		iterator find(const K& key)
		{
			KeyOfT kot;
			Hash hs;
			// 计算 key 在表中的位置
			size_t hashi = hs(key) % _table.size();
			// 寻找
			Node* cur = _table[hashi];
			while (cur)
			{
				if (kot(cur->_data) == key)
					return iterator(cur, this);
				cur = cur->_next;
			}
			// 找不到
			return end();
		}

		bool erase(const K& key)
		{
			KeyOfT kot;
			Hash hs;
			size_t hashi = hs(key) % _table.size();

			Node* prev = nullptr;
			Node* cur = _table[hashi];
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					// 1.删除第一个
					if (prev == nullptr)
						_table[hashi] = cur->_next;
					// 2.中间删除
					else
						prev->_next = cur->_next;

					delete cur;
					return true;
				}

				prev = cur;
				cur = cur->_next;
			}

			return false;
		}
	private:
		vector<Node*> _table;
		size_t _n = 0;
	};
}