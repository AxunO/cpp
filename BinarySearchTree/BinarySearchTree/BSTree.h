#pragma once

#include <iostream>
#include <string>
using namespace std;


namespace key
{
	template<class K>
	struct BSTreeNode
	{
		BSTreeNode* _left;
		BSTreeNode* _right;
		K _key;

		BSTreeNode(const K& key)
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
		{};

	};

	template<class K>
	class BSTree
	{
		typedef BSTreeNode<K> Node;
	public:
		bool Insert(const K& key)
		{
			// 根为空
			if (_root == nullptr)
			{
				_root = new Node(key);
			}
			else
			{
				// 根不为空
				Node* cur = _root;
				Node* parent = cur;
				while (cur)
				{
					// 大于根
					if (key > cur->_key)
					{
						parent = cur;
						cur = cur->_right;
					}
					// 小于根
					else if (key < cur->_key)
					{
						parent = cur;
						cur = cur->_left;
					}
					// 等于
					else
					{
						return false;
					}
				}
				cur = new Node(key);
				if (key > parent->_key)
				{
					parent->_right = cur;
				}
				else
				{
					parent->_left = cur;
				}
			}
			return true;
		}

		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}

		bool Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				// 大于根
				if (key > cur->_key)
				{
					cur = cur->_right;
				}
				// 小于根
				else if (key < cur->_key)
				{
					cur = cur->_left;
				}
				// 等于
				else
				{
					return true;
				}
			}
			return false;
		}

		bool Erase(const K& key)
		{
			if (_root == nullptr)
				return false;

			Node* cur = _root;
			Node* parent = cur;
			while (cur)
			{
				if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					// 要删除节点的左为空，父节点指向右
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							if (cur == parent->_right)
							{
								parent->_right = cur->_right;
							}
							else
							{
								parent->_left = cur->_right;
							}
						}

						delete cur;
					}
					// 右为空，父节点指向左
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == parent->_right)
							{
								parent->_right = cur->_left;
							}
							else
							{
								parent->_left = cur->_left;
							}
						}

						delete cur;
					}
					// 都不为空，替换法，找右子树最小值或左子树最大值
					else
					{
						// 右树最小值
						Node* rightMin = cur->_right;
						Node* rightParent = cur;
						while (rightMin->_left != nullptr)
						{
							rightParent = rightMin;
							rightMin = rightMin->_left;
						}
						if (rightMin == rightParent->_left)
							rightParent->_left = rightMin->_right;
						else
							rightParent->_right = rightMin->_right;

						swap(cur->_key, rightMin->_key);
						delete rightMin;
					}
					return true;
				}
			}
			return false;
		}
	private:
		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);
			cout << root->_key << " ";
			_InOrder(root->_right);
		}
		Node* _root = nullptr;
	};

	void test1()
	{
		BSTree<int> b1;
		int a[] = { 8,3,1,10,6,4,7,14,13 };

		for (auto e : a)
		{
			b1.Insert(e);
		}

		b1.InOrder();

		for (auto e : a)
		{
			b1.Erase(e);
			b1.InOrder();
		}
	}
}
namespace key_val
{
	template<class K, class V>
	struct BSTreeNode
	{
		BSTreeNode* _left;
		BSTreeNode* _right;
		K _key;
		V _val;

		BSTreeNode(const K& key, const V& val)
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
			, _val(val)
		{};

	};

	template<class K, class V>
	class BSTree
	{
		typedef BSTreeNode<K, V> Node;
	public:
		bool Insert(const K& key, const V& val)
		{
			// 根为空
			if (_root == nullptr)
			{
				_root = new Node(key, val);
			}
			else
			{
				// 根不为空
				Node* cur = _root;
				Node* parent = cur;
				while (cur)
				{
					// 大于根
					if (key > cur->_key)
					{
						parent = cur;
						cur = cur->_right;
					}
					// 小于根
					else if (key < cur->_key)
					{
						parent = cur;
						cur = cur->_left;
					}
					// 等于
					else
					{
						return false;
					}
				}
				cur = new Node(key, val);
				if (key > parent->_key)
				{
					parent->_right = cur;
				}
				else
				{
					parent->_left = cur;
				}
			}
			return true;
		}

		void InOrder()
		{
			_InOrder(_root);
		}

		Node* Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				// 大于根
				if (key > cur->_key)
				{
					cur = cur->_right;
				}
				// 小于根
				else if (key < cur->_key)
				{
					cur = cur->_left;
				}
				// 等于
				else
				{
					return cur;
				}
			}
			return cur;
		}

		bool Erase(const K& key)
		{
			if (_root == nullptr)
				return false;

			Node* cur = _root;
			Node* parent = cur;
			while (cur)
			{
				if (key > cur->_key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (key < cur->_key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					// 要删除节点的左为空，父节点指向右
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							if (cur == parent->_right)
							{
								parent->_right = cur->_right;
							}
							else
							{
								parent->_left = cur->_right;
							}
						}

						delete cur;
					}
					// 右为空，父节点指向左
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == parent->_right)
							{
								parent->_right = cur->_left;
							}
							else
							{
								parent->_left = cur->_left;
							}
						}

						delete cur;
					}
					// 都不为空，替换法，找右子树最小值或左子树最大值
					else
					{
						// 右树最小值
						Node* rightMin = cur->_right;
						Node* rightParent = cur;
						while (rightMin->_left != nullptr)
						{
							rightParent = rightMin;
							rightMin = rightMin->_left;
						}
						if (rightMin == rightParent->_left)
							rightParent->_left = rightMin->_right;
						else
							rightParent->_right = rightMin->_right;

						swap(cur->_key, rightMin->_key);
						swap(cur->_val, rightMin->_val);
						delete rightMin;
					}
					return true;
				}
			}
			return false;
		}
	private:
		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);
			cout << root->_key << ":" << root->_val << endl;
			_InOrder(root->_right);
		}
		Node* _root = nullptr;
	};

	void test1()
	{
		BSTree<string, string> dict;
		dict.Insert("string", "字符串");
		dict.Insert("left", "左边");
		dict.Insert("insert", "插入");

		string str;
		while (cin >> str)
		{
			auto ret = dict.Find(str);
			if (ret)
				cout << ret->_val << endl;
			else
				cout << "无此单词" << endl;
		}
	}
}
