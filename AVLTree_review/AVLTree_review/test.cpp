#include <iostream>
#include <vector>
using namespace std;
#include "AVLTree.h"

void test1()
{
	AVLTree<int, int> t1;
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };

	for (auto& e : a)
		t1.insert({ e,e });
	t1.inOrder();
}

void testAVLT()
{
	AVLTree<int, int> t1;
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	int b[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };

	for (auto& e : b)
	{
		if (e == 7)
			int i = 0;
		t1.insert({ e,e });
		//cout <<  e << "->" << t1.isBlance() << endl;
	}

	t1.inOrder();
	cout << t1.isBlance() << endl;
}


void testAVLT2()
{
	const int N = 100000;
	vector<int> v;
	v.reserve(N);
	srand(time(0));

	for (size_t i = 0; i < N; i++)
	{
		v.push_back(rand() + i);
		//cout << v.back() << endl;
	}

	size_t begin2 = clock();
	AVLTree<int, int> t;
	for (auto e : v)
	{
		t.insert(make_pair(e, e));
		//cout << "Insert:" << e << "->" << t.IsBalance() << endl;
	}
	size_t end2 = clock();

	cout << "Insert:" << end2 - begin2 << endl;
	//cout << t.IsBalance() << endl;

	cout << "Height:" << t.height() << endl;
	cout << "Size:" << t.size() << endl;

	size_t begin1 = clock();
	// 确定在的值
	for (auto e : v)
	{
		t.find(e);
	}

	// 随机值
	/*for (size_t i = 0; i < N; i++)
	{
		t.Find((rand() + i));
	}*/

	size_t end1 = clock();

	cout << "Find:" << end1 - begin1 << endl;
}
int main()
{
	testAVLT2();
	return 0;
}