#define _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

// 全缺省构造函数
Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;
}

// 拷贝构造函数-浅拷贝
Date::Date(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
}

// 赋值运算符重载
Date& Date::operator=(const Date& d)
{
	if (this != &d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	return *this;
}

// 日期 += 天数
Date& Date::operator+=(int day)
{
	_day += day;
	while (_day > GetMonthDays(_year, _month))
	{
		_day -= GetMonthDays(_year, _month);
		_month++;

		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;
}

// 日期 + 天数
Date Date::operator+(int day)
{
	Date tmp = *this;
	tmp += day;
	return tmp;
}

// 日期 -= 天数
Date& Date::operator-=(int day)
{
	_day -= day;
	while (_day <= 0)
	{
		_month--;
		if (_month == 0)
		{
			_year--;
			_month = 12;
		}

		_day += GetMonthDays(_year, _month);
	}

	return *this;
}

// 日期 - 天数
Date Date::operator-(int day)
{
	Date tmp = *this;
	tmp -= day;
	return tmp;
}

// 前置++  先++ 再返回
Date& Date::operator++()
{
	*this += 1;
	return *this;
}

// 后置++ 先返回，再++
Date Date::operator++(int)
{
	Date tmp = *this;
	*this += 1;
	return tmp;
}

// 前置--
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}

// 后置--
Date Date::operator--(int)
{
	Date tmp = *this;
	*this -= 1;
	return tmp;
}

// ==
bool Date::operator==(const Date& d)
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}

// !=
bool Date::operator!=(const Date& d)
{
	return !(*this == d);
}

// <
bool Date::operator<(const Date& d)
{
	if (_year < d._year)
	{
		return true;
	}
	else if (_year == d._year)
	{
		if (_month < d._month)
		{
			return true;
		}
		else if (_month == d._month)
		{
			if (_day < d._day)
			{
				return true;
			}
		}
	}

	return false;
}

// <=
bool Date::operator<=(const Date& d)
{
	return *this == d || *this < d;
}

// >
bool Date::operator>(const Date& d)
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year)
	{
		if (_month > d._month)
		{
			return true;
		}
		else if (_month == d._month)
		{
			if (_day > d._day)
			{
				return true;
			}
		}
	}

	return false;
}

// >=
bool Date::operator>=(const Date& d)
{
	return *this == d || *this > d;
}

// 日期 - 日期
int Date::operator-(const Date& d)
{
	Date smaller = d;
	Date larger = *this;
	int flag = 1;

	if (*this < d)
	{
		smaller = *this;
		larger = d;
		flag = -1;
	}

	int count = 0;
	while (smaller != larger)
	{
		smaller++;
		count++;
	}

	return count * flag;
}