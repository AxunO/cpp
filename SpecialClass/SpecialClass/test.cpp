#include <iostream>
using namespace std;
#include <string>
#include <mutex>

// 不可以被拷贝
class A
{
public:
	A(const int& a = 0)
		:_a(a)
	{}

	A(const A&) = delete;
	A& operator=(const A&) = delete;
private:

	int _a;
};

// 只能在堆上创建对象
class HeapOnly
{
public:
	static HeapOnly* CreateObject()
	{
		return new HeapOnly;
	}

	HeapOnly(const HeapOnly&) = delete;
	HeapOnly& operator=(const HeapOnly&) = delete;
private:
	HeapOnly()
	{}
};

// 只能在栈上创建对象
class StackOnly
{
public:
	static StackOnly CreateObject()
	{
		return StackOnly();
	}

	void* operator new(size_t) = delete;
	void operator delete(void*) = delete;
private:
	StackOnly(const int& a = 0)
		:_a(a)
	{}
	int _a;
};

// 不能被继承
//class NonInherit
//{
//public:
//	static NonInherit GetInstance()
//	{
//		return NonInherit();
//	}
//private:
//	NonInherit()
//	{}
//};

class NonInherit final
{
public:
	static NonInherit GetInstance()
	{
		return NonInherit();
	}
private:
	NonInherit()
	{}
};


// 饿汉模式
//class ConfigInfo
//{
//public:
//	// 静态接口，获取单例对象
//	static ConfigInfo* GetInstance()
//	{
//		return &_sInfo;
//	}
//
//	string GetIp()
//	{
//		return _ip;
//	}
//
//	void SetIp(const string& ip)
//	{
//		_ip = ip;
//	}
//
//private:
//	// 构造私有化
//	ConfigInfo()
//	{
//		cout << "ConfigInfo()" << endl;
//	} 
//	// 防拷贝
//	ConfigInfo(const ConfigInfo&) = delete;
//	ConfigInfo& operator=(const ConfigInfo&) = delete;
//
//	// 配置信息
//	string _ip = "127.0.0.1";
//	int _port = 80;
//
//	// 静态全局变量，只是声明
//	static ConfigInfo _sInfo;
//};
//// 在 main 之前，定义静态变量
//ConfigInfo ConfigInfo::_sInfo;


// 懒汉一
//class ConfigInfo
//{
//public:
//	// 静态接口，获取单例对象
//	static ConfigInfo* GetInstance()
//	{
//		// 初始化静态变量
//		static ConfigInfo _sInfo;
//
//		return &_sInfo;
//	}
//
//	string GetIp()
//	{
//		return _ip;
//	}
//
//	void SetIp(const string& ip)
//	{
//		_ip = ip;
//	}
//
//private:
//	// 构造私有化
//	ConfigInfo()
//	{
//		cout << "ConfigInfo()" << endl;
//	}
//	// 防拷贝
//	ConfigInfo(const ConfigInfo&) = delete;
//	ConfigInfo& operator=(const ConfigInfo&) = delete;
//
//	// 配置信息
//	string _ip = "127.0.0.1";
//	int _port = 80;
//
//	// 静态全局变量，只是声明
//	static ConfigInfo _sInfo;
//};
////ConfigInfo ConfigInfo::_sInfo;


// 懒汉二
class ConfigInfo
{
public:
	// 静态接口，获取单例对象
	static ConfigInfo* GetInstance()
	{
		// 第一次调用，初始化静态变量
		if (_spInfo == nullptr)
		{
			unique_lock<mutex> lock(_mtx);
			if (_spInfo == nullptr)
			{
				_spInfo = new ConfigInfo;
			}
		}
		return _spInfo;
	}

	// 垃圾回收类
	class CGarbo
	{
	public:
		~CGarbo()
		{
			if (ConfigInfo::_spInfo)
			{
				cout << "delete:_spInfo" << endl;
				delete ConfigInfo::_spInfo;
			}
		}
	};
	static CGarbo Garbo;


	string GetIp()
	{
		return _ip;
	}

	void SetIp(const string& ip)
	{
		_ip = ip;
	}

private:
	// 构造私有化
	ConfigInfo()
	{
		cout << "ConfigInfo()" << endl;
	}
	// 防拷贝
	ConfigInfo(const ConfigInfo&) = delete;
	ConfigInfo& operator=(const ConfigInfo&) = delete;

	// 配置信息
	string _ip = "127.0.0.1";
	int _port = 80;

	// 静态指针，只是声明
	static ConfigInfo* _spInfo;
	static mutex _mtx;
};
ConfigInfo* ConfigInfo::_spInfo = nullptr;
mutex ConfigInfo::_mtx;
ConfigInfo::CGarbo Garbo;

int main()
{
	cout << ConfigInfo::GetInstance() << endl;

	// getip
	cout << ConfigInfo::GetInstance()->GetIp() << endl;
	// setip
	ConfigInfo::GetInstance()->SetIp("127.1.1.1");
	// getip
	cout << ConfigInfo::GetInstance()->GetIp() << endl;
	return 0;
}