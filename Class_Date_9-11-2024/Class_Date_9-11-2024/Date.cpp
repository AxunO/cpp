#include "Date.h"

bool Date::CheckInvalid()
{
	if (_month <= 0 || _month >= 13)
		return false;
	if (_day <= 0 || _day > GetMonthDay(_year, _month))
		return false;
	return true;
}

// 默认构造
Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;

	if (!CheckInvalid())
		cout << "构造日期非法！" << endl;
}

//// 析构
//Date::~Date()
//{
//	cout << "~Date()" << endl;
//	this->Print();
//}
//
//// 拷贝
//Date::Date(const Date& d)
//{
//	cout << "Date(const Date& d)" << endl;
//	_year = d._year;
//	_month = d._month;
//	_day = d._day;
//}
//
//// 赋值
//Date& Date::operator=(const Date& d)
//{
//	cout << "operator=(const Date& d)" << endl;
//	_year = d._year;
//	_month = d._month;
//	_day = d._day;
//	return *this;
//}

// 运算符重载
bool Date::operator==(const Date& d)
{
	return _year == d._year && _month == d._month && _day == d._day;
}

bool Date::operator!=(const Date& d)
{
	return !(*this == d);
}

bool Date::operator<(const Date& d)
{
	if (_year < d._year)
		return true;
	else if (_year == d._year)
	{
		if (_month < d._month)
			return true;
		else if (_month == d._month)
			return _day < d._day;
	}

	return false;
}

bool Date::operator<=(const Date& d)
{
	return (*this < d) || (*this == d);
}

bool Date::operator>(const Date& d)
{
	return !(*this <= d);
}

bool Date::operator>=(const Date& d)
{
	return !(*this < d);
}

// d+10
Date& Date::operator+=(int x)
{
	_day += x;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		
		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
	}

	return *this;
}

Date Date::operator+(int x)
{
	Date tmp = *this;
	tmp += x;
	return tmp;
}

// d-10
Date& Date::operator-=(int x)
{
	_day -= x;
	while (_day <= 0)
	{
		_month--;
		if (_month <= 0)
		{
			_month = 12;
			_year--;
		}
		_day += GetMonthDay(_year, _month);
	}

	return *this;
}

Date Date::operator-(int x)
{
	Date tmp = *this;
	tmp -= x;
	return tmp;
}

// ++d
Date& Date::operator++()
{
	*this += 1;
	return *this;
}

// d++
Date Date::operator++(int)
{
	Date tmp = *this;
	*this += 1;
	return tmp;
}

// --d
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}

// d--
Date Date::operator--(int)
{
	Date tmp = *this;
	*this -= 1;
	return tmp;
}

// d1 - d2
int Date::operator-(const Date& d)
{
	Date max = *this;
	Date min = d;
	int flag = 1;

	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int cnt = 0;
	while (min != max)
	{
		++min;
		++cnt;
	}

	return flag * cnt;
}

// <<
ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "/" << d._month << "/" << d._day << endl;
	return out;
}
// >>
istream& operator>>(istream& in, Date& d)
{
	while (1)
	{
		cout << "请按年月日输入：" << endl;
		in >> d._year >> d._month >> d._day;

		if (d.CheckInvalid())
			break;
		else
			cout << "输入日期非法，请重新输入" << endl;
	}
	return in;
}