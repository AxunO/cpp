#pragma once

namespace key
{
	// 节点
	template <class K>
	struct BSTreeNode
	{
		BSTreeNode(const K& key)
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
		{}
		BSTreeNode<K>* _left;
		BSTreeNode<K>* _right;
		K _key;
	};

	// 二叉搜索树
	template <class K>
	class BSTree
	{
		typedef BSTreeNode<K> Node;
	public:
		// 插入数据
		bool insert(const K& key)
		{
			// _root为空
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}

			// _root不为空
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur != nullptr)
			{
				// 继续寻找合适位置，更新parent cur
				if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else
					return false;
			}

			// cur为空，可以插入新节点，链接到parent
			// 判断父子关系
			cur = new Node(key);
			if (key > parent->_key)
				parent->_right = cur;
			else
				parent->_left = cur;
			return true;
		}

		bool find(const K& key)
		{
			// _root为空
			if (_root == nullptr)
				return false;
			// _root不为空
			Node* cur = _root;
			while (cur != nullptr)
			{
				// 继续寻找，更新parent cur
				if (cur->_key > key)
					cur = cur->_left;
				else if (cur->_key < key)
					cur = cur->_right;
				else
					return true; // 找到，返回true
			}

			return false;
		}

		bool erase(const K& key)
		{
			// 先找到要删除的节点，然后进行删除
			
			// _root为空
			if (_root == nullptr)
				return false;

			// _root不为空
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur != nullptr)
			{
				// 继续寻找，更新parent cur
				if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else
				{
					// 找到目标节点
					// cur左为空或右为空
					// 左为空，父亲接管cur的右
					if (cur->_left == nullptr)
					{
						// 如果cur是根节点，且只有右树
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							// 判断父子关系
							if (cur == parent->_right) // cur是右子树
								parent->_right = cur->_right;
							else
								parent->_left = cur->_right; // cur 是左子树
						}
						delete cur;
					}
					else if (cur->_right == nullptr)
					{
						// 右为空，parent接管cur的左
						// 如果cur是根节点，且只有左树
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == parent->_right) // cur是右子树
								parent->_right = cur->_left;
							else
								parent->_left = cur->_left;
						}
						delete cur;
					}
					else
					{
						// 左右不为空，替换法
						// 找右子树的最小(最左)节点，替换cur
						Node* rightMin = cur->_right;
						Node* rightMinParent = cur;
						while (rightMin->_left != nullptr)
						{
							rightMinParent = rightMin;
							rightMin = rightMin->_left;
						}
						if (rightMin == rightMinParent->_left)
							rightMinParent->_left = rightMin->_right;
						else
							rightMinParent->_right = rightMin->_right;
						swap(cur->_key, rightMin->_key);
						delete rightMin;
					}
					return true;
				}
			}

			// 找不到
			return false;
		}

		// 后序遍历
		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}
	private:
		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);
			cout << root->_key << " ";
			_InOrder(root->_right);
		}
		Node* _root = nullptr;
	};
}