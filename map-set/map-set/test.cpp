#include <iostream>
using namespace std;
#include <set>
#include <map>
#include <vector>
#include <string>

#include "RBTree.h"
#include "Map.h"
#include "Set.h"
//void test1()
//{
//	vector<int> v = { 1,2,3,4,5,6,7 };
//
//	// 迭代器区间构造
//	set<int> s1(v.begin(), v.end());
//
//	for (auto e : s1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}
//
//void test2()
//{
//	set<string> s1;
//
//	string s;
//	while (cin >> s)
//	{
//		auto ret = s1.insert(s);
//		if (ret.second)
//			cout << "插入成功：" << *(ret.first) << endl;
//		else
//			cout << "插入失败，key已存在：" << *(ret.first) << endl;
//	}
//}
//
//void test3()
//{
//	vector<int> v = { 1,2,3,4,5,6,7 };
//
//	// 迭代器区间构造
//	set<int> s1(v.begin(), v.end());
//	// 寻找 [3, 6) 区间
//	set<int>::iterator first = s1.lower_bound(3);
//	auto last = s1.upper_bound(5);
//	// 输出
//	while (first != last)
//	{
//		cout << *first << " ";
//		++first;
//	}
//	cout << endl;
//
//}
//
//void testPair()
//{
//	vector<pair<int, int>> v;
//	pair<int, int> p1(1, 1); // 1.构造有名对象
//	v.push_back(p1);
//	v.push_back(pair<int, int>(2, 2)); // 2.构造匿名对象
//	v.push_back(make_pair(3, 3)); // 3.调用 make_pair
//	v.push_back({ 4,4 }); // 4.隐式类型转换
//
//	// 输出
//	for (auto e : v)
//	{
//		cout << e.first << "->" << e.second << endl;
//	}
//}
//void testMap1()
//{
//	vector<pair<string, string>> v = { {"string", "字符串"},{"left", "左"},{"right", "右"} };
//	map<string, string> m(v.begin(), v.end());
//
//	for (auto e : m)
//	{
//		cout << e.first << "：" << e.second << endl;
//	}
//}
//void testMap2()
//{
//	vector<pair<string, string>> v = { {"string", "字符串"},{"left", "左"},{"right", "右"} };
//	map<string, string> m(v.begin(), v.end());
//
//	cout << m["string"] << endl;
//	cout << m["left"] << endl;
//	cout << m["right"] << endl;
//}
//
////mapped_type& operator[] (const key_type& k)
////{
////	return (*((this->insert(make_pair(k, mapped_type()))).first)).second;
////}
//
////mapped_type& operator[] (const key_type& k)
////{
////	pair<iterator, bool> ret = insert(make_pair(k, mapped_type()));
////	iterator it = ret.first;
////	return it->second;
////}
//
//void testMap3()
//{
//	vector<string> v = { "string",{"left"},{"right"},{"string"},{"left"},{"right"},{"string"},{"string"} };
//	map<string, int> s;
//	// 统计
//	for (auto& e : v)
//	{
//		//auto it = s.find(e);
//		//if (it != s.end()) // 单词存在
//		//	it->second++;
//		//else
//		//	s.insert({ e,1 }); // 单词不存在
//
//		s[e]++;
//	}
//	// 打印
//	for (auto e : s)
//	{
//		cout << e.first << ":" << e.second << endl;
//	}
//}

void test()
{
	//ns1::test_map1();
	ns1::test_set2();
}
int main()
{
	test();
	return 0;
}