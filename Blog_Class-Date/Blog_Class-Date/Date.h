#pragma once

#include <iostream>
using namespace std;

class Date
{
public:
	Date(int year = 1, int month = 1, int day = 1);
	Date* operator&()
	{
		return (Date*)0x11223344;
	}
	// < ==  > != <= >=
	bool operator<(const Date& d);
	bool operator==(const Date& d);
	bool operator!=(const Date& d);
	bool operator<=(const Date& d);
	bool operator>(const Date& d);
	bool operator>=(const Date& d);

	// d+10
	Date& operator+=(int x);
	Date operator+(int x);

	// d-10
	Date& operator-=(int x);
	Date operator-(int x);

	// ++
	Date& operator++();
	Date operator++(int);

	// --
	Date& operator--();
	Date operator--(int);

	// d1 - d2
	int operator-(Date& d);

	// <<
	friend ostream& operator<<(ostream& out, const Date& d);
	// >>
	friend istream& operator>>(istream& out, Date& d);

	int GetMonthDays(int year, int month)
	{
		static int days[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		// �ж�����
		if (month == 2 &&((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
			return 29;
		return days[month];
	}

	bool CheckInvalid()
	{
		if (_year < 0 || _month < 0 || _month > 12 || _day < 0 || _day > GetMonthDays(_year, _month))
			return false;
		return true;
	}

	void Print() const
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
private:
	int _year = 1;
	int _month = 1;
	int _day = 1;
};

// <<
ostream& operator<<(ostream& out, const Date& d);
// >>
istream& operator>>(istream& out, Date& d);
