#include <iostream>
using namespace std;

#include <stack>
#include <queue>
#include <algorithm>

#include "Stack.h"
#include "Queue.h"
#include "list.h"
void testStack()
{
	// stack 的使用
	stack<int> s1;
	s1.push(4);
	s1.push(3);
	s1.push(2);
	s1.push(1);
	// 遍历，出栈
	while (!s1.empty())
	{
		cout << s1.top() << " ";
		s1.pop();
	}
	cout << endl;
}

void testQueue()
{
	queue<int> q1;
	q1.push(1);
	q1.push(2);
	q1.push(3);
	q1.push(4);

	while (!q1.empty())
	{
		cout << q1.front() << " ";
		q1.pop();
	}
	cout << endl;
}

void testContainer()
{
	stack<int, vector<int>> s_vec;
	stack<int, list<int>> s_lt;

	s_vec.push(1);
	s_vec.push(2);

	s_lt.push(3);
	s_lt.push(4);
}

void testMyStack()
{
	ns1::Stack<int, list<int>> s;
	s.push(1);
	s.push(2);
	s.push(3);
	s.push(4);
	while (!s.empty())
	{
		cout << s.top() << " ";
		s.pop();
	}
	cout << endl;
}

void testMyQueue()
{
	ns1::Queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);

	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;
}

void test_prique()
{
	// priority_queue<int> pq;
	priority_queue<int, vector<int>, greater<int>> pq;
	pq.push(12);
	pq.push(3);
	pq.push(18);
	pq.push(5);
	pq.push(1);
	pq.push(9);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;
}

void testMyPQ()
{
	//ns1::Priority_queue<int> pq;
	ns1::Priority_queue<int, vector<int>, ns1::greater<int>> pq;
	pq.push(12);
	pq.push(3);
	pq.push(18);
	pq.push(5);
	pq.push(1);
	pq.push(9);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
	cout << endl;
}

void testFunctor()
{
	class Add
	{
	public:
		int operator()(const int& a, const int& b)
		{
			return a + b;
		}
	};

	int sum = Add()(1, 1);
	cout << sum << endl;
}

void testRiterator()
{
	ns1::list<int> l;
	l.push_back(1);
	l.push_back(2);
	l.push_back(3);
	l.push_back(4);

	ns1::list<int>::reverse_iterator rit = l.rbegin();
	while (rit != l.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}
int main()
{
	testRiterator();
	return 0;
}