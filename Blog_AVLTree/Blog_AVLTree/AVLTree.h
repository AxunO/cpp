#pragma once

#include <assert.h>
template <class K, class V>
struct AVLTreeNode
{
	AVLTreeNode(const pair<K, V>& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _bf(0)
	{}

	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	int _bf;
};


template <class K, class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	bool insert(const pair<K, V>& kv)
	{
		// 树为空
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		// 树不为空
		// 寻找合适插入位置
		Node* cur = _root, * parent = nullptr;
		while (cur)
		{
			if (kv.first > cur->_kv.first)
			{
				// 插入值 > 当前节点
				parent = cur;
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				// 插入值 < 当前节点
				parent = cur;
				cur = cur->_left;
			}
			else
				return false; // 不允许插入重复值
		}
		// 找到插入位置
		cur = new Node(kv);
		cur->_parent = parent;
		// 判断 cur 是 parent 的左子树还是右子树，链接
		if (cur->_kv.first > parent->_kv.first)
			parent->_right = cur;
		else
			parent->_left = cur;


		// 更新平衡因子, 更新到根节点结束
		while (parent)
		{
			// 更新父节点的平衡因子
			if (cur == parent->_right)
				parent->_bf++;
			else
				parent->_bf--;

			// 检查父节点平衡因子
			if (parent->_bf == 0)
			{
				// 树的高度无变化，不需要向上更新了
				break;
			}
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				// 当前树的高度发生变化，需要向上更新
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				// 需要进行调整

				if (parent->_bf == -2 && cur->_bf == -1)
				{
					// 左左，右旋
					RotateR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == 1)
				{
					// 右右，左旋
					RotateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					// 左右双旋
					RotateLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					// 右左双旋
					RotateRL(parent);
				}
				else
				{
					// 没有这种情况
					assert(false);
				}
				break;
			}
			else
			{
				// 出现这种情况，说明AVL树的实现出现了问题
				assert(false);
			}
		}
		return true;
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		// 改变链接关系
		parent->_left = subLR;
		if (subLR) // 检查是否存在
			subLR->_parent = parent;

		subL->_right = parent;
		Node* pparent = parent->_parent; // 记录 parent 的父节点
		parent->_parent = subL;

		if (pparent)
		{
			// parent 是子树
			if (pparent->_right == parent)
				pparent->_right = subL;
			else
				pparent->_left = subL;

			subL->_parent = pparent;
		}
		else
		{
			// parent是根节点
			_root = subL;
			_root->_parent = nullptr;
		}

		// 更新平衡因子
		subL->_bf = parent->_bf = 0;
	}
	
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		// 旋转，链接
		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		subR->_left = parent;
		Node* pparent = parent->_parent;
		parent->_parent = subR;

		// 检查 parent 是根据节点还是子树
		if (pparent)
		{
			// 子树
			if (pparent->_right == parent)
				pparent->_right = subR;
			else
				pparent->_left = subR;

			subR->_parent = pparent;
		}
		else
		{
			// 根节点
			_root = subR;
			_root->_parent = nullptr;
		}
		// 更新平衡因子
		subR->_bf = parent->_bf = 0;
	}

	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf; // 记录

		// 左旋
		RotateL(subL);
		// 右旋
		RotateR(parent);

		// 更新平衡因子
		subLR->_bf = 0;
		if (bf == 1)
		{
			parent->_bf = 0;
			subL->_bf = -1;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			subL->_bf = 0;
		}
		else
		{
			subL->_bf = parent->_bf = 0;
		}
	}

	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;

		// 右旋，左旋
		RotateR(subR);
		RotateL(parent);

		// 更新平衡因子
		subRL->_bf = 0;
		if (bf == 1)
		{
			subR->_bf = 0;
			parent->_bf = -1;
		}
		else if (bf == -1)
		{
			subR->_bf = 1;
			parent->_bf = 0;
		}
		else
		{
			parent->_bf = subR->_bf = 0;
		}
	}

	int height()
	{
		return _height(_root);
	}

	int size()
	{
		return _size(_root);
	}

	Node* find(const K& key)
	{
		// 树为空
		if (_root == nullptr)
			return nullptr;

		// 树不为空
		Node* cur = _root;
		while (cur)
		{
			if (key > cur->_kv.first)
			{
				// key值 > 当前节点
				cur = cur->_right;
			}
			else if (key < cur->_kv.first)
			{
				// 插入值 < 当前节点
				cur = cur->_left;
			}
			else
				return cur; // 找到
		}
		// 找不到
		return nullptr;
	}

	bool isBalance()
	{
		return _isBalance(_root);
	}
	void inOrder()
	{
		_inOrder(_root);
	}
private:

	bool _isBalance(Node* root)
	{
		if (root == nullptr) return true;

		int height_left = _height(root->_left);
		int height_right = _height(root->_right);

		if (abs(height_left - height_right) > 1)
			return false;

		return _isBalance(root->_left) && _isBalance(root->_right);
	}

	int _height(Node* root)
	{
		if (root == nullptr) return 0;
		return max(_height(root->_left), _height(root->_right)) + 1;
	}

	int _size(Node* root)
	{
		if (root == nullptr) return 0;
		return _size(root->_left) + _size(root->_right) + 1;
	}

	void _inOrder(Node* root)
	{
		if (root == nullptr) return;

		_inOrder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;;
		_inOrder(root->_right);
	}
	Node* _root = nullptr;
};
