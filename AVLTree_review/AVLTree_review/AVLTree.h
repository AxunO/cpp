#pragma once

#include <assert.h>
template <class K, class V>
struct AVLTreeNode
{
	AVLTreeNode(const pair<K,V>& kv = pair<K,V>())
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _bf(0)
	{}

	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	int _bf;
};


template <class K, class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	bool insert(const pair<K,V>& kv)
	{
		// 树为空
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		// 树不为空
		// 寻找合适插入位置
		Node* cur = _root, * parent = nullptr;
		while (cur)
		{
			if (kv.first > cur->_kv.first)
			{
				// 插入值 > 当前节点
				parent = cur;
				cur = cur->_right;
			}
			else if (kv.first < cur->_kv.first)
			{
				// 插入值 < 当前节点
				parent = cur;
				cur = cur->_left;
			}
			else
				return false; // 不允许插入重复值
		}
		// 找到插入位置
		cur = new Node(kv);
		cur->_parent = parent;
		// 判断 cur 是 parent 的左子树还是右子树，链接
		if (cur->_kv.first > parent->_kv.first)
			parent->_right = cur;
		else
			parent->_left = cur;


		// 更新平衡因子, 更新到根节点结束
		while (parent)
		{
			// 更新父节点平衡因子
			if (cur == parent->_right)
				parent->_bf++;
			else
				parent->_bf--;

			// 判断是否需要继续向上更新
			if (parent->_bf == 0)
			{
				// 树的高度无变化，不需要向上更新了
				break;
			}
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				// 高度发生变化，向上更新祖先节点的_bf
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				// 插入当前节点后，不满足 AVL树 的结构了，需要旋转操作

				// 1.左树高，需右旋
				if (parent->_bf == -2 && cur->_bf == -1)
				{
					// 以 parent 为旋转点，右旋
					RotateR(parent);
				}
				// 2.右树高，左旋
				else if (parent->_bf == 2 && cur->_bf == 1)
				{
					RotateL(parent);
				}
				// 3.左树高且右树高，先左旋再右旋
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					RotateLR(parent);
				}
				// 4.右树高且左树高，先右旋再左旋
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					RotateRL(parent);
				}
				
				break;
			}
			else
			{
				// 理论上不会出现这种情况
				assert(false);
			}
		}
		return true;
	}

	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		// 旋转
		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		Node* pParent = parent->_parent;// 记录parent的父节点
		subL->_right = parent;
		parent->_parent = subL;

		// 检查 parent 是不是根节点
		if (parent == _root)
		{
			// 更新根节点
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (pParent->_left == parent)
				pParent->_left = subL;
			else
				pParent->_right = subL;

			subL->_parent = pParent;
		}
		// 更新平衡因子
		subL->_bf = parent->_bf = 0;
	}

	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		// 旋转
		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;

		Node* pParent = parent->_parent;
		subR->_left = parent;
		parent->_parent = subR;

		// 检查 parent 是不是根节点
		if (parent == _root)
		{
			// 更新根节点
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			if (pParent->_right == parent)
				pParent->_right = subR;
			else
				pParent->_left = subR;

			subR->_parent = pParent;
		}
		// 更新平衡因子
		subR->_bf = parent->_bf = 0;
	}

	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;

		RotateL(subL);
		RotateR(parent);

		subLR->_bf = 0;
		if (bf == -1)
		{
			subL->_bf = 0;
			parent->_bf = 1;
		}
		else if (bf == 1)
		{
			parent->_bf = 0;
			subL->_bf = -1;
		}
		else
		{
			subL->_bf = 0;
			parent->_bf = 0;
		}
	}

	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;

		RotateR(subR);
		RotateL(parent);

		// 更新平衡因子
		subRL->_bf = 0;
		if (bf == -1)
		{
			parent->_bf = 0;
			subR->_bf = 1;
		}
		else if (bf == 1)
		{
			subR->_bf = 0;
			parent->_bf = -1;
		}
		else
		{
			parent->_bf = 0;
			subR->_bf = 0;
		}
	}
	Node* find(const K& key)
	{
		// 树为空
		if (_root == nullptr)
			return nullptr;

		// 树不为空
		Node* cur = _root;
		while (cur)
		{
			if (key > cur->_kv.first)
			{
				// key值 > 当前节点
				cur = cur->_right;
			}
			else if (key < cur->_kv.first)
			{
				// 插入值 < 当前节点
				cur = cur->_left;
			}
			else
				return cur; // 找到
		}
		// 找不到
		return nullptr;
	}

	bool isBlance()
	{
		return _isBlance(_root);
	}

	void inOrder()
	{
		_inOrder(_root);
		cout << endl;
	}

	int height()
	{
		return _height(_root);
	}

	int size()
	{
		return _size(_root);
	}
private:
	bool _isBlance(Node* root)
	{
		if (root == nullptr) 
			return true;

		// 计算左右子树高度
		int leftH = _height(root->_left);
		int rightH = _height(root->_right);
		if (abs(leftH - rightH) >= 2)
			return false;

		// 检测平衡因子
		if (rightH - leftH != root->_bf)
		{
			cout << root->_kv.first << root->_bf << endl;
			return false;
		}
		// 左右子树高度不超过1，且左右子树为AVLTree
		return _isBlance(root->_left) && _isBlance(root->_right);
	}

	int _height(Node* root)
	{
		if (root == nullptr)
			return 0;
		int leftH = _height(root->_left);
		int rightH = _height(root->_right);
		return max(leftH, rightH) + 1;
	}

	int _size(Node* root)
	{
		if (root == nullptr)
			return 0;
		return _size(root->_left) + _size(root->_right) + 1;
	}
	void _inOrder(Node* root)
	{
		if (root == nullptr) return;

		_inOrder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;;
		_inOrder(root->_right);
	}
	Node* _root = nullptr;
};
