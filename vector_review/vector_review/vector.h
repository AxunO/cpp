#pragma once

#define _CRT_NO_SECURE_WARNINGS_

#include <iostream>
#include <assert.h>
using namespace std;


namespace ns1
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		// 构造
		vector()
		{}

		vector(size_t n, const T& val = T())
		{
			reserve(n);
			for (int i = 0; i < n; i++)
				push_back(val);
		}
		
		vector(int n, const T& val = T())
		{
			reserve(n);
			for (int i = 0; i < n; i++)
				push_back(val);
		}

		template <class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		vector(const vector<int>& v)
		{
			reserve(v.capacity());
			for (auto& e : v)
				push_back(e);
		}

		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}

		vector(const initializer_list<T>& il)
		{
			for (auto& e : il)
				push_back(e);
		}

		~vector()
		{
			delete[] _start;
			_start = _finish = _endofstorage = nullptr;
		}

		// 遍历
		T& operator[](size_t pos)
		{
			assert(pos < size());
			return _start[pos];
		}

		const T& operator[](size_t pos) const
		{
			assert(pos < size());
			return _start[pos];
		}

		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}
		
		const_iterator begin() const
		{
			return _start;
		}

		const_iterator end() const
		{
			return _finish;
		}

		// 容量
		size_t size() const
		{
			return _finish - _start;
		}

		size_t capacity() const
		{
			return _endofstorage - _start;
		}

		bool empty() const
		{
			return _start == _finish;
		}

		void reserve(size_t n)
		{
			if (n > capacity())
			{
				T* tmp = new T[n];
				size_t old_size = size();

				/*memcpy(tmp, _start, sizeof(T) * size());*/
				for (int i = 0; i < size(); i++)
					tmp[i] = _start[i];
				delete[] _start;

				_start = tmp;
				_finish = tmp + old_size;
				_endofstorage = tmp + n;
			}
		}

		void resize(size_t n, const T& val = T())
		{
			// 删除数据
			if (n <= size())
			{
				_finish = _start + n;
			}
			else
			{
				// 填充数据
				reserve(n);

				/*for (int i = size(); i < n; i++)
					push_back(val);*/

				while (_finish < _start + n)
				{
					*_finish = val;
					++_finish;
				}
			}
		}

		// 修改
		void push_back(const T& val)
		{
			// 检查扩容
			if (_finish == _endofstorage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
				reserve(newcapacity);
			}

			*_finish = val;
			++_finish;
		}

		void pop_back()
		{
			assert(!empty());
			--_finish;
		}

		iterator insert(iterator pos, const T& val)
		{
			assert(pos >= _start);
			assert(pos <= _finish);

			// 检查扩容
			if (_finish == _endofstorage)
			{
				// 记录pos的相对位置
				size_t len = pos - _start;

				size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
				reserve(newcapacity);

				// 原 pos 失效，更新 pos
				pos = _start + len;
			}

			// 挪动数据
			iterator end = _finish;
			while (end != pos)
			{
				*end = *(end - 1);
				--end;
			}

			// 更新数据
			*pos = val;
			++_finish;

			return pos;
		}

		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos < _finish);

			// 挪动数据，覆盖
			iterator cur = pos + 1;
			while (cur != _finish)
			{
				*(cur - 1) = *cur;
				++cur;
			}
			--_finish;
			return pos;
		}

		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endofstorage, v._endofstorage);
		}

	private:
		iterator _start = nullptr;
		iterator _finish = nullptr;
		iterator _endofstorage = nullptr;
	};

	template <class T>
	void PirntVector(vector<T>& v)
	{
		for (int i = 0; i < v.size(); i++)
			cout << v[i] << " ";
		cout << endl;
	}

	template <class T>
	void swap(vector<T>& v1, vector<T>& v2)
	{
		v1.swap(v2);
	}

	void test1()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);
		v1.push_back(6);
		v1.push_back(7);
		v1.push_back(8);
		v1.push_back(9);

		PirntVector(v1);
	}

	void test2()
	{
		vector<int> v1;
		v1.push_back(10);
		v1.resize(10, 1);

		PirntVector(v1);
		cout << v1.size() << endl;
		cout << v1.capacity() << endl;
		
		v1.resize(5);

		PirntVector(v1);
		cout << v1.size() << endl;
		cout << v1.capacity() << endl;
	}

	void test3()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);
		v1.push_back(6);
		v1.push_back(7);
		v1.push_back(8);
		v1.push_back(9);

		vector<int> v2(10, 10);
		vector<int>::iterator it1 = v1.begin();
		for (it1; it1 != v1.end(); it1++)
			cout << *it1 << " ";
		cout << endl;
		
		vector<int>::const_iterator it2 = v2.begin();
		for (it2; it2 != v2.end(); it2++)
			cout << *it2 << " ";
		cout << endl;

		swap(v1, v2);
		PirntVector(v1);
		PirntVector(v2);
	}

	void test4()
	{
		vector<string> v1(2, "hello");
		vector<string> v2(2, "world");
		PirntVector(v1);
		PirntVector(v2);

		/*swap(v1, v2);
		PirntVector(v1);
		PirntVector(v2);*/
		v1.pop_back();
		v1.pop_back();
		PirntVector(v1);
	}

	void test5()
	{
		/*vector<int> v1(10, 1);
		vector<int> v2(10, 'a');
		vector<double> v3(10, 1.1);

		PirntVector(v1);
		PirntVector(v2);
		PirntVector(v3);*/

		vector<int> v1(10, 10);
		vector<int> v2(v1);

		PirntVector(v1);
		PirntVector(v2);
	}

	void test6()
	{
		vector<int> v1(10,1);
		PirntVector(v1);
		// 头插
		v1.insert(v1.begin(), 10);
		PirntVector(v1);
		// 尾插
		v1.insert(v1.end(), 30);
		PirntVector(v1);
		// 中间插
		v1.insert(v1.begin() + 1, 20);
		PirntVector(v1);

		// 头删
		v1.erase(v1.begin());
		PirntVector(v1);
		// 尾删
		v1.erase(v1.end() - 1);
		PirntVector(v1);
		// 中间删
		v1.erase(v1.begin() + 5);
		PirntVector(v1);
	}

	void test7()
	{
		vector<int> v1 = { 1,2,3,4,5,6,7,8,9,10 };
		PirntVector(v1);
	}

	void test8()
	{
		vector<int> v1(10, 2);
		vector<int> v2;
		v2 = v1;
		PirntVector(v1);
		PirntVector(v2);
	}
	
	void test9()
	{
		vector<string> v1;
		v1.push_back("一");
		v1.push_back("二");
		v1.push_back("三");
		v1.push_back("四");
		v1.push_back("五");

		PirntVector(v1);

	}
}