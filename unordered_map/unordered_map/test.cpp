#include <iostream>
using namespace std;
#include "HashTable.h"
#include "Unordered_Map.h"
#include "Unordered_Set.h"



void testOpen1()
{
	int arr[] = { 1001, 11, 32, 24, 35, 6, 12 };
	openaddress::HashTable<int, int> ht;
	for (auto& e : arr)
		ht.insert(make_pair(e, e));

	cout << ht.find(32) << endl;
	cout << ht.find(12) << endl;

	ht.erase(32);
	ht.erase(12);
	cout << ht.find(32) << endl;
	cout << ht.find(12) << endl;
}

//struct StringHashFunc
//{
//	size_t operator()(const string& s)
//	{
//		size_t ret = 0;
//		for (char ch : s)
//		{
//			ret *= 131;
//			ret += ch;
//		}
//		return ret;
//	}
//};
//void testOpen2()
//{
//	openaddress::HashTable<string, int> ht;
//	ht.insert(make_pair("sort", 1));
//	ht.insert(make_pair("left", 1));
//	ht.insert(make_pair("insert", 1));
//	ht.insert(make_pair("insert", 2));
//
//	/*cout << StringHashFunc()("sort") << endl;
//	cout << StringHashFunc()("left") << endl;
//	cout << StringHashFunc()("insert") << endl;*/
//}

//void testBucket1()
//{
//	hash_bucket::HashTable<int, int> ht;
//	int arr[] = { 1001, 11, 32, 24, 35, 6, 12, 43, 37, 78, 19, 11};
//
//	for (auto& e : arr)
//		ht.insert(make_pair(e, e));
//
//	cout << ht.find(1001)->_kv.first << endl;
//	ht.erase(1001);
//	cout << ht.find(1001) << endl;
//	cout << ht.find(11)->_kv.first << endl;
//
//
//}
//
//void testBucket2()
//{
//	hash_bucket::HashTable<int, int> ht;
//	int arr[] = { 1001, 11, 32, 24, 35, 6, 12, 43, 37, 78, 19, 11};
//
//	for (auto& e : arr)
//		ht.insert(make_pair(e, e));
//	
//
//	hash_bucket::HashTable<int, int> ht2(ht);
//	cout << ht2.find(1001)->_kv.first << endl;
//	ht2.erase(1001);
//	cout << ht2.find(1001) << endl;
//	cout << ht2.find(11)->_kv.first << endl;
//
//	hash_bucket::HashTable<int, int> ht3;
//	ht3 = ht;
//	cout << ht3.find(1001)->_kv.first << endl;
//	ht3.erase(1001);
//	cout << ht3.find(1001) << endl;
//	cout << ht3.find(11)->_kv.first << endl;
//
//	cout << ht.find(1001)->_kv.first << endl;
//	ht.erase(1001);
//	cout << ht.find(1001) << endl;
//	cout << ht.find(11)->_kv.first << endl;
//}

void test1()
{
	ns1::test_unordered_set();
	//ns1::test_unordered_map();
}
int main()
{
	test1();
	return 0;
}